using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKLegMove : MonoBehaviour
{
    public float xMaxDist = 0.25f;
    public float yMaxDist = 1f;
    const float minDistance = 0.005f;

    public GameObject target;

    public float speed = 15f;

    public AnimationCurve yCurve;
    private float timer;
    private Coroutine currentLegMover = null;

    // Update is called once per frame
    void Update()
    {
        /*
        if ((Mathf.Abs(transform.position.x - target.transform.position.x) > xMaxDist) || (Mathf.Abs(transform.position.y - target.transform.position.y) > yMaxDist)) {
            transform.position = target.transform.position;
            //timer = 0;
            //timer += Time.deltaTime;
            //transform.position = Vector2.MoveTowards(transform.position, new Vector2(target.transform.position.x, target.transform.position.y + yCurve.Evaluate(timer)), speed * Time.deltaTime);
        }
        */

        
        if (currentLegMover == null)
        {
            // Move the foot
            if ((Mathf.Abs(transform.position.x - target.transform.position.x) > xMaxDist) || (Mathf.Abs(transform.position.y - target.transform.position.y) > yMaxDist))
            {
                currentLegMover = StartCoroutine(LegMover());
            }
        }
        
    }


    private IEnumerator LegMover()
    {
        timer = 0;

        while (Vector3.Distance(transform.position, target.transform.position) > minDistance)
        {
            // Increase curve timer
            timer += Time.deltaTime;
            // Move towards desired target position
            transform.position = Vector2.MoveTowards
                (transform.position, new Vector2(target.transform.position.x, target.transform.position.y + yCurve.Evaluate(timer)), speed * Time.deltaTime);
            yield return null;
        }

        transform.position = target.transform.position;
        currentLegMover = null;
    }
}
