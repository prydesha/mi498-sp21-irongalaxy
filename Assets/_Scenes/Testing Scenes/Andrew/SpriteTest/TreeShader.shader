﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/ShadeShader"
 {
     Properties
     {
         [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
         _Color ("Tint", Color) = (1,1,1,1)
         [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
         [MaterialToggle] _UseFade ("Use Fade", Float) = 0
         _FadeOrigin ("Fade Origin", vector) = (1,1,1)

         _MaxOpacity ("Max Opacity", Range(0,1)) = 0.5
     }
 
     SubShader
     {
         Tags
         { 
             "Queue"="Transparent" 
             "IgnoreProjector"="True" 
             "RenderType"="Transparent" 
             "PreviewType"="Plane"
             "CanUseSpriteAtlas"="True"
         }
 
         Cull Off
         Lighting Off
         ZWrite Off
         Fog { Mode Off }
         Blend One OneMinusSrcAlpha
 
         Pass
         {
         CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #pragma multi_compile DUMMY PIXELSNAP_ON
             #include "UnityCG.cginc"
 
             struct appdata_t
             {
                 float4 vertex   : POSITION;
                 float4 color    : COLOR;
                 float2 texcoord : TEXCOORD0;
             };
 
             struct v2f
             {
                 float4 vertex   : SV_POSITION;
                 float4 objCoords : TEXCOORD1;
                 fixed4 color    : COLOR;
                 half2 texcoord  : TEXCOORD0;
             };
             
             fixed4 _Color;
 
             v2f vert(appdata_t IN)
             {
                 v2f OUT;
                 OUT.vertex = UnityObjectToClipPos(IN.vertex);
                 OUT.objCoords = mul(unity_ObjectToWorld, IN.vertex);
                 OUT.texcoord = IN.texcoord;
                 OUT.color = IN.color * _Color;
                 #ifdef PIXELSNAP_ON
                 OUT.vertex = UnityPixelSnap (OUT.vertex);
                 #endif
 
                 return OUT;
             }
 
             sampler2D _MainTex;
             float _UseFade;
             float _FadeAreaMin[100];
             float _FadeAreaMax[100];
             vector _FadeOrigin;
             float _MaxOpacity;
             vector _Origins[100];
             int _OriginsLength;
             fixed4 frag(v2f IN) : SV_Target
             {
                 //fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
                 fixed4 c = (0, 0, 0, 0);
                 float percent = 1;

                 float min;
                 float max;
                 /*
                 if(_UseFade == 1)
                 {
                     int ID = -1;
                     vector _closestFade = (9999, 9999, 9999);
                     float tempMax = 0;
                     for (int i = 0; i < _OriginsLength; i++)
                     {

                         if (distance(_Origins[i].xy, IN.objCoords.xy) > _FadeAreaMax[i] )
                         {
                             continue;
                         }

                         min = _FadeAreaMin[i];
                         max = _FadeAreaMax[i];
                         float pixDist = distance(_Origins[i].xy, IN.objCoords.xy);

                         if (pixDist < _FadeAreaMin[i])
                         {
                             if (_MaxOpacity < percent)
                             {
                                 percent = _MaxOpacity;
                             }
                         }
                         else
                         {
                             if (_FadeAreaMin[i] > _FadeAreaMax[i])
                             {

                                 min = _FadeAreaMax[i];
                             }

                             if (_FadeAreaMax[i] < _FadeAreaMin[i])
                             {
                                 max = _FadeAreaMin[i];
                             }

                             if (pixDist > min && pixDist < max)
                             {
                                 float fadeAreaSize = max - min;
                                 float pixPos = pixDist - min;
                                 float fadePercent = pixPos / fadeAreaSize;

                                 if (fadePercent < _MaxOpacity)
                                 {
                                     fadePercent = _MaxOpacity;
                                 }

                                 percent *= fadePercent;

                              //   if (fadePercent < percent)
                              //   {
                              //       percent = fadePercent;
                                // }
                             }
                         }
                     }
                 }
                 if (percent < _MaxOpacity)
                 {
                     percent = _MaxOpacity;
                 }
                 */
                 c.r += 500;
                 c.b += 500;
                 return c;
             }
         ENDCG
         }
     }
 }