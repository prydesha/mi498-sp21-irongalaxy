﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class SortingLayers : MonoBehaviour {
	
	public GameObject shade;
	public float areaMin = 1.5f;
	public float areaMax = 4.5f;
	public float flickeringEffect =.1f;
	public float flickeringSpeed = 3f;
	private int ID = -1;
	private float randCos = 0;
	private float randSin = 0;

	private void OnEnable()
    {
		shade = GameObject.Find("NightShade");
		randSin = Random.Range(-10f, 10f);
		randCos = Random.Range(-10f, 10f);

	}



    void LateUpdate () {

		SpriteRenderer tr = shade.GetComponent<SpriteRenderer>(); 

		
		GetComponent<SpriteRenderer>().sortingOrder = -1;
		tr.sharedMaterial.SetVector("_FadeOrigin", transform.position);

		var tempOrigins = tr.sharedMaterial.GetVectorArray("_Origins");
		var tempMin = tr.sharedMaterial.GetFloatArray("_FadeAreaMin");
		var tempMax = tr.sharedMaterial.GetFloatArray("_FadeAreaMax");
		if (tempOrigins == null || tempMax== null || tempMin == null)
		{
			tempOrigins = new Vector4[100];
			tempMax = new float[100];
			tempMin = new float[100];
		}


		var maxAdd = flickeringEffect * (Mathf.Cos(randCos +(Time.time* flickeringSpeed)) + Mathf.Sin(randSin+ (Time.time * flickeringSpeed)));


		for (int i = 0; i < tempOrigins.Length; i++)
		{
			if (tempOrigins[i] == new Vector4(0.0f, 0.0f, 0.0f, 0.0f))
			{
				tempOrigins[i] = transform.position;
				tempMax[i] = areaMax + maxAdd;
				tempMin[i] = areaMin;
				ID = i;
				break;
			}
		}


		int tempLen = 0;
		for (int i = 0; i < tempOrigins.Length; i++)
		{
			if (tempOrigins[i] != new Vector4(0.0f, 0.0f, 0.0f, 0.0f))
			{
				tempLen++;
			}
		}

		tr.sharedMaterial.SetInt("_OriginsLength", tempLen);
		tr.sharedMaterial.SetVectorArray("_Origins", tempOrigins);
		tr.sharedMaterial.SetFloatArray("_FadeAreaMin", tempMin);
		tr.sharedMaterial.SetFloatArray("_FadeAreaMax", tempMax);
		//tree.GetComponent<SpriteRenderer>().sharedMaterial.SetVector("_FadeOrigin",transform.position);

		GetComponent<SpriteRenderer>().sortingOrder = 1;

		if (shade == null)
		{
			shade = GameObject.Find("NightShade");
			if (shade == null)
			{
				return;
			}
		}

	}


    private void OnDisable()
    {
		/**
		if(shade == null)
        {
			shade = GameObject.Find("NightShade");
			if(shade == null)
            {
				return;
            }
		}
		*/

		// This check gets around some errors, but may be created some deeper issues we haven't found yet
	//	if (shade == null || ID == -1) { return; }

	//	SpriteRenderer tr = shade.GetComponent<SpriteRenderer>();
	//	GetComponent<SpriteRenderer>().sortingOrder = -1;
		//var tempMin = tr.sharedMaterial.GetFloatArray("_FadeAreaMin");
		//var tempMax = tr.sharedMaterial.GetFloatArray("_FadeAreaMax");
		//if (tempMin != null && tempMax != null)
		//{
		//	tempMax[ID] = 0;
		//	tempMin[ID] = 0;
		//}
		//if (tr.sharedMaterial != null || ID != -1)
		//{
		//	Debug.Log(tr);
		//	Debug.Log(tr.sharedMaterial);
		//	tr.sharedMaterial.SetFloatArray("_FadeAreaMin", tempMin);
		//	tr.sharedMaterial.SetFloatArray("_FadeAreaMax", tempMax);
		//}
		//GetComponent<SpriteRenderer>().sortingOrder = 1;
	}



	public void Clear()
    {
		SpriteRenderer tr = shade.GetComponent<SpriteRenderer>();
		tr.sharedMaterial.SetVectorArray("_Origins", new Vector4[100]);
		ID = -1;
	}

}
