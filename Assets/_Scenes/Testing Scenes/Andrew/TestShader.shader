Shader "Sprites/TestShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
				 float4 vertex   : POSITION;
				 float4 color    : COLOR;
				 float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
				float4 vertex   : SV_POSITION;
				float4 objCoords : TEXCOORD1;
				fixed4 color : COLOR;
				half2 texcoord  : TEXCOORD0;
            };
			fixed4 _Color;

            v2f vert (appdata v)
            {
                v2f o;

				o.objCoords = mul(unity_ObjectToWorld, v.vertex);
				o.texcoord = v.texcoord;
				o.color = v.color * _Color;
				o.vertex = UnityObjectToClipPos(v.vertex);
                //o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
			sampler2D _AttackTex;
			vector _paintSplatters[2000];
			vector splatterOffset;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.texcoord);
                // just invert the colors
                col.rgb = 1 - col.rgb;
                col.rgb = float3(0, 0, 0);
             //   col.r = 1;

				// Find the output texture dimensions
				int outTexWidth;
				int outTexHeight;
				float2 id = i.texcoord.xy;
			//	9 * .GetDimensions(outTexWidth, outTexHeight);

				// Find the splatter texture dimensions
				//int splatTexWidth;
				//int splatTexHeight;
				//_AttackTex.GetDimensions(splatTexWidth, splatTexHeight);

				// Initialize the default color of the splatter
				float4 pixelSplatterColor = float4(0.0, 0.0, 0.0, 0.0);


				// Ensure pixel coordinates are valid
				if (id.x < 2000 && id.y < 2000)
				{


					// Iterate over splatters until a valid pixel color is acheived
				//	for (uint i = 0; i < splatters.Length; i++)
				//	{
						// Find the splatter referenced by i
					//	PaintSplatter splatter = splatters[i];

						// Find the position of the current pixel in the curent splatter
					int splatterPosX = id.x - _paintSplatters[0] + splatterOffset.x;
					int splatterPosY = id.y - _paintSplatters[1] + splatterOffset.y;

					// Create a uint2 to read texture color at that position
					int2 splatCoords = int2(splatterPosX, splatterPosY);

					// Initialize rotation
					//int2 antiRotatedSplatCoords = RotateCoordsAroundTextureCenter(splatterTexture, splatCoords, splatter.rotation);

					// If those coordinates are valid
					//if (antiRotatedSplatCoords[0] >= 0 && antiRotatedSplatCoords[1] >= 0 && antiRotatedSplatCoords[0] < splatTexWidth && antiRotatedSplatCoords[1] < splatTexHeight)
					//{
						// Gets an anti rotated texture pixel
					float4 splatTexColor = tex2D(_AttackTex, splatCoords);

					// if it is not totally transparent, treat as white
					if (splatTexColor[3] > 0)
					{
						splatTexColor[0] = 1.0;
						splatTexColor[1] = 1.0;
						splatTexColor[2] = 1.0;
					}
					else
					{
						splatTexColor = float4(0.0, 0.0, 0.0, 0.0);
					}

					// Adjust for paint color
					float4 splatterColor = splatTexColor * float4(1.0, 0.0, 0.0, 0.0);

					// Alpha blending
					if (splatterColor[3] > 0.0)
					{
						float outA = pixelSplatterColor[3] + splatterColor[3] * (1 - pixelSplatterColor[3]);
						float3 outRGB = ((float3)pixelSplatterColor * pixelSplatterColor[3] + (float3)splatterColor * splatterColor[3] * (1 - pixelSplatterColor[3])) / outA;
						if (outA == 0.0)
						{
							outRGB = float3(0.0, 0.0, 0.0);
						}
						pixelSplatterColor = float4(outRGB[0], outRGB[1], outRGB[2], outA);
						if (pixelSplatterColor[3] >= 0.9)
						{
							pixelSplatterColor[3] = 1.0;
							break;
						}
					}
					//}
				//}
				// Apply the resulting pixel to the output texture
				//outputTexture[id.xy] = pixelSplatterColor;
					col = splatterColor;
				}
                return col;
            }
            ENDCG
        }
    }
}
