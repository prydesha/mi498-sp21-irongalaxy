/** ------------------------------------------------------------------------------
- Filename:   CameraEventEditor
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/31
- Created by: smigi
------------------------------------------------------------------------------- */


using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
/** CameraEventEditor.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/31       Created file.                                       smigi
 */
[CustomPropertyDrawer(typeof(CameraEvent))]
public class CameraEventEditor : PropertyDrawer
{
    private int editorHeight;
    private Rect position;
    private Dictionary<string, int> heightProp = new Dictionary<string, int>();
    

    private Rect newRect()
    {
        var temp = new Rect(position.x , position.y + editorHeight, position.width, EditorGUIUtility.singleLineHeight);
        editorHeight += 20;
        return temp;

    }
    private Rect newRect(int height)
    {
        var temp = new Rect(position.x, position.y + editorHeight, position.width, EditorGUIUtility.singleLineHeight);
        editorHeight += height;
        return temp;

    }
    private void StartChild()
    {
        EditorGUI.indentLevel += 1;

    }
    private void StartParent()
    {
        EditorGUI.indentLevel -= 1;
    }
    private void ResetRects()
    {
        EditorGUI.indentLevel = 0;
    }
    private void MovementSettings(SerializedProperty name, SerializedProperty curve, SerializedProperty maxSpeed, SerializedProperty maxZoomOrthographic, 
        SerializedProperty stopTime, SerializedProperty camStops, SerializedProperty zoomAcceleration, SerializedProperty transitionTime, SerializedProperty isTwoWay)
    {
        EditorGUI.LabelField(newRect(), "Movement Settings", EditorStyles.boldLabel);
        StartChild();
        isTwoWay.boolValue = EditorGUI.Toggle(newRect(), "Two Way Movement", isTwoWay.boolValue);
        
       // maxSpeed.floatValue =  EditorGUI.FloatField(newRect(),"Max Speed",maxSpeed.floatValue,EditorStyles.numberField);
        curve.animationCurveValue = EditorGUI.CurveField(newRect(), "Movement", curve.animationCurveValue, Color.red, new Rect(0, 0, 1, 1));
        maxZoomOrthographic.floatValue = EditorGUI.FloatField(newRect(), "Max Orthographic Zoom", maxZoomOrthographic.floatValue, EditorStyles.numberField);
        zoomAcceleration.animationCurveValue = EditorGUI.CurveField(newRect(), "Zoom Movement", zoomAcceleration.animationCurveValue, Color.blue, new Rect(0, 0, 1, 1));

        stopTime.floatValue = EditorGUI.FloatField(newRect(), "Stop Time", stopTime.floatValue, EditorStyles.numberField);


        //EditorGUI.ObjectField(newRect(), camStops);

        EditorGUI.ObjectField(newRect(), camStops, typeof(Transform));
    }
    private void ColorSettings(SerializedProperty Colors, SerializedProperty transitionSpeed)
    {
        EditorGUI.LabelField(newRect(), "Color Settings", EditorStyles.boldLabel);
        StartChild();

        EditorGUI.PropertyField(newRect(), Colors);
        transitionSpeed.animationCurveValue = EditorGUI.CurveField(newRect(), "Fade Transition", transitionSpeed.animationCurveValue, Color.yellow, new Rect(0, 0, 1, 1));

    }
    
    
    public override void OnGUI(Rect newposition, SerializedProperty property, GUIContent label)
    {
        position = newposition;
       // var headerRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        EditorGUI.BeginProperty(position, label, property);
        //EditorGUI.BeginFoldoutHeaderGroup(rects.Dequeue(), false,"tetst");
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        editorHeight = 0;

        //movement

        var type = property.FindPropertyRelative("_type");
        var name = property.FindPropertyRelative("eventName");
        var movementCurve = property.FindPropertyRelative("movement");
        var maxSpeed = property.FindPropertyRelative("maxSpeed");
        var maxZoomOrthographic = property.FindPropertyRelative("maxZoomOrthographic");
        var zoomMovement = property.FindPropertyRelative("zoomMovement");
        var stopTime = property.FindPropertyRelative("stopTime");
        var camStops = property.FindPropertyRelative("camStop");
        var transitionTime = property.FindPropertyRelative("transitionTime");
        var stopPlayerMovement = property.FindPropertyRelative("stopPlayerMovement");
        var isTwoWay = property.FindPropertyRelative("isTwoWay");

        var Colors = property.FindPropertyRelative("Colors"); ;
        var transitionSpeed = property.FindPropertyRelative("transitionSpeed"); ;



        property.isExpanded = EditorGUI.Foldout(newRect(), property.isExpanded, "Camera Event");
       // EditorGUI.ObjectField(newRect(),objects,typeof(Object));
        if (property.isExpanded)
        {
            StartChild();
            name.stringValue = EditorGUI.TextField(newRect(), "Event Name", name.stringValue);
            transitionTime.floatValue = EditorGUI.FloatField(newRect(), "Transition Time", transitionTime.floatValue, EditorStyles.numberField);
            stopPlayerMovement.boolValue = EditorGUI.Toggle(newRect(), "Stop Player Movement", stopPlayerMovement.boolValue);
            EditorGUI.PropertyField(newRect(), type);
            // EditorGUI.EndFoldoutHeaderGroup();
            switch ((CameraEvent.CameraEventType)type.intValue)
            {
                case CameraEvent.CameraEventType.Moving:
                    MovementSettings(name, movementCurve, maxSpeed, maxZoomOrthographic, stopTime, camStops, zoomMovement, transitionTime, isTwoWay);
                    //Anything else you want to display

                    break;
                case CameraEvent.CameraEventType.ColorChange:

                    ColorSettings(Colors, transitionSpeed);
                    //Anything else you want to display
                    break;
                case CameraEvent.CameraEventType.All:
                    MovementSettings(name, movementCurve, maxSpeed, maxZoomOrthographic, stopTime, camStops, zoomMovement,transitionTime, isTwoWay);
                    StartParent();
                    ColorSettings(Colors, transitionSpeed);

                    //Anything else you want to display
                    break;
            }
            
        }

        heightProp[property.propertyPath] = editorHeight;
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
        
    }
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (heightProp.ContainsKey(property.propertyPath))
            return base.GetPropertyHeight(property, label) + heightProp[property.propertyPath];
        else
            return base.GetPropertyHeight(property, label);
    }

}
