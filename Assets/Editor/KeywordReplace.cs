﻿//Assets/Editor/KeywordReplace.cs
using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public class KeywordReplace : UnityEditor.AssetModificationProcessor
{
    // Called whenever a new hierarchy object is created
    public static void OnWillCreateAsset(string path)
    {
        // skip meta files
        path = path.Replace(".meta", "");
        int index = path.LastIndexOf(".");
        string file = path.Substring(index);
        // skip non-script files
        if (file != ".cs" && file != ".js" && file != ".boo") return;
        index = Application.dataPath.LastIndexOf("Assets");
        path = Application.dataPath.Substring(0, index) + path;
        file = System.IO.File.ReadAllText(path);

        file = file.Replace("#CREATIONDATE#", System.DateTime.Now.ToString("yyyy/MM/dd"));
        file = file.Replace("#PROJECTNAME#", PlayerSettings.productName);
        file = file.Replace("#SMARTDEVELOPERS#", PlayerSettings.companyName);
        file = file.Replace("#USERNAME#", Environment.UserName);

        System.IO.File.WriteAllText(path, file);
        AssetDatabase.Refresh();
    }
}
