/** ------------------------------------------------------------------------------
- Filename:   PaintManagerV2
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/09
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Linq;
/** PaintManagerV2.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/09       Created file.                                       smigi
 */

public class MultiLayerPaintManagerV2 : PaintManagerBase
{
    /* PUBLIC MEMBERS */

    public SpriteRenderer sr;
    public CustomRenderTexture rt;
    /* PRIVATE MEMBERS */

    private Coroutine testCo;
    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this ssssssssssssssssssssssscomment)

    /* METHODS */



    override public void Draw(Vector2 pos, float angle, float Scale, Texture2D AttackTex, Color color )
    {
        UnityEvent test = new UnityEvent();
        test.AddListener(() => DrawPaint(pos, angle, Scale, AttackTex, color));
        DrawCalls.Enqueue(test);
    }


    override public void Clear(Color clearColor)
    {
        RenderTexture oldActive = RenderTexture.active;
        RenderTexture.active = rt;
        GL.Clear(true, true, clearColor);
        RenderTexture.active = oldActive;
        rt.material.SetVector("testSplatter", new Vector4(500000,50000,0,0));

    }
    private IEnumerator AsorbPaint()
    {
        while (Application.isPlaying)
        {
            var asorbArray = rt.material.GetVectorArray("_asorbers");
            var colorArray = rt.material.GetVectorArray("_asorbingColors");

            if(asorbArray == null)
            {
                asorbArray = new Vector4[10];
                colorArray = new Vector4[10];
            }
            int i = 0;
         //   foreach (PaintDetectionCamera dc in pdc)
         //   {
         //       if (dc.GetColors().Count() > 0)
        //        {
        //            Color asorbColor = dc.GetColors().First();
//
        //            asorbArray[i] = new Vector4(dc.transform.position.x, dc.transform.position.y);
       //             colorArray[i] = asorbColor;
       //             i++;
       //         }
        //    }
            rt.material.SetVectorArray("_asorbers",asorbArray);
            rt.material.SetVectorArray("_asorbingColors",colorArray);

         //   rt.Update();
            yield return new WaitForSecondsRealtime(.05f);
        }
    }
    private void DrawPaint(Vector2 pos, float angle, float Scale, Texture2D AttackTex, Color color)
    {

        Vector4 sp = new Vector4(pos.x, pos.y, angle, Scale);
        rt.material.SetTexture("_AttackTex", AttackTex);
        rt.material.SetVector("_AttackColor", color);
        rt.material.SetVector("testSplatter", sp);
        
    }
    override protected IEnumerator UpdateCRTS()
    {
        rt.Update();
        yield return null;
        rt.material.SetVector("testSplatter", new Vector4(500000, 50000, 0, 0));

    }

    #region Unity Functions

    // Start is called before the first frame update
    override protected void Start()
    {
        


        if (clearOnStart && rt != null)
        {
            Clear(Color.clear);
        }

        
        rt.material.SetVector("_texPos", transform.position);
        rt.material.SetFloat("_texScale", sr.bounds.size.x);
        rt.material.SetFloat("_asorbRate", asorbAmt);

        rt.material.SetTexture("_RenderTex", rt);
    }

    // Update is called once per frame
    override protected void Update()
    {
        if (InputManager.Paint.justPressed && drawOnMouseDown)
        {
            var tempPos = new Vector3(InputManager.MousePosition.x, InputManager.MousePosition.y, 0);


            Draw(Camera.main.ScreenToWorldPoint(tempPos), 0, 2, attackTexs[0], Color.grey);

        }
        if (InputManager.Jab.justPressed && drawOnMouseDown)
        {
            Clear(Color.clear);
        }
    }
    private void Awake()
    {
        if (PM != null)
        {
            Destroy(this.gameObject);
        }
        PM = this;
        //pdc = FindObjectsOfType(typeof(PaintDetectionCamera)) as PaintDetectionCamera[];
       // testCo = StartCoroutine(AsorbPaint());
    }
    #endregion
}


