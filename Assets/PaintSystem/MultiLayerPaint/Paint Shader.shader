Shader "CustomRenderTexture/PaintShader"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex("Texture", 2D) = "white" {}
        _AttackTex("Attack tex", 2D) = "transparent" {}
        _MaxSplatters("Max Splatters", Range(0,1000)) = 1000

    }

        SubShader
    {
         Cull Off
         Lighting Off
         ZWrite Off
         Fog { Mode Off }
         Blend One OneMinusSrcAlpha
       Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }
       Pass
       {
           CGPROGRAM
           #include "UnityCustomRenderTexture.cginc"
           #pragma vertex CustomRenderTextureVertexShader
           #pragma fragment frag
           #pragma target 3.0
           //Color of the attack 
           float4     _AttackColor;
           //texture we are going to draw on 
           sampler2D _MainTex;
           //Greyscale texture of what we are drawing
           sampler2D _AttackTex;

           sampler2D _RenderTex;
           //world pos of the main texture
           float3 _texPos = float3(0,0,0);
           //width of texture we are drawing on in world coords
           float _texScale = 10.0;
           //Data about the attack (pos X, pos Y, Angle, Scale)
           vector testSplatter;

           vector _asorbers[10];

           vector _asorbingColors[10];

           float _asorbRate  = .1;
           float _asorbRange = .5f;

           float4 frag(v2f_customrendertexture IN) : COLOR
           {
                //Convert pixel coords to global world coords
                float2 globalCoords = float2((IN.globalTexcoord.x*_texScale)- (_texScale / 2.0) + (_texPos.x) ,(IN.globalTexcoord.y * _texScale)- (_texScale / 2) + (_texPos.y));


                //scale for attack
                float scale = testSplatter[3];
                //pixel we are drawing on
                fixed4 col = tex2D(_RenderTex, IN.globalTexcoord.xy);
                half2 tempPos = half2(testSplatter.xy);
                float pixDist = distance(tempPos.xy, globalCoords.xy);
                half2 tempDist = half2(tempPos.x - globalCoords.x, tempPos.y - globalCoords.y);



                //Drawing Pixels
                if (pixDist < scale)
                {
                    float phi = ((3.14159265359 / 180.0) * (testSplatter[2]));
                    float sn = sin(phi);
                    float cs = cos(phi);
                    uint W = _texScale;
                    uint H = _texScale;
                    float xc = (float)(W / 2);
                    float yc = (float)(H / 2);

                    float3 coords = float3((tempDist.x), (tempDist.y), 0.0);

                    float3x3 rotationMatrix = {
                        cs, sn, 0.0,
                        -sn, cs, 0.0,
                        0.0, 0.0, 1.0
                    };

                    coords = mul(rotationMatrix, coords);

                    tempDist = float2(coords[0], coords[1]);

                    tempDist = tempDist / scale;
                    tempDist.x = tempDist.x + .5;
                    tempDist.y = tempDist.y + .5;
                    if (tempDist.x > 1 || tempDist.x < 0)
                    {
                        return float4(0, 0, 0, 0);
                    }
                    if (tempDist.y > 1 || tempDist.y < 0)
                    {
                        return float4(0, 0, 0, 0);
                    }
                    fixed4 attackcol = tex2D(_AttackTex, tempDist) ;
                    
                   // if (_AttackColor.r == col.r && _AttackColor.b == col.b && _AttackColor.g == col.g)
                 //   {
                   //     col.a = col.a + attackcol.a;
                 //       return col;
                 //   }
                    if (attackcol.a > 0.2)
                    {
                        float4 tempCol = _AttackColor;
                        tempCol.a = attackcol.a;
                        return tempCol;
                    }
                }
                float curAlpha = col.a;
               // for (int i = 0; i < 10; i++)
                //{
               //     if (distance(_asorbers[i].xy, globalCoords) < _asorbRange)
              //      {
                //        float4 colortest = _asorbingColors[i];
               //         colortest.a = 0;
                        //return float4(1, 1, 0, 1);
              //           curAlpha = .5;
                        
               //     }
               // }
              //  if (curAlpha != col.a)
              //  {
                //    col.a = curAlpha;
              //      return float4(col.r, col.b, col.g, 0);
               // }
                
                
               return float4(0,0,0,0);
           }
           ENDCG
           }
    }
}