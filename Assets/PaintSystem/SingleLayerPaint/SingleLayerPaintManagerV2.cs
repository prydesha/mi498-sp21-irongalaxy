/** ------------------------------------------------------------------------------
- Filename:   PaintManagerV2
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/09
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
/** PaintManagerV2.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/09       Created file.                                       smigi
 */

public class SingleLayerPaintManagerV2 : PaintManagerBase
{
    /* PUBLIC MEMBERS */
    public static List<PaintDetectionCamera> PDCS = new List<PaintDetectionCamera>();



    /* PRIVATE MEMBERS */
    [Tooltip("The range % at which the absorbtion rate starts to drop off")]
    [SerializeField] private float absorbMinPercent = .2f;
    [Tooltip("The sprite render of the canvas")]
    [SerializeField] private SpriteRenderer sr;
    [Tooltip("The size of the canvas")]
    [SerializeField] private Vector2 size;
    [HideInInspector]
    public CustomRenderTexture DrawingCRT { get; private set; }
    [HideInInspector]
    public CustomRenderTexture StorageCRT { get; private set; }

    [Tooltip("If you are changing this talk to a programmer")]
    [SerializeField] private Material DrawingMaterial;
    [Tooltip("If you are changing this talk to a programmer")]
    [SerializeField] private Material StorageMaterial;

    private bool attackChange = false;
    private Texture2D lastTex = null;
    private Coroutine safteyNet = null;
    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 

    /* METHODS */



    override public void Draw(Vector2 pos, float angle, float Scale, Texture2D AttackTex, Color color )
    {
        UnityEvent test = new UnityEvent();
        test.AddListener(() => DrawPaint(pos, angle, Scale, AttackTex, color));
        DrawCalls.Enqueue(test);
    }


    override public void Clear(Color clearColor)
    {
        RenderTexture oldActive = RenderTexture.active;
        RenderTexture.active = DrawingCRT;
        GL.Clear(true, true, clearColor);
        RenderTexture.active = oldActive;
        DrawingCRT.material.SetVector("testSplatter", new Vector4(500000,50000,0,0));

    }
    public void rtClear(RenderTexture toClear)
    {
        RenderTexture oldActive = RenderTexture.active;
        RenderTexture.active = toClear;
        GL.Clear(true, true, Color.clear);
        RenderTexture.active = oldActive;
        DrawingCRT.material.SetVector("testSplatter", new Vector4(500000, 50000, 0, 0));

    }
    override protected void SetAsorbPaint()
    {

        var asorbArray = DrawingCRT.material.GetVectorArray("_asorbers");
        var colorArray = DrawingCRT.material.GetVectorArray("_asorbingColors");

        if(asorbArray == null)
        {
            asorbArray = new Vector4[10];
            colorArray = new Vector4[10];
        }
        for(int i = 0; i< asorbArray.Length- 1;i++)
        {
            if(i <= PDCS.Count- 1)
            {
                asorbArray[i] = new Vector4(PDCS[i].transform.position.x, PDCS[i].transform.position.y, PDCS[i].absorbtionRange, PDCS[i].absorbtionRate);
                if (PDCS[i].GetColors().Count() > 0)
                {
                    Color asorbColor = PDCS[i].GetColors().First();
                    colorArray[i] = asorbColor;

                }
                else
                {
                    colorArray[i] = Color.clear;
                }
            }
            else
            {
                asorbArray[i] = new Vector4(0, 0, 0);
            }

           // asorbArray[i] = new Vector4(dc.transform.position.x, dc.transform.position.y, dc.absorbtionRange);

        }
        DrawingCRT.material.SetVectorArray("_asorbers",asorbArray);
        DrawingCRT.material.SetVectorArray("_asorbingColors",colorArray);
    }

    override protected IEnumerator UpdateCRTS()
    {
        //DrawingCRT.material.SetTexture("_AttackTex", attackTexs[0]);
        //Updating the CRT's
        
        if (!attackChange || (Application.platform != RuntimePlatform.WindowsPlayer && Application.platform != RuntimePlatform.WindowsEditor))
        {
            DrawingCRT.Update();
        }
        yield return null;
        rtClear(StorageCRT);
        StorageCRT.Update();
        yield return null;
        rtClear(DrawingCRT);
        attackChange = false;
        DrawingCRT.material.SetVector("testSplatter", new Vector4(500000, 50000, 0, 0));
    }




    private void DrawPaint(Vector2 pos, float angle, float Scale, Texture2D AttackTex, Color color)
    {
        if (lastTex == null)
        {
            lastTex = AttackTex;
            if(DrawingCRT.material.GetTexture("_AttackTex") != AttackTex)
            {
                DrawingCRT.material.SetTexture("_AttackTex", AttackTex);
                attackChange = true;
            }
        }
        if (lastTex != AttackTex)
        {
            DrawingCRT.material.SetTexture("_AttackTex", AttackTex);
            lastTex = AttackTex;
            attackChange = true;
        }
        Vector4 sp = new Vector4(pos.x, pos.y, angle, Scale);
        DrawingCRT.material.SetVector("_AttackColor", color);
        DrawingCRT.material.SetVector("testSplatter", sp);

    }




    #region Unity Functions

    // Start is called before the first frame update
    override protected void Start()
    {
        sr.enabled = true;
        if (clearOnStart && StorageCRT != null)
        {
            Clear(Color.clear);
        }

        //DrawingMaterial = new Material(DrawingMaterial);
       // StorageMaterial = new Material(StorageMaterial);

        DrawingCRT = new CustomRenderTexture((int)size.x, (int)size.y);
        DrawingCRT.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
        DrawingCRT.format = RenderTextureFormat.ARGB32;
        DrawingCRT.initializationMode = CustomRenderTextureUpdateMode.OnDemand;
        DrawingCRT.initializationSource = CustomRenderTextureInitializationSource.Material;
        DrawingCRT.updateMode = CustomRenderTextureUpdateMode.OnDemand;
        DrawingCRT.filterMode = FilterMode.Point;

        StorageCRT = new CustomRenderTexture((int)size.x, (int)size.y);
        StorageCRT.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
        StorageCRT.format = RenderTextureFormat.ARGB32;
        StorageCRT.initializationMode = CustomRenderTextureUpdateMode.OnDemand;
        StorageCRT.initializationSource = CustomRenderTextureInitializationSource.Material;
        StorageCRT.updateMode = CustomRenderTextureUpdateMode.OnDemand;
        StorageCRT.filterMode = FilterMode.Point;

        DrawingCRT.material = new Material(DrawingMaterial); 
        DrawingCRT.material.SetVector("testSplatter", new Vector4(500000, 50000, 0, 0));
        DrawingCRT.material.SetTexture("_MainTex", StorageCRT);


        StorageCRT.material = new Material(StorageMaterial); 
        StorageCRT.material.SetTexture("_RenderTex", DrawingCRT);



        GetComponent<SpriteRenderer>().material.SetTexture("_SecondaryTex", StorageCRT);

        
        DrawingCRT.material.SetVector("_texPos", transform.position);
        DrawingCRT.material.SetFloat("_texScale", sr.bounds.size.x);
        DrawingCRT.material.SetFloat("_asorbRate", asorbAmt);
        DrawingCRT.material.SetTexture("_RenderTex", StorageCRT);
        DrawingCRT.material.SetFloat("_absorbMinPercent", absorbMinPercent);

        StorageCRT.Release();
        if (safteyNet == null)
            safteyNet = StartCoroutine(StartCanvas());
    }

    // Update is called once per frame
    override protected void Update()
    {
        if (InputManager.Paint.justPressed && drawOnMouseDown)
        {
            var tempPos = new Vector3(InputManager.MousePosition.x, InputManager.MousePosition.y, 0);


            Draw(Camera.main.ScreenToWorldPoint(tempPos), 0, 5, attackTexs[0], PaintColorAuthoritative.GetColor(PaintColorAuthoritative.PaintColorEnum.Yellow));

        }
        if (InputManager.Jab.justPressed && drawOnMouseDown)
        {
            Clear(Color.clear);
        }
    }

    #endregion
}



