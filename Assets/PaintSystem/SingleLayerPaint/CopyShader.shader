Shader "CustomRenderTexture/CopyCRTShader"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _RenderTex("Secondary tex", 2D) = "transparent" {}

    }

        SubShader
    {
         Cull Off
         Lighting Off
         ZWrite Off
         Fog { Mode Off }
         Blend One OneMinusSrcAlpha
       Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }
       Pass
       {
           CGPROGRAM
           #include "UnityCustomRenderTexture.cginc"
           #pragma vertex CustomRenderTextureVertexShader
           #pragma fragment frag
           #pragma target 3.0

           //texture we are going to draw on 
           sampler2D _MainTex;


           sampler2D _RenderTex;


           float4 frag(v2f_customrendertexture IN) : COLOR
           {

                //pixel we are drawing on
                fixed4 col = tex2D(_RenderTex, IN.globalTexcoord.xy);

                return col;
           }
           ENDCG
           }
    }
}