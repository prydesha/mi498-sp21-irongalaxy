/** ------------------------------------------------------------------------------
- Filename:   PaintManagerBase
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/03/06
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/** PaintManagerBase.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/06       Created file.                                       smigi
 */
public class PaintManagerBase : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    [HideInInspector]
    public static PaintManagerBase PM = null;



    /* PRIVATE MEMBERS */
    [Tooltip("Will draw textures from the attack texs List where ever you click (for dev)")]
    [SerializeField] protected bool drawOnMouseDown = true;
    [Tooltip("Will clear the CRT's on start")]
    [SerializeField] protected bool clearOnStart = false;
    [Tooltip("The Draw rate of the CRT, This also is the rate at which paint will be drawn and absorbed")]
    [SerializeField] protected float CanvasUpdateRate = .05f;
    [Tooltip("The amount of paint absorbed per canvas tick")]
    [SerializeField] protected float asorbAmt;
    [Tooltip("The attack textures that are used when drawing on click")]
    [SerializeField] protected List<Texture2D> attackTexs;
    protected Queue<UnityEvent> DrawCalls = new Queue<UnityEvent>();
    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */
    virtual public void Draw(Vector2 pos, float angle, float Scale, Texture2D AttackTex, Color color) { }

    virtual public void Clear(Color clearColor) { }
    protected IEnumerator PaintBuffer()
    {
        while (Application.isPlaying)
        {

            if (DrawCalls.Count > 0)
            {
                //yield return StartCoroutine(DrawCalls.Dequeue());
            }

            yield return null;
        }
    }

    virtual protected void SetAsorbPaint()
    {

    }
    virtual protected IEnumerator UpdateCRTS()
    {
        yield return null;
    }

    protected IEnumerator StartCanvas()
    {
        while (Application.isPlaying)
        {
            SetAsorbPaint();

            if (DrawCalls.Count > 0)
            {
              DrawCalls.Dequeue().Invoke();
            }

            yield return StartCoroutine(UpdateCRTS());
            yield return new WaitForSeconds(CanvasUpdateRate);
        }
    }

    #region Unity Functions


    // Update is called once per frame
    virtual protected void Start()
    {

    }

    // Update is called once per frame
    virtual protected void Update()
    {

    }
    private void Awake()
    {

        PM = this;

    }
    #endregion
}
