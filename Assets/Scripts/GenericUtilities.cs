/** ------------------------------------------------------------------------------
- Filename:   GenericUtilities
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/08
- Created by: shawn
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/** GenericUtilities.cs
 * --------------------- Description ------------------------
 * This class is designed to be a place to store 
 * generic static functions which could be used by any script
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/08       Created file.                                       shawn
 */
public static class GenericUtilities
{
	/// <summary>
	/// Turn a 2d transform so that its bottom is
	/// facing the specified 2 dimensional point
	/// </summary>
	/// <param name="trans"></param>
	/// <param name="lookAtPoint"></param>
	/// <param name="snapToCardinal">If set to true, the rotation will be clamped to its closest
	/// cardinal direction (including intermediate cardinal directions) before assigning to the transform</param>
	public static void LookAt2D(this Transform trans, Vector2 lookAtPoint, bool snapToCardinal = false)
	{
		// source: https://answers.unity.com/questions/585035/lookat-2d-equivalent-.html
		// this source is only applicable to the first 3 lines and last line
		Vector3 originToPoint = (Vector3)lookAtPoint - trans.position;
		originToPoint.Normalize();
		float zRotation = Mathf.Atan2(originToPoint.y, originToPoint.x) * Mathf.Rad2Deg;
		if (snapToCardinal)
		{
			const float cardinalInterval = 45f;
			float halfCardinal = cardinalInterval / 2;
			float remain = Mathf.Abs(zRotation) % cardinalInterval;
			bool positive = zRotation >= 0;
			// closer to lower bound
			if (remain <= halfCardinal)
			{
				zRotation -= positive ? remain : -remain;
			}
			// closer to upper bound
			else
			{
				float change = cardinalInterval - remain;
				zRotation -= positive ? -change : change;
			}
			
		}
		trans.rotation = Quaternion.Euler(0f, 0f, zRotation + 90f);
	}
	
	/// <summary>
	/// Convert a floating point angle (in degrees) 
	/// to a normalized vector2. 
	/// Example: 0 equates to [1, 0], 90 equates to [0, 1]
	/// </summary>
	/// <param name="degrees"></param>
	/// <returns></returns>
	public static Vector2 Deg2Vector(this float degrees)
	{
		degrees = degrees % 360;
		float radians = degrees * Mathf.Deg2Rad;
		return new Vector2((float)Mathf.Cos(radians), (float)Mathf.Sin(radians)).normalized;
	}

	/// <summary>
	/// Returns true if the parameter layer is part of the calling
	/// layer mask
	/// </summary>
	/// <param name="mask"></param>
	/// <param name="layer"></param>
	/// <returns></returns>
	public static bool ContainsLayer(this LayerMask mask, int layer)
	{
		return ((1<<layer) & mask) != 0;
	}
	
	/// <summary>
	/// Changes the dimensions of an image to best suit
	/// a provided sprite and the image's original dimensions
	/// </summary>
	/// <param name="image">the image element to resize</param>
	/// <param name="sprite">the sprite to base our new dimensions from</param>
	/// <param name="targetSize">most often, this should be the smallest default dimension of the
	/// provided image (when the game starts)</param>
	public static void ResizeImage(Image image, Sprite sprite, float targetSize)
	{
		const float increaseBuffer = 1.14f;
		if (sprite.rect.height >= sprite.rect.width)
		{
			// Then our preview will be maxed vertically and shrunk horizontally
			image.rectTransform.sizeDelta = new Vector2((sprite.rect.width * targetSize / sprite.rect.height) * increaseBuffer, 
				targetSize * increaseBuffer);
		}
		else
		{
			// Else our building is wider than tall so it will be maxed horizontally and shrunk vertically
			image.rectTransform.sizeDelta = new Vector2(targetSize * increaseBuffer, 
				(sprite.rect.height * targetSize / sprite.rect.width) * increaseBuffer);
		}
	}
}
