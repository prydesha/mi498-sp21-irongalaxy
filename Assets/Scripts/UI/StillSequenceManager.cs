/** ------------------------------------------------------------------------------
- Filename:   StillSequenceManager
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/04/05
- Created by: shawn
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/** StillSequenceManager.cs
 * --------------------- Description ------------------------
 * Script which displays and keeps track of a series of
 * still images being displayed to the player
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/04/05       Created file.                                       shawn
 */
[RequireComponent(typeof(Canvas))]
public class StillSequenceManager : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    [SerializeField] private bool _showOnAwake = false;
    [SerializeField] private Image _display = null;
    [SerializeField] private Image _background = null;
    [SerializeField] private List<SpriteTimer> _imageSequence = null;
    [SerializeField] private float _transitionTime = 2f;
    [Range(0.9f, 1.1f)]
    [SerializeField] private float _zoomScale = 1.05f;
    public UnityEvent OnSequenceBegin;
    [Tooltip("Event called when this sequence stops showing")]
    public UnityEvent OnSequenceEnd;
    
    /* PRIVATE MEMBERS */
    // used to resize the display variable for different images
    private float _targetDimension = 1f;
    private Canvas _canvas = null;
    private int _sequenceIndex = 0;
    private bool _showingSequence = false;
    private Coroutine _sequenceRoutine = null;
    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    /// <summary>
    /// Begins displaying this sequence of images
    /// </summary>
    /// <returns>false if the sequence was already showing (does not restart)</returns>
    public bool ShowSequence()
    {
	    if (_showingSequence)
	    {
		    return false;
	    }
	    _canvas.enabled = true;
	    _showingSequence = true;
	    if (_sequenceRoutine != null)
	    {
		    StopCoroutine(_sequenceRoutine);
	    }
	    _sequenceIndex = -1;
	    _sequenceRoutine = StartCoroutine(DisplaySequence());
	    InputManager.SwitchInputType(InputManager.ACTION_MAP_UI);
	    OnSequenceBegin?.Invoke();
	    return true;
    }

    /// <summary>
    /// Stops showing this sequence and performs the standard
    /// behavior associated with ending the image display
    /// </summary>
    public void SkipSequence()
    {
	    if (!_showingSequence)
	    {
		    return;
	    }
	    if (_sequenceRoutine != null)
	    {
		    StopCoroutine(_sequenceRoutine);
	    }
	    _sequenceIndex = _imageSequence.Count - 1;
	    _sequenceRoutine = StartCoroutine(DisplaySequence());
    }

    /// <summary>
    /// Coroutine which transitions between images over time
    /// </summary>
    /// <returns></returns>
    private IEnumerator DisplaySequence()
    {
	    _display.gameObject.SetActive(true);
	    while (_sequenceIndex < _imageSequence.Count)
	    {
		    // fade out
		    if (_sequenceIndex >= 0)
		    {
			    yield return StartCoroutine(Fade(_display, Color.white, Color.black, _transitionTime / 2));
		    }
		    // switch
		    _sequenceIndex++;
		    if (_sequenceIndex < _imageSequence.Count)
		    {
			    _display.transform.localScale = Vector3.one;
			    _display.sprite = _imageSequence[_sequenceIndex].Image;
			    GenericUtilities.ResizeImage(_display, _imageSequence[_sequenceIndex].Image, _targetDimension);
			    // fade in
			    yield return StartCoroutine(Fade(_display, Color.black, Color.white, _transitionTime / 2));
			    
			    // wait for next switch
			    float timer = _imageSequence[_sequenceIndex].DisplayTime;
			    while (timer > 0)
			    {
				    timer -= Time.deltaTime;
				    var ogScale = _display.transform.localScale;
				    _display.transform.localScale = Vector3.Lerp(ogScale, 
					    ogScale * _zoomScale, Time.deltaTime);
				    yield return null;
				    // check to skip to next image
				    if (InputManager.Select.justPressed || InputManager.Paint.justPressed)
				    {
					    break;
				    }
				    // check to skip to end of sequence
				    if (InputManager.Pause.justPressed || InputManager.UnPause.justPressed)
				    {
					    SkipSequence();
					    break;
				    }
			    }
		    }
		    else
		    {
			    // fade the background
			    Color start = _background.color;
			    Color end = start;
			    end.a = 0;
			    _display.gameObject.SetActive(false);
			    yield return StartCoroutine(Fade(_background, start, end, _transitionTime / 2));
		    }
	    }
	    // wrap up
	    _showingSequence = false;
	    _canvas.enabled = false;
	    _sequenceRoutine = null;
	    InputManager.SwitchInputType(InputManager.ACTION_MAP_INGAME);
	    OnSequenceEnd?.Invoke();
    }

    /// <summary>
    /// Fade in or out our image display
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="duration"></param>
    /// <returns></returns>
    private IEnumerator Fade(Image image, Color start, Color end, float duration)
    {
	    float t = 0;
	    while (t <= duration)
	    {
		    if (image == _display)
		    {
			    var ogScale = _display.transform.localScale;
			    _display.transform.localScale = Vector3.Lerp(ogScale, 
				    ogScale * _zoomScale, Time.deltaTime);
		    }
		    t += Time.deltaTime;
		    image.color = Color.Lerp(start, end, t / duration);
		    yield return null;
	    }
    }

    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {
	    _canvas = GetComponent<Canvas>();
	    _targetDimension = _display.rectTransform.rect.height;

	    if (_showOnAwake)
	    {
		    ShowSequence();
	    }
    }

    #endregion

    [System.Serializable]
    public class SpriteTimer
    {
	    public Sprite Image;
	    [Min(1f)]
	    public float DisplayTime;
    }
}
