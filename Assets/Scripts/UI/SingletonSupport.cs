/** ------------------------------------------------------------------------------
- Filename:   SingletonSupport
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/31
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** SingletonSupport.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/31       Created file.                                       smigi
 */
public class SingletonSupport : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    public void LoadScene(string scene)
    {
        LevelManager.LM.LoadScene(scene);
    }
    public void QuitGame()
    {
        LevelManager.LM.QuitGame();
    }

    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #endregion
}
