/** ------------------------------------------------------------------------------
- Filename:   SceneTransition
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/31
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** SceneTransition.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/31       Created file.                                       smigi
 */
public class SceneTransition : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public string scene;
    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */


    public IEnumerator SwitchScenes()
    {

        

        yield return CameraManager.Instance.CallEvent("OutScene");

        LevelManager.LM.LoadScene(scene);
        yield return null;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.layer == 7)
        {

            StartCoroutine(SwitchScenes());
            
        }
    }


}
