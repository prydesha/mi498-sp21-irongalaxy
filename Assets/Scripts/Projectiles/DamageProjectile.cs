/** ------------------------------------------------------------------------------
- Filename:   DamageProjectile
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/18
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** DamageProjectile.cs
 * --------------------- Description ------------------------
 * Variant of projectiles that does damage
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/18       Created file.                                       Jonathan
 * 2021/03/31		Added SFX											Shawn
 */
public class DamageProjectile : BaseProjectile
{
    /* PUBLIC MEMBERS */
    public enum EffectLocations
    {
        atParticle, atTarget
    }

    /* PRIVATE MEMBERS */
    [Header("Damage Settings")]
    [Tooltip("Damage dealt by this projectile")]
    [SerializeField] private float _damage = 3;
    [Tooltip("Whether to deal damage to invulnerable targets")]
    [SerializeField] private bool _dealDamageThroughInvulnerability = true;
    [Tooltip("A multiplier applied to damage when hitting an invulnerable target")]
    [SerializeField] private float damageMultiplierIfInvincible = 1.0f;
    [Tooltip("The effect to spawn when dealing damage through invulnerability")]
    [SerializeField] private GameObject _brokenInvulnerabilityEffect = null;
    [SerializeField] private string _brokenInvulnerabilitySFX = ""; 
    [Tooltip("Whether to spawn the broken invulnerability effect at the projecile's position, or what it hit")]
    [SerializeField] private EffectLocations _brokenInvulnerabilityEffectLocation = EffectLocations.atTarget;

    /* PROPERTIES */

    /* METHODS */

    /// <summary>
    /// Override of BaseProjectile.Fire
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="direction"></param>
    /// <param name="overrideColor"></param>
    /// <param name="speed"></param>
    public override void Fire(Vector2 startPosition, Vector2 direction, Color overrideColor, float speed = 0)
    {
        base.Fire(startPosition, direction, overrideColor, speed);
    }

    /// <summary>
    /// Description:
    /// Handles collision events
    /// </summary>
    /// <param name="obj"></param>
    protected override void HandleCollision(GameObject obj)
    {
        base.HandleCollision(obj);
        if (DamageObject(obj))
        {
            Destroy(gameObject);
            SelfDestruct();
        }
    }

    /// <summary>
    /// Description:
    /// Deals damage to a health component on the parameter object
    /// </summary>
    /// <param name="obj"></param>
    protected virtual bool DamageObject(GameObject obj)
    {
        Health health = obj.GetComponentInParent<Health>();
        if (health != null)
        {
            float damage = _damage * (health.DamageImmune ? damageMultiplierIfInvincible : 1.0f);
            health.Damage(damage, _dealDamageThroughInvulnerability, transform.position, projectileRigidbody.velocity);
            if (_dealDamageThroughInvulnerability && health.DamageImmune)
            {
                if (_brokenInvulnerabilityEffect != null)
                {
                    Vector2 position = Vector2.zero;
                    switch(_brokenInvulnerabilityEffectLocation)
                    {
                        case EffectLocations.atTarget:
                            position = health.transform.position;
                            break;
                        case EffectLocations.atParticle:
                            position = transform.position;
                            break;
                        default:
                            Debug.LogError("Invalid location");
                            break;
                    }
                    GameObject effect = GameObject.Instantiate(_brokenInvulnerabilityEffect, position, Quaternion.identity, null);
                    if (_brokenInvulnerabilitySFX != "")
                    {
	                    AudioManager.Play(_brokenInvulnerabilitySFX);
                    }
                }
            }
            return true;
        }
        return false;
    }
}
