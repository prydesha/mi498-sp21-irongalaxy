/** ------------------------------------------------------------------------------
- Filename:   BaseProjectile
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/18
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** BaseProjectile.cs
 * --------------------- Description ------------------------
 * Base class for projectiles
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/18       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(Rigidbody2D))]
public class BaseProjectile : MonoBehaviour
{
    /* PUBLIC MEMBERS */

    // Static list of all projectiles (allows avoiding GetComponent() calls on projectiles)
    protected static Dictionary<GameObject, BaseProjectile> projectiles = new Dictionary<GameObject, BaseProjectile>();

    /* PRIVATE MEMBERS */
    [Header("Base Projectile Properties")]
    [Tooltip("Default speed of the projectile")]
    [SerializeField] private float defaultSpeed = 4f;
    [Tooltip("Effect to spawn when destroying self")]
    [SerializeField] private GameObject destructionEffect = null;
    [Tooltip("Layers which cannot be affected by this projectile")]
    [SerializeField] private LayerMask ignoredLayers;
    // The rigidbody of the projectile
    private Rigidbody2D _projectileRigidbody = null;

    /* PROPERTIES */
    // Accessor for the rigidbody of the projectile
    protected Rigidbody2D projectileRigidbody
    {
        get
        {
            if (_projectileRigidbody == null)
            {
                _projectileRigidbody = GetComponent<Rigidbody2D>();
            }
            return _projectileRigidbody;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// When starting up, add this gameobject to the list of all projecitles
    /// </summary>
    private void Awake()
    {
        Setup();
    }

    /// <summary>
    /// Description:
    /// Sets up this projectile
    /// </summary>
    private void Setup()
    {
        if (!projectiles.ContainsKey(gameObject))
        {
            projectiles.Add(gameObject, this);
        }
    }

    /// <summary>
    /// Description:
    /// Handles 2D collisions
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
	    if (!ignoredLayers.ContainsLayer(collision.gameObject.layer))
	    {
		    HandleCollision(collision.gameObject);
	    }
    }

    /// <summary>
    /// Description:
    /// Handles 2D triggers
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
	    if (!ignoredLayers.ContainsLayer(collision.gameObject.layer))
	    {
		    HandleCollision(collision.gameObject);
	    }
    }

    /// <summary>
    /// Description:
    /// Handles generic collision events
    /// </summary>
    /// <param name="obj"></param>
    protected virtual void HandleCollision(GameObject obj)
    {

    }

    /// <summary>
    /// Description: Fires the projectile in a direction (also sets color if necessary)
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="direction"></param>
    /// <param name="overrideColor"></param>
    /// <param name="speed"></param>
    public virtual void Fire(Vector2 startPosition, Vector2 direction, Color overrideColor, float speed = 0)
    {
        if (speed == 0)
        {
            speed = defaultSpeed;
        }
        projectileRigidbody.position = startPosition;
        projectileRigidbody.velocity = direction.normalized * speed;
        SetProjectileColor(overrideColor);
        FaceDirection(direction);
    }

    /// <summary>
    /// Fires a projectile based on a prefab (static function)
    /// </summary>
    /// <param name="projectilePrefab"></param>
    /// <param name="startPosition"></param>
    /// <param name="direction"></param>
    /// <param name="overrideColor"></param>
    /// <param name="speed"></param>
    public static void FireProjectile(GameObject projectilePrefab, Vector2 startPosition, Vector2 direction, Color overrideColor, float speed = 0, int layer = -1)
    {
        GameObject projectile = GameObject.Instantiate(projectilePrefab, startPosition + direction * projectilePrefab.transform.localScale.y, Quaternion.identity, null);
        if ((BaseProjectile)projectile != null)
        {
            ((BaseProjectile)projectile).Fire(startPosition + direction * projectile.transform.localScale.y, direction, overrideColor, speed);
            if (layer != -1)
            {
                projectile.layer = layer;
            }
        }
        else
        {
            Destroy(projectile);
            Debug.LogError("No projectile script on projectile prefab");
        }
    }

    /// <summary>
    /// Description:
    /// Spawns an object at this position and then destroys itself
    /// </summary>
    public void SelfDestruct()
    {
        if (destructionEffect != null)
        {
            GameObject.Instantiate(destructionEffect, transform.position, transform.rotation, null);
        }
        Destroy(gameObject);
    }

    /// <summary>
    /// Description:
    /// Sets the color of the projectile (implement on a per-projectile type basis)
    /// </summary>
    /// <param name="color"></param>
    public virtual void SetProjectileColor(Color color)
    {
        
    }

    /// <summary>
    /// Description: causes a projectile to rotate to face a direction
    /// </summary>
    /// <param name="direction"></param>
    public virtual void FaceDirection(Vector2 direction)
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Vector2.SignedAngle(Vector2.up, direction)));
    }

    /// <summary>
    /// Description:
    /// Allows gameobjects to be treated as projectiles
    /// </summary>
    /// <param name="obj"></param>
    public static implicit operator BaseProjectile(GameObject obj)
    {
        if (projectiles.ContainsKey(obj))
        {
            return projectiles[obj];
        }
        return null;
    }
}
