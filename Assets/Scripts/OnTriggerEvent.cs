/** ------------------------------------------------------------------------------
- Filename:   OnTriggerEvent
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/30
- Created by: shawn
------------------------------------------------------------------------------- */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/** OnTriggerEvent.cs
 * --------------------- Description ------------------------
 * Simple script to call an event when something
 * enters its trigger collider
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/30       Created file.                                       shawn
 */
[RequireComponent(typeof(Collider2D))]
public class OnTriggerEvent : MonoBehaviour
{
    /* PUBLIC MEMBERS */

    public int ValidLayer = 0;
    public UnityEvent OnTrigger;

    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    private void OnTriggerEnter(Collider other)
    {
	    if (other.gameObject.layer == ValidLayer)
	    {
		    OnTrigger?.Invoke();
	    }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
	    if (other.gameObject.layer == ValidLayer)
	    {
		    OnTrigger?.Invoke();
	    }
    }
}
