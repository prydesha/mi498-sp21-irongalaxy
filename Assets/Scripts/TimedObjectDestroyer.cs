/** ------------------------------------------------------------------------------
- Filename:   TimedObjectDestroyer
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/03/08
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** TimedObjectDestroyer.cs
 * --------------------- Description ------------------------
 * destroys an object after an amount of time
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/08       Created file.                                       Jonathan
 */
public class TimedObjectDestroyer : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public float duration;

    /* PRIVATE MEMBERS */
    private float _startTime = 0;
    [SerializeField] private bool startAutomatic = true;
    [SerializeField] private GameObject destructionEffect = null;

    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    private void Start()
    {
        _startTime = Mathf.Infinity;
        if (startAutomatic)
        {
            StartTimer();
        }
    }

    private void Update()
    {
        if (Time.time - duration > _startTime)
        {
            if (destructionEffect != null)
            {
                GameObject.Instantiate(destructionEffect, transform.position, transform.rotation, null);
            }
            Destroy(this.gameObject);
        }
    }

    public void StartTimer()
    {
        if (_startTime >= Mathf.Infinity)
        {
            _startTime = Time.time;
        }
    }
}
