using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class WinCanvas : MonoBehaviour
{
    public TextMeshProUGUI WinText;

    const string WIN_STRING = "YOU WIN!";
    const string LOSE_STRING = "YOU LOSE!";


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Canvas>().enabled = false; 
    }

    public void OnWin()
    {
        WinText.text = WIN_STRING;
        GetComponent<Canvas>().enabled = true;
    }

    public void OnLose()
    {
        WinText.text = LOSE_STRING;
        GetComponent<Canvas>().enabled = true;
    }



}
