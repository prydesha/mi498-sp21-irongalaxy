/** ------------------------------------------------------------------------------
- Filename:   UIManager
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/03/09
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;
/** UIManager.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/09       Created file.                                       smigi
 */
public class UIManager : MonoBehaviour
{
    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */

    [SerializeField]
    private GameObject PauseMenu;
    [SerializeField]
    private Button FirstSelectedButton;
    [SerializeField]
    private GameObject EndScreen;

    private TMP_Text EndText;
    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */


    public void OnPause()
    {
        LevelManager.LM.StopScene();
        InputManager.SwitchInputType("UI");

        if(PauseMenu == null)
        {
            PauseMenu = GameObject.Find("GameUI").transform.Find("PauseMenu").gameObject;
        }
        PauseMenu.SetActive(true);
        FirstSelectedButton.Select();
        FirstSelectedButton.OnSelect(null);

    }

    public void OnUnPause()
    {
        LevelManager.LM.ContinueScene();
        InputManager.SwitchInputType("InGame");
        PauseMenu.SetActive(false);

    }

    public void SetVolume(System.Single per)
    {
        AudioManager.MasterMixer.SetVolumeToPercentage(per);
    }



    public void EndScreenWin(float temp)
    {
        EndScreen.SetActive(true);
        EndText.text = "YOU WIN!";
        EndScreen.transform.Find("Restart").gameObject.GetComponent<Button>().Select();
        InputManager.SwitchInputType("UI");
    }
    public void EndScreenLose(float temp)
    {
        EndScreen.SetActive(true);
        EndText.text = "YOU LOSE!";
        EndScreen.transform.Find("Restart").gameObject.GetComponent<Button>().Select();
        InputManager.SwitchInputType("UI");
    }



    #region Unity Functions

    // Start is called before the first frame updatea
    void Awake()
    {
        EndScreen.SetActive(false);
        EndText = EndScreen.transform.Find("WinText").gameObject.GetComponent<TMP_Text>();
        InputManager.UnPause.OnButtonPress += OnUnPause;
        InputManager.Pause.OnButtonPress += OnPause;
        if (EventSystem.current != null)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }

    }
    private void Start()
    {
        if (GameManager.GM.Player != null && GameManager.GM.Player.Health != null)
        {
            GameManager.GM.Player.Health.OnDeath += EndScreenLose;
        }
        if (GameManager.GM.currentBoss != null && GameManager.GM.currentBoss.Health != null)
        {
            GameManager.GM.currentBoss.Health.OnDeath += EndScreenWin;
        }
    }
    private void OnDisable()
    {
        if (Application.isPlaying && InputManager.UnPause != null && InputManager.Pause != null)
        {
            InputManager.UnPause.OnButtonPress -= OnUnPause;
            InputManager.Pause.OnButtonPress -= OnPause;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    #endregion
}
