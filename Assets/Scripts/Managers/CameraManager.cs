/** ------------------------------------------------------------------------------
- Filename:   CameraManager
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/26
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class CameraEvent 
{
    public enum CameraEventType
    {
        Moving,
        ColorChange,
        All
    }
    public string eventName;
    public bool stopPlayerMovement = true;
    public float transitionTime = 3f;
    public CameraEventType _type = CameraEventType.All;
    //Movement
    public float maxSpeed = 3f;
    public float maxZoomOrthographic = 5f;
    public AnimationCurve movement;
    public AnimationCurve zoomMovement;
    public Transform camStop;
    public GameObject objects;
    public float stopTime = 2f;
    public bool isTwoWay = true;

    //Color
    public Gradient Colors;
    public AnimationCurve transitionSpeed;



    private bool isMoving = false;
    private bool isColoring = false;

    public IEnumerator Invoke(Cinemachine.CinemachineVirtualCamera cam,SpriteRenderer Fader)
    {
        
        switch (_type)
        {
            case CameraEvent.CameraEventType.Moving:

                yield return Movement(cam);
                //Anything else you want to display

                break;
            case CameraEvent.CameraEventType.ColorChange:

                yield return ColorChange(Fader);

                //Anything else you want to display
                break;
            case CameraEvent.CameraEventType.All:
                var tempTime = 0f;
                while( tempTime < transitionTime * 4f)
                {
                    if(!isMoving && !isColoring)
                    {
                        break;
                    }
                    tempTime += Time.deltaTime;
                    yield return null;
                }
                //Anything else you want to display
                break;
        }

        yield return null;
    }
    public IEnumerator Movement(Cinemachine.CinemachineVirtualCamera cam)
    {
        isMoving = true;
        var tempTime = 0f;

        var originalOrtho = cam.m_Lens.OrthographicSize;
        var originalTransform = cam.Follow;
        var orignalPos = originalTransform.position;

        var newTransform = cam.transform.GetChild(1);
        newTransform.position = originalTransform.position;
        var maxDist = Vector2.Distance(newTransform.position, camStop.position);


        Vector2 dir = (new Vector2(camStop.position.x, newTransform.position.y) - new Vector2(camStop.position.x, newTransform.position.y)).normalized;
        cam.Follow = newTransform;
        while (tempTime < transitionTime)
        {

            cam.m_Lens.OrthographicSize = originalOrtho + (zoomMovement.Evaluate(tempTime / transitionTime) * maxZoomOrthographic);

            newTransform.position = new Vector2(orignalPos.x, orignalPos.y) + (dir * movement.Evaluate(tempTime / transitionTime) * maxDist);
            var pos = newTransform.position;
            pos.z = originalTransform.position.z;
            newTransform.position = pos;

            tempTime += Time.deltaTime;


            yield return null;
        }
        newTransform.localPosition = new Vector3(0, 0, 0);
        yield return new WaitForSecondsRealtime(stopTime);
        if (isTwoWay)
        {
            while (tempTime > 0)
            {
                maxDist = Vector2.Distance(originalTransform.position, camStop.position);
                dir = (new Vector2(originalTransform.position.x, originalTransform.position.y) - new Vector2(newTransform.position.x, newTransform.position.y)).normalized;

                cam.m_Lens.OrthographicSize = originalOrtho + (zoomMovement.Evaluate(tempTime / transitionTime) * maxZoomOrthographic);

                newTransform.position = new Vector2(originalTransform.position.x, originalTransform.position.y) + (-dir * movement.Evaluate(tempTime / transitionTime) * maxDist);
                var pos = newTransform.position;
                pos.z = originalTransform.position.z;
                newTransform.position = pos;

                tempTime -= Time.deltaTime;


                yield return null;
            }


        }
        cam.m_Lens.OrthographicSize = originalOrtho;

        cam.Follow = originalTransform;
        isMoving = false;
        yield return null;
    }

    public IEnumerator ColorChange(SpriteRenderer Fader)
    {
        isColoring = true ;
        var tempTime = 0f;

        while(tempTime < transitionTime)
        {
            Fader.color = Colors.Evaluate(transitionSpeed.Evaluate(tempTime / transitionTime));
            tempTime += Time.deltaTime;
            yield return null;
        }
        isColoring = false;
        yield return null; ;
    }
}





/** CameraManager.cs
 * --------------------- Description ------------------------
 * Component which causes the camera to move according to a set of rules
 * 
 * This is a very basic thing to get us the ability to move around beyond a stationary camera Don't judge me
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/26       Created file.                                       Jonathan
 * 2021/02/23       Added bounds.                                       Blake
 */
 
public class CameraManager : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public SpriteRenderer Fader;

    [HideInInspector]
    public static CameraManager Instance;
    

    [SerializeField]private CameraEvent[] CameraEvents = new CameraEvent[0];
    private Cinemachine.CinemachineVirtualCamera Camera;
    private Queue<CameraEvent> EventCalls = new Queue<CameraEvent>();
    private Transform DefaultTransform;
    /* PRIVATE MEMBERS */
    private float x;
    private float y;
    private float z;

    /* PROPERTIES */


    /* METHODS */

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;
        Camera = GetComponent<Cinemachine.CinemachineVirtualCamera>();
        DefaultTransform = Camera.Follow;
         

    }


    public Coroutine CallEvent(string Event)
    {
        foreach(CameraEvent CE in CameraEvents)
        {
            if(CE.eventName == Event)
            {
                if(CE._type == CameraEvent.CameraEventType.All)
                {
                    StartCoroutine(CE.ColorChange(Fader));
                    StartCoroutine(CE.Movement(Camera));
                }
                return StartCoroutine(CE.Invoke(Camera, Fader)); 
            }
        }
        Debug.Log("No Camera event of string: "+ Event+ " Exists.");
        return null;
    }


}
