/** ------------------------------------------------------------------------------
- Filename:   LevelManager
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/16
- Created by: smigi
------------------------------------------------------------------------------- */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/** LevelManager.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/16       Created file.                                       smigi
 */

public class LevelManager : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public static LevelManager LM;

    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */
    public void StopScene()
    {
        Time.timeScale = 0;

    }

    public void ContinueScene()
    {
        Time.timeScale = 1;
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif
    }

    public void LoadScene(string scene)
    {
        if (Application.CanStreamedLevelBeLoaded(scene))
        {
            
            

            InputManager.SwitchInputType("InGame");
            ContinueScene();
            //SceneManager.LoadScene(scene);
            StartCoroutine(LoadSceneCoroutine(scene));
        }
        else
        {
            Debug.Log("The attempted load of scene: " + scene + " failed because the scene does not exist.");
        }
    }

    /// <summary>
    /// Description:
    /// Loads a scene asynchronously
    /// If you make the progress variable a class or static variable, you can use it to display a loading bar
    /// </summary>
    /// <param name="sceneName"></param>
    /// <returns></returns>
    private IEnumerator LoadSceneCoroutine(string sceneName)
    {
        float progress = 0;
        string originalSceneName = SceneManager.GetActiveScene().name;
        float startTime = Time.fixedTime;
        float minDuration = 0.5f;
        /*AsyncOperation loadOperation = */SceneManager.LoadScene(sceneName);//, LoadSceneMode.Additive);
        
        //loadOperation.allowSceneActivation = false;

        //while (!loadOperation.isDone || Time.fixedTime < startTime + minDuration)
        //{
        //    yield return null;
        //    progress = Mathf.Min(loadOperation.progress, (Time.fixedTime - startTime) / minDuration);
        //    if (loadOperation.progress >= 0.9f)
        //    {
        //        GC.Collect();
        //        loadOperation.allowSceneActivation = true;
        //    }
        //}

        //SceneManager.UnloadSceneAsync(originalSceneName);

        
        progress = 0;
        yield return null;
    }

    public void Reload()
    {
        Scene scene = SceneManager.GetActiveScene();
        LoadScene(scene.name);
    }
    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Awake()
    {

        if (LM != null)
        {

            Destroy(this.gameObject);
            return;
        }
        LM = this;
        DontDestroyOnLoad(this.gameObject);
        if (transform.parent != null)
        {
            Debug.LogError("Level Manager should not be childed to anything");
        }
        else
        {
           // DontDestroyOnLoad(this.gameObject);
        }

        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void OnSceneLoaded(Scene aScene, LoadSceneMode aMode)
    {
        if (CameraManager.Instance != null)
            CameraManager.Instance.CallEvent("IntoScene");
    }
    #endregion
}
