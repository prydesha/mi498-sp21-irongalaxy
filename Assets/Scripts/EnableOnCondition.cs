/** ------------------------------------------------------------------------------
- Filename:   EnableOnCondition
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/29
- Created by: shawn
------------------------------------------------------------------------------- */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** EnableOnCondition.cs
 * --------------------- Description ------------------------
 * Very stupid script I had to make to ensure that the
 * crab's shell cracked consistently
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/29       Created file.                                       shawn
 */
public class EnableOnCondition : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    [SerializeField] private Boss _boss;
    [SerializeField] private int enableValue = 3;

    /* PRIVATE MEMBERS */


    /* PROPERTIES */
    

    /* METHODS */

    private void Start()
    {
	    if (_boss)
	    {
		    _boss.OnPhaseChange.AddListener(TryEnable);
	    }
	    gameObject.SetActive(false);
    }

    public void TryEnable(int condition)
    {
	    if (condition == enableValue)
	    {
		    gameObject.SetActive(true);
	    }
    }
}
