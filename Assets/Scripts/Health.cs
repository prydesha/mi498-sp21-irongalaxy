/** ------------------------------------------------------------------------------
- Filename:   Health
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/26
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;

/** Health.cs
 * --------------------- Description ------------------------
 * Class which manages hit points and provides basic functionality for detecting overlapped colors
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/26       Created file.                                       Jonathan
 * 2021/03/02       Added invincibility frames to						Shawn
 *					damage function
 */
public class Health : MonoBehaviour
{
    /* PUBLIC MEMBERS */
	public delegate void HealthChangeEvent(float amount);
	/// <summary>
	/// Event which occurs when this Health behavior takes damage
	/// </summary>
	public HealthChangeEvent OnDamage = delegate { };
	/// <summary>
	/// Event which occurs when this Health behavior receives health
	/// </summary>
	public HealthChangeEvent OnHealed = delegate { };
    /// <summary>
    /// Event which occurs when this Health behavior receives health
    /// </summary>
    public HealthChangeEvent OnDeath = delegate { };

    // Duration of hit stop
    public const float HITSTOPDURATION = 0.05f;

    /* PRIVATE MEMBERS */
    [Header("Health Variables")]
    [Tooltip("The maximum health value")]
    [SerializeField] private float _maxHealth = 100;
    [Tooltip("The current health value")]
    [SerializeField] private float _currentHealth = 100;
    [Tooltip("Is the player invincible")]
    [SerializeField] private bool _isInvinsible = false;
    [Tooltip("Damage reduction applied when in yellow paint")]
    [SerializeField] private float enfeebleDamageModifier = 0.5f;
	[Tooltip("Number of frames to wait after this health is damaged before it" +
	         "can be damaged again")]
    [Min(0)]
    [SerializeField] private int _invincibilityFrames = 5;

    [Header("Damage, Healing, and Death Events")]
    [Tooltip("Sprite Flasher component that controls flashing the sprite various colors")]
    [SerializeField] private SpriteFlasher _spriteFlasher = null;
    [Tooltip("Name of sound effect played upon taking damage")]
    [SerializeField] private string audioClipName = "";
    [Tooltip("Name of sound effect played upon death")]
    [SerializeField] private string deathSFX = "";
    [Tooltip("The gameobject to spawn when damage is taken")]
    [SerializeField] private GameObject _damageEffect = null;
    [Tooltip("The gameobject to spawn when damage is avoided due to being immune to damage")]
    [SerializeField] private GameObject _damageAvoidEffect = null;
    [Tooltip("The gameobject to spawn dying")]
    [SerializeField] private GameObject _deathEffect = null;
    [Tooltip("Event called when hp decreases")]
    [SerializeField] private IntUnityEvent _onDamage = new IntUnityEvent();
    [Tooltip("Event called when hp increases")]
    [SerializeField] private IntUnityEvent _onHeal = new IntUnityEvent();
    [Tooltip("Event called when hp reaches 0")]
    [SerializeField] private UnityEvent _onDeath = new UnityEvent();
    [Tooltip("Event called whenever something tries to damage this health (can occur even " +
             "during iframes and actual damaging events)")]
    [SerializeField] private IntUnityEvent _onDamageAttempt = new IntUnityEvent();
	[Tooltip("Sound effect to play when something tries to damage this health but the health is " +
	         "currently immune to damage. Leave blank for now sound effect")]
    [SerializeField] private string _hitImmuneSFX = "";
    
    // List of sprite renderers to flash when taking damage
    private List<SpriteRenderer> _spriteRenderers = new List<SpriteRenderer>();

    // Amount of defense coroutines active
    private int enfeebleCoroutines = 0;

    private Coroutine _iframes = null;
	// true if this health is currently not damageable (regardless of iFrames)
    private bool _damageImmune = false;

    // Current hit stop coroutine
    private Coroutine hitStopCoroutine = null;

    private CinemachineImpulseSource _cameraImpulse = null;

    /* PROPERTIES */

    public UnityEvent getOnDeath()
    {
        return _onDeath;
    }

    // The percentage of health remaining on this health component
    public float healthPercentage
    {
        get
        {
            return _currentHealth / _maxHealth;
        }
        set
        {
            _currentHealth = value * _maxHealth;
        }
    }
    // Accessor for the current health
    public float health
    {
        get
        {
            return _currentHealth;
        }
        set
        {
            if (!_isInvinsible)
            {
                var val = value;
                float oldHealth = _currentHealth;
                val = Mathf.Clamp(val, 0.0f, _maxHealth);
                _currentHealth = val;
                float difference = Mathf.Abs(oldHealth - val);
                // check to trigger events
                if (oldHealth != 0 && oldHealth != val)
                {
                    //var difference = Mathf.Abs(oldHealth - val);
                    if (val < oldHealth)
                    {
                        OnDamageBehavior(difference, enfeebleCoroutines > 0);
                        HitStop(difference);
                    }
                    if (val > oldHealth)
                    {
                        OnHealBehavior(difference);
                    }
                }
                // check for death
                if (oldHealth > 0 && val <= 0)
                {
                    if (hitStopCoroutine != null)
                    {
                        StopCoroutine(hitStopCoroutine);
                        Time.timeScale = 1;
                    }

                    if (deathSFX != "")
                    {
	                    AudioManager.Play(deathSFX);
                    }
                    OnDeathBehavior(difference);
                }
            }
	        //_currentHealth = val;
        }
    }

    public float maxHealth => _maxHealth;

    private float currentDamageMultiplier
    {
        get
        {
            return 1.0f * (enfeebleCoroutines > 0? enfeebleDamageModifier: 1.0f);
        }
    }

    /// <summary>
    /// True if this health cannot be damaged currently
    /// </summary>
    public bool DamageImmune
    {
	    get => _damageImmune;// || _iframes != null;
	    set => _damageImmune = value;
    }

    /* METHODS */

    /// <summary>
    /// Because unity events aren't very smart
    /// </summary>
    /// <param name="damage"></param>
    public void Damage(float damage)
    {
	    Damage(damage, false);
    }

    /// <summary>
    /// Description:
    /// Damages this health component
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="ignoreImmunities">set to true to damage this health
    /// regardless of invincibility frames or imperviousness</param>
    public bool Damage(float damage, bool ignoreImmunities)
    {
	    // make sure we're actually trying to deal damage
	    if (damage <= 0)
	    {
		    return false;
	    }
	    _onDamageAttempt?.Invoke((int)damage);
	    
	    if (!ignoreImmunities)
	    {
		    // check for not being damageable
		    if (DamageImmune)
		    {
			    if (_hitImmuneSFX != "")
			    {
				    AudioManager.Play(_hitImmuneSFX);
			    }
			    return false;
		    }
		    // check for i frames
		    if (_iframes != null)
		    {
			    return false;
		    }
		    if (_invincibilityFrames != 0)
		    {
			    _iframes = StartCoroutine(InvincibilityFrames());
		    }
	    }
        health -= damage * currentDamageMultiplier;
        return true;
    }

    /// <summary>
    /// Because C# isn't always smart either
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="ignoreImmunities"></param>
    /// <param name="damagePosition"></param>
    /// <param name="damageDirection"></param>
    public void Damage(float damage, bool ignoreImmunities, Vector2 damagePosition, Vector2 damageDirection)
    {
        if (Damage(damage, ignoreImmunities))
        {
            SpawnDamageEffect(damagePosition, damageDirection);
        }
        else if (DamageImmune)
        {
            SpawnDamageAvoidEffect(damagePosition, damageDirection);
        }
    }

    /// <summary>
    /// Description:
    /// Spawns a damage effect at a position, rotated so that 'up' faces in a direction
    /// </summary>
    /// <param name="position"></param>
    /// <param name="direction"></param>
    private void SpawnDamageEffect(Vector2 position, Vector2 direction)
    {
        if (_damageEffect != null)
        {
            Quaternion startRotation = Quaternion.AngleAxis(Vector3.SignedAngle(_damageEffect.transform.up, direction, Vector3.forward), Vector3.forward);
            GameObject effect = GameObject.Instantiate(_damageEffect, position, startRotation, null);
        }
    }

    /// <summary>
    /// Description:
    /// Spawns a damage avoidance effect at a position, rotated so that 'up' faces in a direction
    /// </summary>
    /// <param name="position"></param>
    /// <param name="direction"></param>
    private void SpawnDamageAvoidEffect(Vector2 position, Vector2 direction)
    {
        if (_damageAvoidEffect != null)
        {
            Quaternion startRotation = Quaternion.AngleAxis(Vector3.SignedAngle(_damageAvoidEffect.transform.up, direction, Vector3.forward), Vector3.forward);
            GameObject effect = GameObject.Instantiate(_damageAvoidEffect, position, startRotation, null);
        }
    }

    /// <summary>
    /// Description: Handles invincibility frames
    /// </summary>
    /// <returns></returns>
    private IEnumerator InvincibilityFrames()
    {
	    int frameCount = _invincibilityFrames;
	    while (frameCount > 0)
	    {
		    yield return new WaitForEndOfFrame();
		    frameCount--;
	    }
	    _iframes = null;
    }

    /// <summary>
    /// Description:
    /// Heals this health component
    /// </summary>
    /// <param name="healing"></param>
    public void Heal(float healing)
    {
        health += healing;
    }

    /// <summary>
    /// Description:
    /// Begins defending for 0.15 seconds
    /// </summary>
    public void Enfeeble()
    {
        float time = 0.15f;
        StartCoroutine(EnfeebleCoroutine(time));
    }

    /// <summary>
    /// Description:
    /// Defends for a set amount of time
    /// </summary>
    /// <param name="duration"></param>
    /// <returns></returns>
    public IEnumerator EnfeebleCoroutine(float duration = 0.15f)
    {
        enfeebleCoroutines++;
        for (float t = 0; t <= duration; t += Time.deltaTime)
        {
            yield return null;
        }
        enfeebleCoroutines--;
    }

    /// <summary>
    /// Description:
    /// On damage, plays effects
    /// </summary>
    /// <param name="damage"></param>
    private void OnDamageBehavior(float damage, bool modified = false)
    {
        // Flash sprite if possible
        if (_spriteFlasher != null)
        {
            Color flashColor = Color.white;
            if (modified)
            {
                flashColor = PaintColorAuthoritative.GetColor(PaintColorAuthoritative.PaintColorEnum.Yellow);
            }
            else
            {
                //flashColor = PaintColorAuthoritative.GetColor(PaintColorAuthoritative.PaintColorEnum.Green);
                flashColor = new Color(0.33f, 0.33f, 0.33f, 1.0f);
            }
            _spriteFlasher.FlashColor(Color.Lerp(flashColor, Color.white, 0.33f), 0.12f, true);
        }
        // Play audio if possible
        if (audioClipName != "")
        {
            AudioManager.Play(audioClipName);
        }
        // Play other misc effects
        _onDamage.Invoke(Mathf.CeilToInt(damage));
        OnDamage.Invoke(damage);
        // screen shake
        if (_cameraImpulse)
        {
	        _cameraImpulse.GenerateImpulse(damage);
        }
    }

    /// <summary>
    /// Description:
    /// On heal, plays effects
    /// </summary>
    /// <param name="healing"></param>
    private void OnHealBehavior(float healing)
    {
        // Flash sprite if possible
        if (_spriteFlasher != null)
        {
            //_spriteFlasher.FlashColor(PaintColorAuthoritative.GetColor(PaintColorAuthoritative.PaintColorEnum.Red), 0.5f, false);
        }
        // Play audio if possible
        //if (audioClipName != "")
        //{
        //    AudioManager.Play(audioClipName);
        //}
        // Play other misc effects
        _onHeal.Invoke(Mathf.CeilToInt(healing));
        OnHealed.Invoke(healing);
    }

    /// <summary>
    /// Description:
    /// Things to do when dying
    /// </summary>
    private void OnDeathBehavior(float triggeringDamage)
    {
        if (_deathEffect != null)
        {
            GameObject effect = GameObject.Instantiate(_deathEffect, transform.position, transform.rotation, null);
        }

        _onDeath.Invoke();
        OnDeath.Invoke(triggeringDamage);
    }

    /// <summary>
    /// Description:
    /// Putting this here for now, not a permanant location
    /// </summary>
    public void PulseBlue()
    {
        if (_spriteFlasher != null && !_spriteFlasher.flashing)
        {
            Color color = PaintColorAuthoritative.GetColor(PaintColorAuthoritative.PaintColorEnum.Blue);
            _spriteFlasher.FlashColor(Color.Lerp(color, Color.white, 0.7f), 1, false);
        }
    }

    /// <summary>
    /// Applies hit stop
    /// </summary>
    public void HitStop(float damageAmount)
    {
        if (gameObject.activeInHierarchy && _currentHealth > 0)
        {
            if (hitStopCoroutine != null)
            {
                StopCoroutine(hitStopCoroutine);
                Time.timeScale = 1;
            }
            hitStopCoroutine = StartCoroutine(HitStopCoroutine(HITSTOPDURATION * Mathf.Clamp(damageAmount / 5.0f, 0.0f, 1.0f)));
        }
    }

    /// <summary>
    /// Description:
    /// Applies hit stop for a set amount of time
    /// </summary>
    /// <param name="duration"></param>
    /// <returns></returns>
    public IEnumerator HitStopCoroutine(float duration)
    {
        Time.timeScale = 0.0f;
        yield return new WaitForSecondsRealtime(duration);
        Time.timeScale = 1.0f;
    }

    // called before the first frame of the game
    private void Awake()
    {
	    _cameraImpulse = GetComponent<CinemachineImpulseSource>();
    }
}

/// <summary>
/// Unity event extension that lets me create int based unity events
/// </summary>
[System.Serializable]
public class IntUnityEvent : UnityEvent<int>
{
}
