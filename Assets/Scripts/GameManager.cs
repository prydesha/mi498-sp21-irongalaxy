/** ------------------------------------------------------------------------------
- Filename:   GameManager
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/03/09
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/** GameManager.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/09       Created file.                                       smigi
 */
public class GameManager : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public static GameManager GM;
    public Player Player;
    public Boss currentBoss;
    public bool inCutscene = false;
    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */




    #region Unity Functions


    // Start is called before the first frame update
    void Start()
    {

    }

    private void Awake()
    {
        Player = FindObjectOfType<Player>();
        currentBoss = FindObjectOfType<Boss>();



        GM = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    #endregion
}
