/** ------------------------------------------------------------------------------
- Filename:   ScaleOverTime
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/18
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** ScaleOverTime.cs
 * --------------------- Description ------------------------
 * Scales an object over time using an animation curve
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/18       Created file.                                       Jonathan
 */
public class ScaleOverTime : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public float duration = 1.0f;
    public Vector3 maxScale = Vector3.one;
    public AnimationCurve curve = new AnimationCurve();

    /* PRIVATE MEMBERS */
    private float _t;

    /* PROPERTIES */

    public float t
    {
        get
        {
            return Mathf.Clamp((Time.time - _t) / duration, 0, 1);
        }
    }

    /* METHODS */

    private void Awake()
    {
        _t = Time.time;
    }

    private void Update()
    {
        transform.localScale = maxScale * curve.Evaluate(t);
    }
}
