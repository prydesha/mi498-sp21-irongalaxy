/** ------------------------------------------------------------------------------
- Filename:   VignetteFlasher
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/03/08
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/** VignetteFlasher.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/08       Created file.                                       Jonathan
 */
public class VignetteFlasher : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    [Tooltip("The maximum change in intensity of the vignette as a result of a flash")]
    [SerializeField] private float maxIntensityChange = 0.25f;
    [Tooltip("The curve that specifies how a flash occurs")]
    [SerializeField] private AnimationCurve flashCurve = new AnimationCurve();
    // The coroutine that handles the flash
    private Coroutine flasher = null;
    // The volume to modify
    private Volume volume = null;
    // The target vignette effect
    private Vignette vignette = null;
    // Base intensity of vignette effect
    private float baseIntensity = 0.5f;

    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up the color matcher
    /// </summary>
    private void Awake()
    {
        if (volume == null)
        {
            volume = GetComponent<Volume>();
        }
        if (volume != null)
        {
            volume.profile.TryGet(out vignette);
            baseIntensity = (float)vignette.intensity;
        }
    }

    /// <summary>
    /// Description:
    /// Converts integer intensity to 0-1 float intensity
    /// </summary>
    /// <param name="intensity"></param>
    /// <returns></returns>
    public float ConvertIntensity(int intensity)
    {
        return Mathf.Clamp(intensity, 0, 20) / 20.0f;
    }

    /// <summary>
    /// Description:
    /// Resets vignette
    /// </summary>
    public void Reset()
    {
        SetVignetteIntensity(baseIntensity);
    }

    /// <summary>
    /// Description:
    /// Sets vignette intensity
    /// </summary>
    /// <param name="value"></param>
    public void SetVignetteIntensity(float value)
    {
        if (vignette != null)
        {
            var setVal = new ClampedFloatParameter(Mathf.Clamp(value, 0, 1), 0, 1, true);
            vignette.intensity.SetValue(setVal);
        }
    }

    /// <summary>
    /// Description:
    /// Causes the vignette intensity to flash
    /// </summary>
    /// <param name="intensity"></param>
    public void Flash(int intensity)
    {
        if (flasher != null)
        {
            StopCoroutine(flasher);
            Reset();
        }
        flasher = StartCoroutine(FlashCoroutine(intensity));
    }

    /// <summary>
    /// Description:
    /// Coroutine that handles flashing
    /// </summary>
    /// <param name="intensity"></param>
    /// <returns></returns>
    public IEnumerator FlashCoroutine(int intensity)
    {
        float duration = ConvertIntensity(intensity);
        float maxLerp = ConvertIntensity(intensity);
        for (float t = 0; t < duration; t+=Time.deltaTime)
        {
            float percentage = Mathf.Clamp(t / duration, 0, 1);
            float lerped = Mathf.Clamp(flashCurve.Evaluate(percentage) * maxLerp, 0, maxIntensityChange);
            float result = baseIntensity + lerped;
            SetVignetteIntensity(result);
            yield return null;
        }
        Reset();
    }
}
