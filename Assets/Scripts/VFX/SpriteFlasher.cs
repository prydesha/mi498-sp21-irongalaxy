/** ------------------------------------------------------------------------------
- Filename:   SpriteFlasher
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/22
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/** SpriteFlasher.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/22       Created file.                                       Jonathan
 */
public class SpriteFlasher : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    [Header("Flash variables")]
    [Tooltip("Curve that controls color over the duration of the flash. a cuve value of 0 is white, 1 is the target color")]
    [SerializeField] private AnimationCurve _flashCurve = new AnimationCurve();
    [Tooltip("The duration of a flash if no duration argument is supplied to FlashColor")]
    [SerializeField] private float defaultFlashTime = 0.05f;
    [Tooltip("The color of a flash if no arguments are supplied")]
    [SerializeField] private Color defaultFlashColor = Color.red;
    [SerializeField] private List<SpriteRenderer> _ignoreSprites = new List<SpriteRenderer>();
    // The coroutine that is currently handling the flash
    private Coroutine _flashCoroutine = null;
    // The sprite renderers that are affected by this sprite flasher
    private List<SpriteRenderer> _spriteRenderers = new List<SpriteRenderer>();
    // Whether this sprite flasher has been set up
    private bool setup;

    /* PROPERTIES */

    public bool flashing
    {
        get
        {
            return _flashCoroutine != null;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up this sprite flasher
    /// </summary>
    private void Setup()
    {
        if (setup)
        {
            return;
        }
        else
        {
            _spriteRenderers = GetComponentsInChildren<SpriteRenderer>().ToList();
            foreach (SpriteRenderer sr in _ignoreSprites)
            {
                _spriteRenderers.Remove(sr);
            }
            setup = true;
        }
    }

    /// <summary>
    /// Function that causes all child sprites to flash
    /// </summary>
    /// <param name="target"></param>
    /// <param name="duration"></param>
    public void FlashColor(Color target, float duration, bool force = false)
    {
        if (force && _flashCoroutine != null)
        {
            StopCoroutine(_flashCoroutine);
            _flashCoroutine = null;
        }
        if (_flashCoroutine == null)
        {
            _flashCoroutine = StartCoroutine(FlashColorCoroutine(target, duration));
        }
    }
    /// <summary>
    /// Description:
    /// Overload for FlashColor with a single argument (so it can be called from unity events)
    /// </summary>
    public void FlashColor()
    {
        FlashColor(defaultFlashColor, defaultFlashTime);
    }

    /// <summary>
    /// Description:
    /// Coroutine that handles sprite colors during a flash
    /// </summary>
    /// <param name="target"></param>
    /// <param name="duration"></param>
    /// <returns></returns>
    private IEnumerator FlashColorCoroutine(Color target, float duration)
    {
        for (float t = 0; t < duration; t += Time.deltaTime)
        {
            float percentage = Mathf.Clamp(_flashCurve.Evaluate(t / duration), 0, 1);
            SetSpritesColor(Color.Lerp(Color.white, target, percentage));
            yield return null;
        }
        SetSpritesColor(Color.white);
        _flashCoroutine = null;
    }

    /// <summary>
    /// Description:
    /// Sets all sprite colors in child sprites
    /// </summary>
    /// <param name="target"></param>
    private void SetSpritesColor(Color target)
    {
        Setup();
        foreach (SpriteRenderer sr in _spriteRenderers)
        {
            sr.color = target;
        }
    }
}
