/** ------------------------------------------------------------------------------
- Filename:   ColorManagerMatcher_Vignette
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/03/08
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/** ColorManagerMatcher_Vignette.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/08       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(Volume))]
public class ColorManagerMatcher_Vignette : BaseColorManagerMatcher
{
    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */
    // The volume to modify
    private Volume volume = null;
    // The target vignette effect
    private Vignette vignette = null;

    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up the color matcher
    /// </summary>
    private void Awake()
    {
        if (volume == null)
        {
            volume = GetComponent<Volume>();
        }
        if (volume != null)
        {
            volume.profile.TryGet(out vignette);
        }
    }

    /// <summary>
    /// Description:
    /// Sets the vignette color
    /// </summary>
    public override void SetColor()
    {
        Color color = Color.black;
        if (paintColorManager != null)
        {
            color = paintColorManager;
        }

        if (vignette != null)
        {
            vignette.color.value = color;
        }
    }
}
