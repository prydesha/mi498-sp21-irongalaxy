/** ------------------------------------------------------------------------------
- Filename:   OverlapColorDelegator
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/16
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

/** OverlapColorDelegator.cs
 * --------------------- Description ------------------------
 * Finds the colors overlapped by this component using a PaintDetectionCamera and calls corresponding events
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/16       Created file.                                       Jonathan
 * 2021/03/03		Added ability to detect the absence					Shawn
 *					of color
 */
public class OverlapColorDelegator : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public delegate void ColorChangeEventDelegate(Color c);
    public ColorChangeEventDelegate colorChangeEvent = delegate { };

    [HideInInspector] public bool ignorePaint = false;

    /* PRIVATE MEMBERS */

    [SerializeField] private PaintDetectionCamera _pdc = null;
    [SerializeField] private float _checkRate = 0.01f;

    [Header("Color Events")]
    public UnityEvent _redEvent = new UnityEvent();
    public UnityEvent _yellowEvent = new UnityEvent();
    public UnityEvent _blueEvent = new UnityEvent();
    public UnityEvent _greenEvent = new UnityEvent();
    public UnityEvent _noColorEvent = new UnityEvent();

    private Coroutine checkCoroutine = null;

    /* PROPERTIES */

    private PaintDetectionCamera pdc
    {
        get
        {
            if (_pdc == null)
            {
                _pdc = GetComponentInChildren<PaintDetectionCamera>();
            }
            return _pdc;
        }
    }

    public float checkRate
    {
        get
        {
            return _checkRate;
        }
    }

    /* METHODS */

    private void Awake()
    {
        if (pdc == null)
        {
            Debug.LogError("No Paint Detection Camera assigned, destroying delegator");
            Destroy(this);
        }
        //_redEvent.AddListener(delegate { Debug.Log("red"); });
        checkCoroutine = StartCoroutine(DelegateColors(_checkRate));
    }

    /*
    private void Update()
    {
        Debug.Log("" + (checkCoroutine != null) + ", " +  pdc.GetColors().Count());
    }//*/

    private IEnumerator DelegateColors(float rate)
    {
        while (Application.isPlaying)
        {
            if (!ignorePaint)
            {
                var colors = pdc.GetColors().ToArray();
                if (colors.Length > 0)
                {
                    foreach (Color c in colors)
                    {
                        if (c.IsColor(PaintColorAuthoritative.PaintColorEnum.Red))
                        {
                            _redEvent.Invoke();
                            colorChangeEvent.Invoke(c);
                        }
                        else if (c.IsColor(PaintColorAuthoritative.PaintColorEnum.Yellow))
                        {
                            _yellowEvent.Invoke();
                            colorChangeEvent.Invoke(c);
                        }
                        else if (c.IsColor(PaintColorAuthoritative.PaintColorEnum.Blue))
                        {
                            _blueEvent.Invoke();
                            colorChangeEvent.Invoke(c);
                        }
                        else if (c.IsColor(PaintColorAuthoritative.PaintColorEnum.Green))
                        {
                            _greenEvent.Invoke();
                            colorChangeEvent.Invoke(c);
                        }
                    }
                }
                else
                {
                    _noColorEvent.Invoke();
                    colorChangeEvent.Invoke(Color.black);
                }
            }

	        yield return new WaitForSecondsRealtime(rate);
        }
    }
}
