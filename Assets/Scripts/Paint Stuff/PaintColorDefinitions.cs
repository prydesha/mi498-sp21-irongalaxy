/** ------------------------------------------------------------------------------
- Filename:   PaintColorDefinitions
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/16
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PaintColorDefinitions.cs
 * --------------------- Description ------------------------
 * Defines a set of colors to be used for paints
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/16       Created file.                                       Jonathan
 */
 [ExecuteAlways()]
 [CreateAssetMenu(fileName = "Paint Color Definitions", menuName = "Scriptable Objects/Paint/Paint Color Definitions")]
public class PaintColorDefinitions : ScriptableObject
{
    /* PUBLIC MEMBERS */
    [Tooltip("Integer used to differentiate PaintColorDefinitions from player prefs")]
    public int identifier = 0;

    /* PRIVATE MEMBERS */
    [Tooltip("A list of definitions of color values for paint color enum values")]
    [SerializeField] private List<ColorDefinition> _colors = new List<ColorDefinition>();

    // Forward declaration of a dictionary to be used when reading colors from this SO as a dictionary
    private Dictionary<PaintColorAuthoritative.PaintColorEnum, Color> colorDictionary = new Dictionary<PaintColorAuthoritative.PaintColorEnum, Color>();
    // The state of the list of color definitions the last time colors were read from this definitions list
    private List<ColorDefinition> _lastColors = new List<ColorDefinition>();

    /* PROPERTIES */
    // Gets the color definitions as a dictionary
    public Dictionary<PaintColorAuthoritative.PaintColorEnum, Color> colors
    {
        get
        {
            if (_colors == _lastColors && _lastColors != null)
            {
                return colorDictionary;
            }
            else
            {
                colorDictionary.Clear();
                foreach (ColorDefinition def in _colors)
                {
                    colorDictionary.Add(def.colorType, def.color);
                }
                _lastColors = _colors;
                return colorDictionary;
            }
        }
    }

    /* METHODS */

    public static implicit operator Dictionary<PaintColorAuthoritative.PaintColorEnum, Color>(PaintColorDefinitions def)
    {
        return def.colors;
    }
}

/// <summary>
/// Class which enables easier inspector modifications of color definitions
/// </summary>
[System.Serializable]
public class ColorDefinition
{
    // The color type for which a specific color will be defined
    public PaintColorAuthoritative.PaintColorEnum colorType = PaintColorAuthoritative.PaintColorEnum.Null;
    // The definition of that color
    public Color color = Color.white;
}
