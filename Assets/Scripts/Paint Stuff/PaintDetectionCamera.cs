/** ------------------------------------------------------------------------------
- Filename:   PaintDetectionCamera
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/16
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/** PaintDetectionCamera.cs
 * --------------------- Description ------------------------
 * Reads paint colors that the camera renders and can be read from
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/16       Created file.                                       Jonathan
 */
[RequireComponent(typeof(Camera))]
public class PaintDetectionCamera : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public enum Modes
    {
        All, Threshold, Greatest, GreatestWThreshhold
    }
    [Tooltip("The range of paint absorbed when the pdc is over it")]
    public float absorbtionRange = 2f;
    [Tooltip("The rate of paint absorbed when the pdc is over it")]
    public float absorbtionRate = .1f;
    /* PRIVATE MEMBERS */
    // The camera used to render paint
    private Camera _thisCamera = null;
    [Tooltip("Compute shader that is used to collect paint colors from a small render texture")]
    [SerializeField] private ComputeShader colorResolutionShader = null;
    [Tooltip("The method by which this paint detection camera decides which colors to report")]
    [SerializeField] private Modes mode = Modes.Greatest;
    [Tooltip("The amount of coverage required before this pdc reports a given color")]
    [Range(0.0f, 1.0f)] [SerializeField] private float coveragePercentageThreshold = 0.5f;
    // The colors last collected
    private Color[] _colors;
    // Whether colors have been collected this frame
    private bool harvestedThisFrame = false;
    // Whether this component has been set up.
    private bool setup = false;

    #region data structures declared outside of get operations to reduce memory garbage
    // Amounts of each color (used in get color operations)
    private Dictionary<float, int> _colorsAmounts = new Dictionary<float, int>();
    // Hue-> color dictionary (used in get color operations)
    private Dictionary<float, Color> _colorHues = new Dictionary<float, Color>();
    // Set of collected colors (used in get color operations)
    private HashSet<Color> _collectedColors = new HashSet<Color>();
    private List<Color> _getColorsResult = new List<Color>();
    #endregion

    // Sanity check texture2D for testing PaintColorAuthoritative.GetClosestColor
    //[SerializeField] private Texture2D sanity;

    /* PROPERTIES */

    /// <summary>
    /// Accessor for the camera
    /// </summary>
    private Camera thisCamera
    {
        get
        {
            if (_thisCamera == null)
            {
                _thisCamera = GetComponent<Camera>();
            }
            return _thisCamera;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up this paint detection camera
    /// </summary>
    private void Awake()
    {
        //QualitySettings.vSyncCount = 0;  // VSync must be disabled
        //Application.targetFrameRate = 500;
        //sanity = new Texture2D(360, 1);
        Setup();
        if (SingleLayerPaintManagerV2.PDCS != null)
        {
            SingleLayerPaintManagerV2.PDCS.Add(this);
        }
    }
    private void OnDestroy()
    {
        if (SingleLayerPaintManagerV2.PDCS != null)
        {
            SingleLayerPaintManagerV2.PDCS.Remove(this);
        }
    }

    // For debug purposes, print the amount of colors found
    private void Update()
    {
        //Debug.Log(GetColors().Count());
    }

    /// <summary>
    /// Description:
    /// Resets harvested parameter (ensures reading colors via compute shader only once per frame)
    /// </summary>
    private void LateUpdate()
    {
        harvestedThisFrame = false;
    }

    /// <summary>
    /// Description:
    /// Sets up the paint camera with a render texture
    /// </summary>
    private void Setup()
    {
        if (!setup)
        {
            if (thisCamera.targetTexture != null)
            {
                RenderTexture tex = new RenderTexture(_thisCamera.targetTexture);
                tex.enableRandomWrite = true;
                tex.Create();
                _thisCamera.targetTexture = tex;
            }
            else
            {
                RenderTexture tex = new RenderTexture(5, 10, 1);
                tex.enableRandomWrite = true;
                tex.Create();
                _thisCamera.targetTexture = tex;
            }
            ResetColors();
            setup = true;
        }


        //if (Application.isPlaying)
        //{
        //    IEnumerable<Color> SanityColors()
        //    {
        //        for (int i = 0; i < 360; i++)
        //        {
        //            if (i == 359)
        //            {
        //                Debug.Log("Should be red");
        //            }
        //            Color input = Color.HSVToRGB((float)i / 360.0f, 1f, 1f);
        //            yield return PaintColorAuthoritative.GetClosestColor(input);
        //        }
        //    }
        //    sanity.SetPixels(SanityColors().ToArray());
        //    sanity.Apply();
        //}
    }

    /// <summary>
    /// Description:
    /// Resets color buffer
    /// </summary>
    private void ResetColors()
    {
        _colors = new Color[thisCamera.targetTexture.width * thisCamera.targetTexture.height];
    }

    /// <summary>
    /// Description:
    /// Reads colors from camera's target texture
    /// </summary>
    private void ComputeColors()
    {
        Setup();
        if (colorResolutionShader != null)
        {
            ResetColors();

            ComputeBuffer dataBuffer = new ComputeBuffer(_colors.Length, sizeof(float) * 4);
            dataBuffer.SetData(_colors);

            int kernel = colorResolutionShader.FindKernel("CSMain");

            if (kernel >= 0)
            {
                colorResolutionShader.SetBuffer(kernel, "colors", dataBuffer);
                colorResolutionShader.SetTexture(kernel, "input", thisCamera.targetTexture);
                colorResolutionShader.SetInt("width", thisCamera.targetTexture.width);
                colorResolutionShader.SetInt("height", thisCamera.targetTexture.height);
                colorResolutionShader.Dispatch(kernel, Mathf.CeilToInt(thisCamera.targetTexture.width / 32.0f), Mathf.CeilToInt(thisCamera.targetTexture.height / 32.0f), 1);
            }

            dataBuffer.GetData(_colors);

            dataBuffer.Dispose();
            harvestedThisFrame = true;
        }
    }

    /// <summary>
    /// Description:
    /// Returns colors read by this paint detection camera
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Color> GetColors()
    {
        if (!harvestedThisFrame)
        {
            ComputeColors();
            _getColorsResult.Clear();
        }
        else
        {
            foreach (Color c in _getColorsResult)
            {
                yield return c;
            }
            yield break;
        }
        switch(mode)
        {
            case Modes.Threshold:
                foreach (var c in GetColors_Threshhold())
                {
                    _getColorsResult.Add(c);
                    yield return c;
                }
                break;
            case Modes.Greatest:
                foreach (var c in GetColors_Greatest())
                {
                    _getColorsResult.Add(c);
                    yield return c;
                }
                break;
            case Modes.All:
                foreach (var c in GetColors_All())
                {
                    _getColorsResult.Add(c);
                    yield return c;
                }
                break;
            case Modes.GreatestWThreshhold:
                foreach (var c in GetColors_ThreshholdMax())
                {
                    _getColorsResult.Add(c);
                    yield return c;
                }
                break;
        }
    }

    private IEnumerable<Color> GetColors_All()
    {
        _collectedColors.Clear();
        for (int i = 0; i < _colors.Length; i++)
        {
            Color next = _colors[0];
            if (next.a > 0.1f)
            {
                next = PaintColorAuthoritative.GetClosestColor(next);
                if (!_collectedColors.Contains(next))
                {
                    _collectedColors.Add(next);
                    yield return next;
                }
            }
        }
    }

    private IEnumerable<Color> GetColors_Greatest()
    {
        _colorsAmounts.Clear();
        _colorHues.Clear();
        int totalArea = thisCamera.targetTexture.width * thisCamera.targetTexture.height;
        for (int i = 0; i < _colors.Length; i++)
        {
            Color next = _colors[i];
            if (next.a > 0.1f)
            {
                next = PaintColorAuthoritative.GetClosestColor(next);
                float[] nextHSV = new float[3];
                Color.RGBToHSV(next, out nextHSV[0], out nextHSV[1], out nextHSV[2]);
                foreach (float key in _colorsAmounts.Keys)
                {
                    if (Mathf.Abs(key - nextHSV[0]) <= PaintColorAuthoritative.COMPARISONRANGE)
                    {
                        nextHSV[0] = key;
                    }
                }
                if (!_colorsAmounts.ContainsKey(nextHSV[0]))
                {
                    _colorsAmounts.Add(nextHSV[0], 0);
                    _colorHues.Add(nextHSV[0], next);
                }
                _colorsAmounts[nextHSV[0]] = _colorsAmounts[nextHSV[0]] + 1;
            }
        }
        int max = int.MinValue;
        float h = 0;
        foreach (var kvp in _colorsAmounts)
        {
            if (kvp.Value > max)
            {
                max = kvp.Value;
                h = kvp.Key;
            }
        }
        if (_colorHues.ContainsKey(h))
        {
            yield return _colorHues[h];
        }
    }

    private IEnumerable<Color> GetColors_ThreshholdMax()
    {

        _colorsAmounts.Clear();
        _colorHues.Clear();
        int totalArea = thisCamera.targetTexture.width * thisCamera.targetTexture.height;
        for (int i = 0; i < _colors.Length; i++)
        {
            Color next = _colors[i];
            if (next.a > 0.1f)
            {
                next = PaintColorAuthoritative.GetClosestColor(next);
                float[] nextHSV = new float[3];
                Color.RGBToHSV(next, out nextHSV[0], out nextHSV[1], out nextHSV[2]);
                foreach (float key in _colorsAmounts.Keys)
                {
                    if (Mathf.Abs(key - nextHSV[0]) <= PaintColorAuthoritative.COMPARISONRANGE)
                    {
                        nextHSV[0] = key;
                    }
                }
                if (!_colorsAmounts.ContainsKey(nextHSV[0]))
                {
                    _colorsAmounts.Add(nextHSV[0], 0);
                    _colorHues.Add(nextHSV[0], next);
                }
                _colorsAmounts[nextHSV[0]] = _colorsAmounts[nextHSV[0]] + 1;
            }
        }
        int max = int.MinValue;
        float h = 0;
        foreach (var kvp in _colorsAmounts)
        {
            if (kvp.Value > max)
            {
                max = kvp.Value;
                h = kvp.Key;
            }
        }
        if (_colorHues.ContainsKey(h) && _colorsAmounts[h] > (int)(coveragePercentageThreshold * totalArea))
        {
            yield return _colorHues[h];
        }
    }

    private IEnumerable<Color> GetColors_Threshhold()
    {
        _colorsAmounts.Clear();
        int totalArea = thisCamera.targetTexture.width * thisCamera.targetTexture.height;
        for (int i = 0; i < _colors.Length; i++)
        {
            Color next = _colors[i];
            if (next.a > 0.1f)
            {
                next = PaintColorAuthoritative.GetClosestColor(next);
                float[] nextHSV = new float[3];
                Color.RGBToHSV(next, out nextHSV[0], out nextHSV[1], out nextHSV[2]);
                foreach (float key in _colorsAmounts.Keys)
                {
                    if (Mathf.Abs(key - nextHSV[0]) <= PaintColorAuthoritative.COMPARISONRANGE)
                    {
                        nextHSV[0] = key;
                    }
                }
                if (!_colorsAmounts.ContainsKey(nextHSV[0]))
                {
                    _colorsAmounts.Add(nextHSV[0], 0);
                }
                if (_colorsAmounts[nextHSV[0]] >= (int)(coveragePercentageThreshold * totalArea))
                {
                    yield return next;
                }
                _colorsAmounts[nextHSV[0]] = _colorsAmounts[nextHSV[0]] + 1;
            }
        }
    }
}
