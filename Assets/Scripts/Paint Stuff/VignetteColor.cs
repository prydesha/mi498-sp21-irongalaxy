/** ------------------------------------------------------------------------------
- Filename:   VignetteColor
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/03/10
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/** VignetteColor.cs
 * --------------------- Description ------------------------
 * Controlls the color of a vignette
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/10       Created file.                                       Jonathan
 */
[RequireComponent(typeof(Volume))]
public class VignetteColor : MonoBehaviour
{
    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */
    [Tooltip("Color delegator to match")]
    [SerializeField] private OverlapColorDelegator delegator = null;

    // The volume to modify
    private Volume volume = null;
    // The target vignette effect
    private Vignette vignette = null;
    // Coroutine handling color lerping
    private Coroutine lerpCoroutine = null;

    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up the color matcher
    /// </summary>
    private void Awake()
    {
        if (volume == null)
        {
            volume = GetComponent<Volume>();
        }
        if (volume != null)
        {
            volume.profile.TryGet(out vignette);
        }
        if (delegator != null)
        {
            delegator.colorChangeEvent += SetColor;
        }
    }

    private void OnDisable()
    {
        if (delegator != null)
        {
            delegator.colorChangeEvent -= SetColor;
        }
    }

    private void SetVignetteColor(Color color)
    {
        if (vignette != null)
        {
            ColorParameter param = new ColorParameter(color, true);
            vignette.color.SetValue(param);
        }
    }

    public void SetColor(Color color)
    {
        if (lerpCoroutine != null)
        {
            StopCoroutine(lerpCoroutine);
        }
        lerpCoroutine = StartCoroutine(LerpColorCoroutine(color));
    }

    public IEnumerator LerpColorCoroutine(Color target)
    {
        Color start = vignette.color.value;
        for (float t = 0; t < 0.1f; t+= Time.deltaTime)
        {
            Color newColor = Color.Lerp(start, target, t / 0.1f);
            SetVignetteColor(newColor);
            yield return null;
        }
        SetVignetteColor(target);
    }
}
