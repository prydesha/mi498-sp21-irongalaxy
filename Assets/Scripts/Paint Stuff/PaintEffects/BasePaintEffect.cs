/** ------------------------------------------------------------------------------
- Filename:   BasePaintEffect
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/17
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/** BasePaintEffect.cs
 * --------------------- Description ------------------------
 * Base Class for classes which implement more complex paint effects
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/17       Created file.                                       Jonathan
 */
public abstract class BasePaintEffect : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    [SerializeField] protected PaintColorAuthoritative.PaintColorEnum _paintColor = PaintColorAuthoritative.PaintColorEnum.Null;

    /* PRIVATE MEMBERS */

    // The color delegator that decides paint effects
    private PaintDetectionCamera paintDetectionCamera = null;
    // Whether this effect's pdc is detecting the triggering paint color
    private bool _inPaint = false;

    /* PROPERTIES */

    // Determines whether the entity is in paint
    public bool inPaint => _inPaint;

    /* METHODS */

    /// <summary>
    /// Description:
    /// Called when starting up
    /// </summary>
    private void Awake()
    {
        Setup();
    }

    /// <summary>
    /// Description:
    /// Called every update
    /// </summary>
    protected virtual void Update()
    {
        if (paintDetectionCamera != null && paintDetectionCamera.GetColors().Contains(PaintColorAuthoritative.GetColor(_paintColor)))
        {
            _inPaint = true;
        }
        else
        {
            _inPaint = false;
        }
    }

    /// <summary>
    /// Description:
    /// Sets up the paint effect handler
    /// </summary>
    protected virtual void Setup()
    {
        if (paintDetectionCamera == null)
        {
            paintDetectionCamera = GetComponentInChildren<PaintDetectionCamera>();
        }
    }

    /// <summary>
    /// Description:
    /// Function to apply paint effects. Implementation will be paint color specific.
    /// </summary>
    protected virtual void ApplyPaintEffect()
    {

    }
}
