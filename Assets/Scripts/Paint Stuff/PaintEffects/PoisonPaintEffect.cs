/** ------------------------------------------------------------------------------
- Filename:   BasePaintEffect
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/17
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** BasePaintEffect.cs
 * --------------------- Description ------------------------
 * Class that applies poison effect to health when in paint of a certain color
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/17       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(Health))]
public class PoisonPaintEffect : BasePaintEffect
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    [SerializeField] private float _poisonTickTime = 0.5f;
    [SerializeField] private float _poisonDps = 1.5f;
    private Health _health = null;
    private float t = 0;

    /* PROPERTIES */
    protected float _poisonDamage
    {
        get
        {
            return _poisonDps * _poisonTickTime;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description every update apply paint effects
    /// </summary>
    protected override void Update()
    {
        base.Update();

        if (inPaint)
        {
            t += Time.deltaTime;
            if (t > _poisonTickTime)
            {
                ApplyPaintEffect();
                t = 0;
            }
        }
        else
        {
            t = 0;
        }
    }

    protected override void Setup()
    {
        base.Setup();
        _health = GetComponent<Health>();
    }

    /// <summary>
    /// Description:
    /// Function to apply paint effects. Implementation will be paint color specific.
    /// </summary>
    protected override void ApplyPaintEffect()
    {
        if (_health != null)
        {
            _health.Damage(_poisonDamage);
        }
    }
}
