/** ------------------------------------------------------------------------------
- Filename:   IcePaintEffect
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/17
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** IcePaintEffect.cs
 * --------------------- Description ------------------------
 * Determines if the entity is in icy paint
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/17       Created file.                                       Jonathan
 */
public class IcePaintEffect : BasePaintEffect
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    public bool movementImpaired => inPaint;

    /* METHODS */
}
