/** ------------------------------------------------------------------------------
- Filename:   LightningPaintEffect
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/17
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** LightningPaintEffect.cs
 * --------------------- Description ------------------------
 * Provides support for launching projectiles if in a certain color of paint
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/17       Created file.                                       Jonathan
 */
public class LightningPaintEffect : BasePaintEffect
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */

    [SerializeField] private GameObject _lightningAura = null;
    [SerializeField] private GameObject _lightningProjectile = null;

    /* PROPERTIES */


    /* METHODS */

    protected override void Update()
    {
        base.Update();

        if (_lightningAura != null)
        {
            _lightningAura.SetActive(inPaint);
        }
    }

    public void CreateLightningProjectile(Vector2 position, Vector2 direction)
    {
        if (inPaint)
        {
            Color projectileColor = PaintColorAuthoritative.GetColor(_paintColor);
            if (_lightningProjectile != null)
            {
                BaseProjectile.FireProjectile(_lightningProjectile, position - direction, -direction, projectileColor);
            }
        }
    }
}
