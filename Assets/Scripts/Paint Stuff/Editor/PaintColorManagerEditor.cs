/** ------------------------------------------------------------------------------
- Filename:   PaintColorManagerEditor
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/09
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

/** PaintColorManagerEditor.cs
 * --------------------- Description ------------------------
 * Custom inspector extension for PlayerPaintColorManager class
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/09       Created file.                                       Jonathan
 */
[CustomEditor(typeof(PaintColorManager))]
public class PaintColorManagerEditor : Editor
{
    Color paintColor = Color.white;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        PaintColorManager PCM = (PaintColorManager)target;

        paintColor = PCM.paintColor;
        Color newColor = EditorGUILayout.ColorField("Paint Color", paintColor);
        if (paintColor != newColor)
        {
            PCM.paintColor = newColor;
            if (PCM.gameObject.scene.rootCount == 0)
            {
                EditorUtility.SetDirty(target);
                EditorSceneManager.MarkSceneDirty((PCM).gameObject.scene);
            }
        }
    }
}
