/** ------------------------------------------------------------------------------
- Filename:   PaintColorDefinitionsEditor
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/30
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/** PaintColorDefinitionsEditor.cs
 * --------------------- Description ------------------------
 * Custom editor for paint color definitions
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/30       Created file.                                       Jonathan
 */
 [CustomEditor(typeof(PaintColorDefinitions))]
public class PaintColorDefinitionsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        PaintColorDefinitions pcDef = target as PaintColorDefinitions;

        if (GUILayout.Button("Assign"))
        {
            PaintColorAuthoritative.SetDefs(pcDef);
        }
    }
}
