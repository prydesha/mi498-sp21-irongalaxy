/** ------------------------------------------------------------------------------
- Filename:   PaintColorWindow
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/16
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


/** PaintColorWindow.cs
 * --------------------- Description ------------------------
 * An editor window that allows editing paint colors
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/16       Created file.                                       Jonathan
 */
 
public class PaintColorWindow : EditorWindow
{
    public static Dictionary<PaintColorAuthoritative.PaintColorEnum, Color> colors;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Paint/Paint Color Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        PaintColorWindow window = (PaintColorWindow)EditorWindow.GetWindow(typeof(PaintColorWindow));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Paint Colors", EditorStyles.boldLabel);

        if (colors == null)
        {
            colors = new Dictionary<PaintColorAuthoritative.PaintColorEnum, Color>()
            {
                {PaintColorAuthoritative.PaintColorEnum.Null,   new Color(((float)0x00/255.0f), ((float)0x00/255.0f), ((float)0x00/255.0f), 0.0f) },
                {PaintColorAuthoritative.PaintColorEnum.Red,    new Color(((float)0xff/255.0f), ((float)0x2c/255.0f), ((float)0x4f/255.0f), 1.0f) },
                {PaintColorAuthoritative.PaintColorEnum.Yellow, new Color(((float)0xff/255.0f), ((float)0xdf/255.0f), ((float)0x42/255.0f), 1.0f) },
                {PaintColorAuthoritative.PaintColorEnum.Green,  new Color(((float)0x28/255.0f), ((float)0xd4/255.0f), ((float)0x31/255.0f), 1.0f) },
                {PaintColorAuthoritative.PaintColorEnum.Blue,   new Color(((float)0x20/255.0f), ((float)0x46/255.0f), ((float)0xef/255.0f), 1.0f) },
            };
        }

        foreach (PaintColorAuthoritative.PaintColorEnum color in PaintColorAuthoritative.PaintColorEnum.GetValues(typeof(PaintColorAuthoritative.PaintColorEnum)))
        {
            colors[color] = EditorGUILayout.ColorField(colors[color]);
        }
    }
}
