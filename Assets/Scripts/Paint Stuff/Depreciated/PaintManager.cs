/** ------------------------------------------------------------------------------
- Filename:   PaintManager
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/24
- Created by: Andrew Smigielski
------------------------------------------------------------------------------- */
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;

/** PaintManager.cs
 * --------------------- Description ------------------------
 *The Job of this manager is to properly draw textures on the canvas
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/24       Created file.                                       smigi
 */

public struct DrawThreadStruct
{
    public Vector2 pixelUV;
    public int width;
    public int height;
    public int texWidth;
    public int texHeight;
    public int curX;
    public int curY;
    public int minX;
    public int minY;
    public int canvasMaxX;
    public int canvasMaxY;
    public int arrayWidth;
    public int texArrayWidth;
}

public class PaintManager : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    
    public Texture2D Attack;
    [Tooltip("List of Sprites that make up the drawable surface in map format, Sprites[y][x]")]
    public List<List<SpriteRenderer>> Sprites = new List<List<SpriteRenderer>>();
    [Tooltip("Gameobject that the spawner script is attatched to")]
    public GameObject Spawner;
    [Tooltip("Default Sprite used for the canvas")]
    public GameObject DefaultSprite;
    [Tooltip("The Sprite Renderer used by the default sprite")]
    public SpriteRenderer DefaultSR;
    [Tooltip("Active Paint Manager")]
    public static PaintManager PM;

    /* PRIVATE MEMBERS */
    [Tooltip("List of all textures")]
    private List<Sprite> textures = new List<Sprite>();
    /* PROPERTIES */

    private List<Color> colorList = new List<Color> { Color.red, Color.blue, Color.yellow };
    private int curColor = 0;
    private int curAngle = 0;
    public ComputeShader ComputeDraw = null;
    /* METHODS */



    static Color[] rotateSquare(Color[] arr, double phi, Texture2D originTexture)
    {
        int x;
        int y;
        int i;
        int j;
        double sn = Math.Sin(phi);
        double cs = Math.Cos(phi);
        Color[] arr2 = originTexture.GetPixels();
        int W = originTexture.width;
        int H = originTexture.height;
        int xc = W / 2;
        int yc = H / 2;
        for (j = 0; j < H; j++)
        {
            for (i = 0; i < W; i++)
            {
                arr2[j * W + i] = new Color(0, 0, 0, 0);
                x = (int)(cs * (i - xc) + sn * (j - yc) + xc);
                y = (int)(-sn * (i - xc) + cs * (j - yc) + yc);
                if ((x > -1) && (x < W) && (y > -1) && (y < H))
                {
                    arr2[j * W + i] = arr[y * W + x];
                }
            }
        }
        return arr2;
    }
    public static Color[] RotateImage(Texture2D originTexture, int angle)
    {
        Texture2D result;
        result = new Texture2D(originTexture.width, originTexture.height);
        Color[] pix1 = result.GetPixels();
        Color[] pix2 = originTexture.GetPixels();
        int W = originTexture.width;
        int H = originTexture.height;
        int x = 0;
        int y = 0;
        Color[] pix3 = rotateSquare(pix2, (Math.PI / 180 * (double)angle), originTexture);
        for (int j = 0; j < H; j++)
        {
            for (var i = 0; i < W; i++)
            {
                //pix1[result.width/2 - originTexture.width/2 + x + i + result.width*(result.height/2-originTexture.height/2+j+y)] = pix2[i + j*originTexture.width];
                pix1[result.width / 2 - W / 2 + x + i + result.width * (result.height / 2 - H / 2 + j + y)] = pix3[i + j * W];
            }
        }
       // result.SetPixels32(pix1);
       // result.Apply();
        return pix1;
    }
    /**
 * Draws a mesh on the canvas based on the attack inputed
 *
 * @param {Vector2} World position of attack mesh.
 * @param {Texture2D} Texture of attack.
 * @param {Color} Color of attack.
 */

    public Color32[] RunComputeShader(DrawThreadStruct drawThreads, Color32[] InputColor, Color32[] AttackColor)
    {
        // pixelUV, width, height, tex, curX, curY,attackColors, appliedColors,minX,minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth
        int pixelUVSize = sizeof(float) * 2;
        int widthSize = sizeof(int) * 1;
        int heightSize = sizeof(int) * 1;
        int texWidthSize = sizeof(int) * 1;
        int texHeightSize = sizeof(int) * 1;
        int curXSize = sizeof(int) * 1;
        int curYSize = sizeof(int) * 1;
        int minXSize = sizeof(int) * 1;
        int minYSize = sizeof(int) * 1;
        int canvasMaxXSize = sizeof(int) * 1;
        int canvasMaxYSize = sizeof(int) * 1;
        int arrayWidth = sizeof(int) * 1;
        int texArrayWidth = sizeof(int) * 1;

        int totalSize = pixelUVSize + widthSize + heightSize + texWidthSize + texHeightSize + curXSize + curYSize   + minXSize + minYSize + canvasMaxXSize + canvasMaxYSize + arrayWidth + texArrayWidth;

        Color32[] ColorArray = new Color32[0].Concat(InputColor).ToArray();

        ComputeBuffer dataBuffer = new ComputeBuffer(1, totalSize);
        ComputeBuffer inputColorBuffer = new ComputeBuffer(InputColor.Length,sizeof(float) * 4);
        ComputeBuffer AttackColorBuffer = new ComputeBuffer(AttackColor.Length,sizeof(float) * 4);
        ComputeBuffer returnColorBuffer = new ComputeBuffer(ColorArray.Length, sizeof(float) * 4);



        DrawThreadStruct[] temp = { drawThreads };
        dataBuffer.SetData(temp);
        inputColorBuffer.SetData(InputColor);
        AttackColorBuffer.SetData(AttackColor);
        returnColorBuffer.SetData(ColorArray);


        int kernel = ComputeDraw.FindKernel("CSMain");
        if (kernel >= 0 && drawThreads.width>0)
        {
            ComputeDraw.SetBuffer(kernel, "Data", dataBuffer);
            ComputeDraw.SetBuffer(kernel, "inputColor", inputColorBuffer);
            ComputeDraw.SetBuffer(kernel, "attackColor", AttackColorBuffer);
            ComputeDraw.SetBuffer(kernel, "returnColorArray", returnColorBuffer);
            ComputeDraw.Dispatch(kernel, Mathf.CeilToInt(drawThreads.width/(32.0f*10.0f)), Mathf.CeilToInt(drawThreads.height / 32.0f), 1);
        }
        returnColorBuffer.GetData(ColorArray);
        dataBuffer.Dispose();
        inputColorBuffer.Dispose();
        AttackColorBuffer.Dispose();
        returnColorBuffer.Dispose();

        List<Color32> stop = ColorArray.ToList();
        stop = stop.GetRange(0, 100);
;        return ColorArray;
    }


    public void Draw(Vector2 Pos, Texture2D attack, Color32 color, int Angle)
    {

        int curX = (int)((Pos.x - Spawner.transform.position.x + (DefaultSR.sprite.bounds.size.x / 2)) / DefaultSR.sprite.bounds.size.x);
        int curY = (int)((Pos.y - Spawner.transform.position.y + (DefaultSR.sprite.bounds.size.y / 2)) / DefaultSR.sprite.bounds.size.y);
        if (curX < 0 || curX > Sprites[0].Count - 1 || curY < 0 || curY > Sprites.Count - 1)
            return;
        Texture2D tex = Sprites[curY][curX].sprite.texture as Texture2D;
        //checking if texture being drawn on needs a unique new texture

        // pixelUV position of pixel in coords relative to texture
        Vector2 pixelUV = (Pos - new Vector2(Sprites[curY][curX].transform.position.x, Sprites[curY][curX].transform.position.y));

        pixelUV.x = pixelUV.x / DefaultSR.bounds.size.x;
        pixelUV.y = pixelUV.y / DefaultSR.bounds.size.y;

        pixelUV += new Vector2(.5f, .5f);

        pixelUV.x *= tex.height;
        pixelUV.y *= tex.width;


        Color32[] attackColors = attack.GetPixels32();//RotateImage(attack,Angle);

        var width = attack.width;
        var height = attack.height;

        var maxX = (int)(pixelUV.x + (width - width / 2)) / tex.width;
        var maxY = (int)(pixelUV.y + (height - height / 2)) / tex.height;
        var minX = ((int)(pixelUV.x + (0 - width / 2)) / tex.width) ;
        var minY = ((int)(pixelUV.y + (0 - height / 2)) / tex.height) ;
        if ((pixelUV.x + (0 - width / 2)) / tex.width < 0)
        {
            minX = ((int)(pixelUV.x + (0 - width / 2)) / tex.width) - 1;
        }
        if ((pixelUV.y + (0 - height / 2)) / tex.height < 0)
        {
            minY = ((int)(pixelUV.x + (0 - width / 2)) / tex.width) -1;
        }

        List<Texture2D> appliedTex = new List<Texture2D>();
        List<RenderTexture> appliedRTex = new List<RenderTexture>();

        var texArrayWidth = 0;
        Color32[] appliedColors = { };
        List<Color32> tempAppliedColors = new List<Color32>();
        for (int y = curY + minY; y <= curY + maxY;y++)
        {
            for (int x = curX + minX; x <= curX + maxX; x++)
            {
                if(x< 0|| y< 0 || x > Sprites[0].Count-1 || y > Sprites.Count-1)
                {
                    continue;
                }
                Texture2D temptex = Sprites[y][x].sprite.texture as Texture2D;
               // RenderTexture tempRtex = Sprites[y][x].material.GetTexture("_SecondaryTex") as RenderTexture ;
                //checking if texture being drawn on needs a unique new texture
                if (temptex.name == "White-Square")
                {

                    var newTex = Instantiate(temptex);
                    var sprite = Sprite.Create(newTex, DefaultSR.sprite.textureRect, new Vector2(.5f, .5f), Sprites[y][x].sprite.pixelsPerUnit);
                     
                    temptex = sprite.texture;
                    textures.Add(sprite);
                    Sprites[y][x].sprite = sprite;
                }

                /*
                if (tempRtex.name == "256X")
                {


                    var newRTex = new RenderTexture(tempRtex.width,tempRtex.height,1); //Instantiate(tempRtex);
                    newRTex.enableRandomWrite = true;
                    newRTex.Create();
                    // var sprite = Sprite.Create(newTex, DefaultSR.sprite.textureRect, new Vector2(.5f, .5f), Sprites[y][x].sprite.pixelsPerUnit);

                    // temptex = sprite.texture;
                    //textures.Add(sprite);
                    Sprites[y][x].material.SetTexture("_SecondaryTex", newRTex);
                    tempRtex = newRTex;
                }
                */
                //  appliedTex.Add(temptex);

                //var tempColors = temptex.GetPixels32();
                Color32[] tempArray = temptex.GetPixels32();
                texArrayWidth = tempArray.Length;
                tempAppliedColors.AddRange(tempArray);
               // appliedRTex.Add(tempRtex);
              //  var z = new Color32[appliedColors.Length + tempColors.Length];
              // appliedColors.CopyTo(z, 0);
              // tempColors.CopyTo(z, appliedColors.Length);
              // appliedColors = z;
              // appliedColors = appliedColors.Concat(tempColors).ToArray();

            }
        }
        appliedColors = tempAppliedColors.ToArray();
        var arrayWidth = maxX - minX +1;


        DrawThreadStruct DT = new DrawThreadStruct();
        DT.pixelUV =pixelUV;
        DT.width = width;
        DT.height = height;
        DT.texWidth = tex.width;
        DT.texHeight = tex.height;
        DT.curX = curX;
        DT.curY = curY;
        DT.minX= minX;
        DT.minY =minY;
        DT.canvasMaxX = Sprites[0].Count;
        DT.canvasMaxY = Sprites.Count;
        DT.arrayWidth = arrayWidth;
        DT.texArrayWidth = texArrayWidth;
        appliedColors = RunComputeShader(DT,appliedColors,attackColors);
        /*
        for (int y = 0; y < height; y++)
        {

           // StartCoroutine(DrawThread( y, pixelUV, width, height, tex, curX, curY, attackColors, appliedColors, minX, minY,Sprites[0].Count,Sprites.Count));
            for (int x = 0; x < width; x += 10)
            {
                
                    if (x + 10 < width)
                    { 
                        DrawPixel(x, y, pixelUV, width, height, tex, curX, curY,attackColors, appliedColors,minX,minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x+1, y, pixelUV, width, height, tex, curX, curY , attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x+2, y, pixelUV, width, height, tex, curX, curY , attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x+3, y, pixelUV, width, height, tex, curX, curY, attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x+4, y, pixelUV, width, height, tex, curX, curY, attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x+5, y, pixelUV, width, height, tex, curX, curY, attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x + 6, y, pixelUV, width, height, tex, curX, curY, attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x + 7, y, pixelUV, width, height, tex, curX, curY, attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x + 8, y, pixelUV, width, height, tex, curX, curY, attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                        DrawPixel(x + 9, y, pixelUV, width, height, tex, curX, curY, attackColors, appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                    }
                    else
                    {
                        while(x<width)
                        {
                            DrawPixel(x, y, pixelUV, width, height, tex, curX, curY,attackColors,appliedColors, minX, minY, Sprites[0].Count, Sprites.Count, arrayWidth, texArrayWidth);
                            x++;
                        }
                    }

            }
        }
        */

        var tempList = appliedColors.ToList();
        for (int i = 0; i < appliedColors.Length; i += texArrayWidth)
        {

            //var arrayheight = maxY + minY;
            var x = (i/texArrayWidth) % arrayWidth;
            var y = (i / texArrayWidth) / arrayWidth;
            if ((curY + minY + y) < 0 || curY + minY + y > Sprites.Count || curX + minX + x < 0 || curX + minX + x > Sprites[0].Count)
                continue;
            var temp = Sprites[curY + minY + y][curX + minX + x];
            var temp2 = tempList.GetRange(i, texArrayWidth ).ToArray();
            temp.sprite.texture.SetPixels32(temp2);

            temp.sprite.texture.Apply();
           // Sprites[0][0].sprite.texture.SetPixels32(temp2);
        }

        //applies the new textures for all sprites hit
     //   foreach (Texture2D i in appliedTex)
     //   {
      //      i.Apply();
     //   }
       // appliedTex.Clear();
    }



    void DrawPixel(int x,int y, Vector2 pixelUV,int width,int height, Texture2D tex, int curX,int curY, Color[] colors, Color[] appliedColors, int minX, int minY,int canvasMaxX,int canvasMaxY, int arrayWidth,int texArrayWidth)
    {

        if (colors[(y * width) + x] == Color.black)
        {
            int gridx = 0;
 

            //check for pixels that need to be drawn off the center texture
            if ((pixelUV.x + (x - width / 2)) / tex.width > 0)
            {
                gridx = (int)(pixelUV.x + (x - width / 2)) / tex.width;
            }
            else
            {
                gridx = (int)((pixelUV.x + (x - width / 2)) / tex.width) - 1;
            }
            int gridy = 0;
            if ((pixelUV.y + (y - height / 2)) / tex.height > 0)
            {
                gridy = (int)(pixelUV.y + (y - height / 2)) / tex.height;
            }
            else
            {
                gridy = (int)((pixelUV.y + (y - height / 2)) / tex.height) - 1;
            }
            if (curX + gridx < 0 || curY + gridy < 0 || curX + gridx > canvasMaxX - 1 || curY + gridy > canvasMaxY - 1)
            {
                return; 
            }
            var texX = (int)pixelUV.x + (x - width / 2) - (gridx * tex.width);
            var texY = (int)pixelUV.y + (y - height / 2) - (gridy * tex.height);
            // Find index 
            var i = (gridx - minX + ((gridy - minY) * arrayWidth)) * texArrayWidth + texX + (texY * tex.width);


            if (i < 0 || i > appliedColors.Length-1)
                return;
            appliedColors[(gridx - minX + ((gridy - minY) * arrayWidth)) * texArrayWidth + texX + (texY * tex.width)] = Color.red ;

        }
    } 

    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {

        if(PaintManager.PM == null)
        {
            PM = this;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (InputManager.Paint.isDown)
        {
            var tempPos = new Vector3(InputManager.MousePosition.x, InputManager.MousePosition.y,1);
            curColor++;
            if (curColor > colorList.Count - 1)
                curColor = 0;
            curAngle += 45;
           Draw(Camera.main.ScreenToWorldPoint(tempPos),Attack, colorList[curColor],curAngle);
        }
        if(Mouse.current.rightButton.isPressed)
        {
            var tempPos = new Vector3(InputManager.MousePosition.x, InputManager.MousePosition.y, 1);
            //Overwrite(Camera.main.ScreenToWorldPoint(tempPos), Attack, colorList[curColor], curAngle);
            Clear();
        }

    }




    private void Overwrite(Vector2 Pos, Texture2D attack, Color color, int Angle)
    {
        int curX = (int)((Pos.x - Spawner.transform.position.x + (DefaultSR.sprite.bounds.size.x / 2)) / DefaultSR.sprite.bounds.size.x);
        int curY = (int)((Pos.y - Spawner.transform.position.y + (DefaultSR.sprite.bounds.size.y / 2)) / DefaultSR.sprite.bounds.size.y);
        if (curX < 0 || curX > Sprites[0].Count - 1 || curY < 0 || curY > Sprites.Count - 1)
            return;
        Texture2D tex = Sprites[curY][curX].sprite.texture as Texture2D;
        if (tex.name == "White-Square")
        {

            var newTex = Instantiate(tex);
            var sprite = Sprite.Create(newTex, DefaultSR.sprite.textureRect, new Vector2(.5f, .5f), Sprites[curY][curX].sprite.pixelsPerUnit);

            textures.Add(sprite);
            tex = newTex;
            Sprites[curY][curX].sprite = sprite;
        }

        Sprites[curY][curX].sprite.texture.SetPixels(0,0,64,62,Attack.GetPixels());
        Sprites[curY][curX].sprite.texture.Apply();
    }
    private void Clear()
    {
        var Pos = InputManager.MousePosition;
        int curX = (int)((Pos.x - Spawner.transform.position.x + (DefaultSR.sprite.bounds.size.x / 2)) / DefaultSR.sprite.bounds.size.x);
        int curY = (int)((Pos.y - Spawner.transform.position.y + (DefaultSR.sprite.bounds.size.y / 2)) / DefaultSR.sprite.bounds.size.y);
        if (curX < 0 || curX > Sprites[0].Count - 1 || curY < 0 || curY > Sprites.Count - 1)
            return;
        Texture2D tex = Sprites[curY][curX].sprite.texture as Texture2D;

        // pixelUV position of pixel in coords relative to texture
        Vector2 pixelUV = (Pos - new Vector2(Sprites[curY][curX].transform.position.x, Sprites[curY][curX].transform.position.y));

        pixelUV.x = pixelUV.x / DefaultSR.bounds.size.x;
        pixelUV.y = pixelUV.y / DefaultSR.bounds.size.y;

        pixelUV += new Vector2(.5f, .5f);

        pixelUV.x *= tex.height;
        pixelUV.y *= tex.width;
        Color resetColor = new Color(255, 255, 255, 0);
        Color[] resetColorArray = tex.GetPixels();
        for (int i = 0; i < resetColorArray.Length; i++)
        {
            resetColorArray[i] = resetColor;
        }
        tex.SetPixels(resetColorArray);
        tex.Apply();
    }





    IEnumerator DrawThread( int y, Vector2 pixelUV, int width, int height, Texture2D tex, int curX, int curY, Color[] colors, List<List<Color[]>> appliedColors, int minX, int minY,int canvasMaxX, int canvasMaxY)
    {
        PaintThread thread = new PaintThread();
        thread.y = y;
        thread.pixelUV = pixelUV;
        thread.width = width;
        thread.height = height;
        thread.texHeight = tex.height;
        thread.texWidth = tex.width;
        thread.curX = curX;
        thread.curY = curY;
        thread.attackColors = colors;
        thread.appliedColors = appliedColors;
        thread.minX = minX;
        thread.minY = minY;
        thread.canvasMaxX= canvasMaxX;
        thread.canvasMaxY = canvasMaxY;
        thread.Start();
        yield return StartCoroutine(thread.WaitFor());
        thread.Abort();

    }


    #endregion
}



public class PaintThread : MultiThreading.ThreadedJob
{

    public int y;
    public Vector2 pixelUV;
    public int width;
    public int height;
    public int texWidth;
    public int texHeight;
    public int curX;
    public int curY;
    public Color[] attackColors;
    public List<List<Color[]>> appliedColors;
    public int minX;
    public int minY;
    public int canvasMaxX;
    public int canvasMaxY;

    protected override void ThreadFunction()
    {

        for (int x = 0; x < width; x += 10)
        {

            if (x + 10 < width)
            {
                DrawPixel(x, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 1, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 2, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 3, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 4, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 5, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 6, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 7, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 8, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                DrawPixel(x + 9, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
            }
            else
            {
                while (x < width)
                {
                    DrawPixel(x, y, pixelUV, width, height, texWidth, texHeight, curX, curY, attackColors, appliedColors, minX, minY);
                    x++;
                }
            }

        }
    }
    void DrawPixel(int x, int y, Vector2 pixelUV, int width, int height, int texWidth, int texHeight, int curX, int curY, Color[] colors, List<List<Color[]>> appliedColors, int minX, int minY)
    {

        if (colors[(y * width) + x] == Color.black)
        {
            int gridx = 0;
            //check for pixels that need to be drawn off the center texture
            if ((pixelUV.x + (x - width / 2)) / texWidth > 0)
            {
                gridx = (int)(pixelUV.x + (x - width / 2)) / texWidth;
            }
            else
            {
                gridx = (int)((pixelUV.x + (x - width / 2)) / texWidth) - 1;
            }
            int gridy = 0;
            if ((pixelUV.y + (y - height / 2)) / texHeight > 0)
            {
                gridy = (int)(pixelUV.y + (y - height / 2)) / texHeight;
            }
            else
            {
                gridy = (int)((pixelUV.y + (y - height / 2)) / texHeight) - 1;
            }
            if (curX + gridx < 0 || curY + gridy < 0 || curX + gridx > canvasMaxX - 1 || curY + gridy > canvasMaxY - 1)
            {
                return;
            }

            var temp = appliedColors[gridy - minY][gridx - minX];
            //texture coords
            var texX = (int)pixelUV.x + (x - width / 2) - (gridx * texWidth);
            var texY = (int)pixelUV.y + (y - height / 2) - (gridy * texHeight);
            temp[texX + (texY * texWidth)] = Color.red;
        }
    }

}