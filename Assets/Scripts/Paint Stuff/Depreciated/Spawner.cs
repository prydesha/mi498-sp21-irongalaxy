/** ------------------------------------------------------------------------------
- Filename:   Spawner
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/25
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** Spawner.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/25       Created file.                                       smigi
 */
public class Spawner : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public int width;
    public int height;

   

    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */



    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {


        for (int y = 0; y < height; y++)
        {
            PaintManager.PM.Sprites.Add(new List<SpriteRenderer>());
            for (int x = 0; x < width; x++)
            {
                var temp = Instantiate(PaintManager.PM.DefaultSprite, new Vector3(x * PaintManager.PM.DefaultSR.sprite.bounds.size.x, y * PaintManager.PM.DefaultSR.sprite.bounds.size.y, 0), PaintManager.PM.DefaultSprite.transform.rotation);
                PaintManager.PM.Sprites[y].Add(temp.GetComponent<SpriteRenderer>());
            }
        }
    }

    #endregion
}
