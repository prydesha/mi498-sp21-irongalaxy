/** ------------------------------------------------------------------------------
- Filename:   PaintCamera
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/31
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/** PaintCamera.cs
 * --------------------- Description ------------------------
 * Script that contains classes, structs, and functions necessary to communicate with the compute shader that 
 * determines where paint splatters appear, and what they affect.
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/31       Created file.                                       Jonathan
 */

#region Structs for compute shader communication
public struct PaintSplatter
{
    public Vector2 position;
    public float rotation;
    public Color color;
    public uint splatterTexID;
}

public struct PaintBounds
{
    public Vector2 position;
    public Vector2 size;
}
#endregion

#region data structure for keeping track of paint splatters in reverse-application order
// https://stackoverflow.com/questions/384042/can-i-limit-the-depth-of-a-generic-stack
public class DropOutStack<T> : IEnumerable<T>
{
    private T[] items;
    private int top = 0;
    private int _Count = 0;

    public int Length
    {
        get
        {
            return _Count;
        }
    }

    public DropOutStack(int capacity)
    {
        items = new T[capacity];
    }

    public void Push(T item)
    {
        items[top] = item;
        top = (top + 1) % items.Length;
        _Count = Mathf.Clamp(_Count + 1, 0, items.Length);
    }
    public T Pop()
    {
        top = (items.Length + top - 1) % items.Length;
        _Count = Mathf.Clamp(_Count - 1, 0, items.Length);
        return items[top];
    }

    public IEnumerator<T> GetEnumerator()
    {
        int runOnceCheck = top+1;
        for (int i = top; i != runOnceCheck; i = (items.Length + i - 1) % items.Length)
        {
            runOnceCheck = top;
            yield return items[i];
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
#endregion

/// <summary>
/// Class which handles communication with a compute shader to create paint effects
/// </summary>
[RequireComponent(typeof(Camera))]
public class PaintCamera : MonoBehaviour
{
    #region Variables
    /* PUBLIC MEMBERS */
    // The maximum splatters that can be kept track of
    const int maxSplatters = 2000;

    // Static reference to an instance of this class (Singleton)
    public static PaintCamera Instance = null;

    [Tooltip("The shader to use to compute paint effects")]
    public ComputeShader paintShader = null;
    [Tooltip("The texture of a splatter to draw")]
    public Texture2D splatTexture = null;
    

    /* PRIVATE MEMBERS */
    [Tooltip("The color of paint to draw with")]
    [SerializeField] private Color paintColor = Color.red;

    // The camera view that this script creates a painted view of
    private Camera _paintCameraCamera = null;
    // The render texture used to contain that view
    private RenderTexture _rTexture = null;
    // A structure holding paint splatters in reverse application order
    private DropOutStack<PaintSplatter> _splatters = new DropOutStack<PaintSplatter>(maxSplatters);

    /* PROPERTIES */

    // Accessor for the output render texture
    public RenderTexture rTexture
    {
        get
        {
            if (_rTexture == null)
            {
                _rTexture = new RenderTexture(rTexWidth, rTexHeight, 16);
                _rTexture.enableRandomWrite = true;
                _rTexture.Create();
            }
            return _rTexture;
        }
    }

    // Accessor for the camera offset
    private Vector2 offset
    {
        get
        {
            Vector2 cameraWorldSize = paintCameraCamera.ViewportToWorldPoint(Vector2.one) - paintCameraCamera.ViewportToWorldPoint(Vector2.zero);
            Vector2 cameraWorldPosition = paintCameraCamera.transform.position;
            Vector2 renderTextureCoordsCameraOffset = WorldToPixelCoordinates(cameraWorldPosition - (cameraWorldSize / 2));

            return renderTextureCoordsCameraOffset;
        }
    }

    // Accessor for the paint camera
    public Camera paintCameraCamera
    {
        get
        {
            if (_paintCameraCamera == null)
            {
                _paintCameraCamera = GetComponent<Camera>();
            }
            return _paintCameraCamera;
        }
    }

    // The width of the render texture
    private int rTexWidth
    {
        get
        {
            return paintCameraCamera.pixelWidth / 10;// / 8;
        }
    }
    // The height of the render texture
    private int rTexHeight
    {
        get
        {
            return paintCameraCamera.pixelHeight / 10;// / 8;
        }
    }
    #endregion

    #region Methods
    /* METHODS */

    /// <summary>
    /// Description:
    /// Creates singleton instance (do not call DontDestroyOnLoad())
    /// </summary>
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    /// <summary>
    /// Description:
    /// Every update, add a paint splatter depending on input
    /// Draw paint splatters to render texture
    /// </summary>
    private void Update()
    {
        if (InputManager.Paint.justPressed)
        {
            //Vector2 cameraWorldSize = paintCameraCamera.ViewportToWorldPoint(Vector2.one) - paintCameraCamera.ViewportToWorldPoint(Vector2.zero);
            //Vector2 cameraPixelSize = paintCameraCamera.pixelRect.size;
            //Vector2 paintLocation = paintCameraCamera.ScreenToWorldPoint(InputManager.MousePosition);
            //AddPaintSplatter(paintColor, paintLocation, Random.Range(-180.0f, 180.0f));
        }
        //Color[] colors = new Color[5];
        //PaintBounds[] boundsArr = new PaintBounds[1];
        //boundsArr[0].position = Vector2.zero;
        //boundsArr[0].size = Vector2.one * 20;
        //PaintOverlapBounds(boundsArr, _splatters.ToArray(), ref colors);
        //paintCameraCamera.transform.position = new Vector2(Mathf.Sin(Time.time * 0.1f) * 250, Mathf.Cos(Time.time * 0.1f) * 250);
        PaintSplatters(_splatters.ToArray());
    }

    // Convert texture2D to render texture
    RenderTexture Tex2DToRenderTex(Texture2D tex2D)
    {
        RenderTexture result = new RenderTexture(tex2D.width, tex2D.height, 1);
        result.enableRandomWrite = true;
        result.Create();

        Graphics.Blit(tex2D, result);

        return result;
    }

    private Vector2 WorldToPixelCoordinates(Vector2 worldCoords)
    {
        Vector2 cameraWorldSize = paintCameraCamera.ViewportToWorldPoint(Vector2.one) - paintCameraCamera.ViewportToWorldPoint(Vector2.zero);
        Vector2 cameraPixelSize = paintCameraCamera.pixelRect.size;
        Vector2 texturePixelSize = new Vector2(rTexWidth, rTexHeight);

        Vector2 pixelCoords = worldCoords;

        float scaleFactorX = texturePixelSize.x / cameraWorldSize.x;// / cameraPixelSize.x;
        float scaleFactorY = texturePixelSize.y / cameraWorldSize.y;// / cameraPixelSize.y;

        pixelCoords.x = (int)(worldCoords.x * scaleFactorX);
        pixelCoords.y = (int)(worldCoords.y * scaleFactorY);

        return pixelCoords;
    }

    /// <summary>
    /// Description:
    /// Add a randomized paint splatter
    /// </summary>
    public void AddRandomPaintSplatter()
    {
        PaintSplatter newSplat = new PaintSplatter();
        newSplat.position = new Vector2(Random.Range(0 - splatTexture.width, rTexWidth), Random.Range(0 - splatTexture.height, rTexHeight));
        newSplat.color = Color.red;
        _splatters.Push(newSplat);
    }

    /// <summary>
    /// Description:
    /// Add a paint splatter of a certain color, at a position and rotation
    /// </summary>
    /// <param name="splatterColor"></param>
    /// <param name="splatterPosition"></param>
    /// <param name="splatterAngle"></param>
    public void AddPaintSplatter(Color splatterColor, Vector2 splatterPosition, float splatterAngle = 0)
    {
        PaintSplatter newSplat = new PaintSplatter();
        newSplat.position = WorldToPixelCoordinates(splatterPosition) - (new Vector2(splatTexture.width / 2, splatTexture.height / 2));
        newSplat.color = splatterColor;
        newSplat.rotation = splatterAngle;
        _splatters.Push(newSplat);
    }

    /// <summary>
    /// Description:
    /// Draw paint splatters to render texture
    /// </summary>
    /// <param name="splatters"></param>
    public void PaintSplatters(PaintSplatter[] splatters)
    {
        if (rTexture != null && splatTexture != null && splatters.Length > 0)
        {
            int positionSize = sizeof(float) * 2;
            int rotationSize = sizeof(float) * 1;
            int colorSize = sizeof(float) * 4;
            int texIDSize = sizeof(uint);
            int totalSize = positionSize + rotationSize + colorSize + texIDSize;

            //PaintBounds[] boundsArr = new PaintBounds[1];
            //boundsArr[0] = new PaintBounds();
            //boundsArr[0].position = Vector2.zero;
            //boundsArr[0].size = Vector2.one * 20;

            RenderTexture splatRenderTexture = Tex2DToRenderTex(splatTexture);

            ComputeBuffer dataBuffer = new ComputeBuffer(splatters.Length, totalSize);
            dataBuffer.SetData(splatters);
            //ComputeBuffer dummyBoundsBuffer = new ComputeBuffer(boundsArr.Length, sizeof(float) * 4);
            //dummyBoundsBuffer.SetData(boundsArr);

            int kernel = paintShader.FindKernel("CSDrawPaint");

            if (kernel >= 0)
            {
                paintShader.SetBuffer(kernel, "splatters", dataBuffer);
                //paintShader.SetBuffer(kernel, "bounds", dummyBoundsBuffer);
                paintShader.SetTexture(kernel, "splatterTexture", splatRenderTexture);
                paintShader.SetTexture(kernel, "attackTextures[0]", splatRenderTexture);
                paintShader.SetTexture(kernel, "outputTexture", rTexture);
                paintShader.SetFloats("splatterOffset", offset.x, offset.y);
                paintShader.SetInt("splatterCount", _splatters.Length);
                paintShader.Dispatch(kernel, Mathf.CeilToInt(rTexWidth / 32.0f), Mathf.CeilToInt(rTexHeight / 32.0f), 1);
            }

            dataBuffer.Dispose();
            //dummyBoundsBuffer.Dispose();
        }
    }

    /// <summary>
    /// Description:
    /// More user friendly and higher abstraction access to PaintOverlapBounds
    /// </summary>
    /// <param name="position"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    public Color[] GetColorsInBounds(Vector2 position, Vector2 size)
    {
        PaintBounds bounds = new PaintBounds();
        bounds.position = WorldToPixelCoordinates(position - (size / 2));
        bounds.size = WorldToPixelCoordinates(size);
        Color[] result = new Color[10] { Color.clear, Color.clear, Color.clear, Color.clear, Color.clear, Color.clear, Color.clear, Color.clear, Color.clear, Color.clear};
        PaintOverlapBounds(new PaintBounds[1] { bounds }, _splatters.ToArray(), ref result);
        return result;
    }

    /// <summary>
    /// Description:
    /// Find the colors of paint that overlap a set of bounds
    /// </summary>
    /// <param name="bounds"></param>
    /// <param name="splatters"></param>
    /// <param name="colors"></param>
    public void PaintOverlapBounds(PaintBounds[] bounds, PaintSplatter[] splatters, ref Color[] colors)
    {
        if (splatTexture != null && splatters.Length > 0)
        {
            int splatPositionSize = sizeof(float) * 2;
            int splatRotationSize = sizeof(float) * 1;
            int splatColorSize = sizeof(float) * 4;
            int texIDSize = sizeof(uint);
            int splatTotalSize = splatPositionSize + splatRotationSize + splatColorSize + texIDSize;

            int boundsPositionSize = sizeof(float) * 2;
            int boundsSizeSize = sizeof(float) * 2;
            int boundsTotalSize = boundsPositionSize + boundsSizeSize;

            RenderTexture splatRenderTexture = Tex2DToRenderTex(splatTexture);

            ComputeBuffer splatterBuffer = new ComputeBuffer(splatters.Length, splatTotalSize);
            splatterBuffer.SetData(splatters);
            ComputeBuffer boundsBuffer = new ComputeBuffer(bounds.Length, boundsTotalSize);
            boundsBuffer.SetData(bounds);
            ComputeBuffer colorBuffer = new ComputeBuffer(colors.Length, sizeof(float) * 4);
            colorBuffer.SetData(colors);

            int kernel = paintShader.FindKernel("CSCheckPaintBounds");

            if (kernel >= 0)
            {
                paintShader.SetBuffer(kernel, "splatters", splatterBuffer);
                paintShader.SetBuffer(kernel, "bounds", boundsBuffer);
                paintShader.SetBuffer(kernel, "colors", colorBuffer);
                paintShader.SetTexture(kernel, "outputTexture", rTexture);
                paintShader.SetTexture(kernel, "splatterTexture", splatRenderTexture);
                paintShader.SetFloats("splatterOffset", offset.x, offset.y);
                paintShader.Dispatch(kernel, 1, 1,/*Mathf.CeilToInt(bounds[0].size.x / 32.0f), Mathf.CeilToInt(bounds[0].size.y / 32.0f),*/ bounds.Length);
            }

            colorBuffer.GetData(colors);

            splatterBuffer.Dispose();
            boundsBuffer.Dispose();
            colorBuffer.Dispose();
        }
    }
    #endregion
}
