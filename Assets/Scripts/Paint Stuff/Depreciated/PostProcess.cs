/** ------------------------------------------------------------------------------
- Filename:   PostProcess
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/31
- Created by: Jonathan
------------------------------------------------------------------------------- */

using UnityEngine;

/** PostProcess.cs
 * --------------------- Description ------------------------
 * Script which passes camera data through a material
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/31       Created file.                                       Jonathan
 */
 [ExecuteInEditMode]
public class PostProcess : MonoBehaviour
{
    [Tooltip("Material that the camera will pass data through")]
    [SerializeField] public Material material;
    //RenderTexture maskTexture = null;

    /// <summary>
    /// Unity function called by the camera
    /// Runs camera data through a material
    /// </summary>
    /// <param name="source"></param>
    /// <param name="destination"></param>
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material);
    }
}
