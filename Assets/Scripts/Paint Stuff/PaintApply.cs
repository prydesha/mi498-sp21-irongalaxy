/** ------------------------------------------------------------------------------
- Filename:   PaintApply
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/29
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PaintApply.cs
 * --------------------- Description ------------------------
 * Applies paint at current location
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/29       Created file.                                       Jonathan
 */
public class PaintApply : MonoBehaviour
{
    /* ENUMS */
    public enum ApplicationMethods
    {
        MatchTransform, RandomRotation
    }

    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    [Tooltip("The method in which paint is put down")]
    [SerializeField] private ApplicationMethods _applicationMethod = ApplicationMethods.RandomRotation;
    [Tooltip("The color of paint to put down")]
    [SerializeField] private PaintColorAuthoritative.PaintColorEnum _paintColor = PaintColorAuthoritative.PaintColorEnum.Null;
    [Tooltip("The texture to put down")]
    [SerializeField] private Texture2D _paintTexture = null;
    [Tooltip("The scale of the texture to be drawn with")]
    [SerializeField] private float _scale = 1;
    [Tooltip("Whether to place paint when starting up")]
    [SerializeField] private bool _placeOnStart = true;

    /* PROPERTIES */

    // the rotation of the paint to be placed
    protected float paintRotation
    {
        get
        {
            switch (_applicationMethod)
            {
                case ApplicationMethods.MatchTransform:
                    return Vector3.SignedAngle(Vector3.up, transform.up, Vector3.forward);
                case ApplicationMethods.RandomRotation:
                    return Random.Range(-180.0f, 180.0f);
                default:
                    return 0;
            }
        }
    }

    /* METHODS */



    /// <summary>
    /// Description:
    /// On Startup, draw paint if _placeOnStart is true
    /// </summary>
    void Start()
    {
        if (_placeOnStart)
        {
            Draw();
        }
    }

    protected virtual void Draw()
    {
        if (SingleLayerPaintManagerV2.PM != null)
        {
            SingleLayerPaintManagerV2.PM.Draw(transform.position, paintRotation, _scale, _paintTexture, PaintColorAuthoritative.GetColor(_paintColor));
        }
    }
}
