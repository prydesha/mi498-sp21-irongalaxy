/** ------------------------------------------------------------------------------
- Filename:   PlayerPaintColorManager
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/09
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerPaintColorManager.cs
 * --------------------- Description ------------------------
 * Class which manages the player's currently selected paint color
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/09       Created file.                                       Jonathan
 */
public class PaintColorManager : MonoBehaviour
{
    /* DELEGATE MEMBERS */
    public delegate void OnPaintColorChangedDelegate();
    public OnPaintColorChangedDelegate onPaintColorChanged = delegate { };

    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */

    [Tooltip("DO NOT CHANGE THIS! IT'S A WIP")]
    [SerializeField] protected PaintColorAuthoritative.PaintColorEnum _paintColor;

    /* PROPERTIES */

    // Accessor for the player's currently selected paint color
    public virtual Color paintColor
    {
        get
        {
            return PaintColorAuthoritative.GetColor(_paintColor);
        }
        set
        {
            Color closest = PaintColorAuthoritative.GetClosestColor(value);
            _paintColor = PaintColorAuthoritative.GetValue(closest);
            onPaintColorChanged.Invoke();
        }
    }

    public PaintColorAuthoritative.PaintColorEnum PaintColorEnumVal => _paintColor;

    /* METHODS */

    /// <summary>
    /// Description:
    /// Implicit operator that allows this class to be treated as a color
    /// </summary>
    /// <param name="colorManager"></param>
    public static implicit operator Color(PaintColorManager colorManager) {
        if (colorManager == null)
        {
            return Color.white;
        }
        return colorManager.paintColor;
    }
}
