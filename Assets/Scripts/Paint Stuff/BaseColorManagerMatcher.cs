/** ------------------------------------------------------------------------------
- Filename:   BaseColorManagerMatcher
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/23
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** BaseColorManagerMatcher.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/23       Created file.                                       Jonathan
 */
 [ExecuteAlways]
public abstract class BaseColorManagerMatcher : MonoBehaviour
{
    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */
    [Tooltip("The player to base this color matcher on")]
    [SerializeField] protected PaintColorManager _paintColorManager = null;
    // Whether or not this has been set up
    protected bool setup = false; 

    /* PROPERTIES */
    // The color manager
    protected virtual PaintColorManager paintColorManager
    {
        get
        {
            return _paintColorManager;
        }
        set
        {
            _paintColorManager = value;
        }
    }

    // the target color
    protected virtual Color _targetColor
    {
        get
        {
            return paintColorManager;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up this color matcher
    /// </summary>
    protected virtual void OnEnable()
    {
        Setup(true);
    }

    /// <summary>
    /// Avoids nasty delegate errors
    /// </summary>
    protected virtual void OnDisable()
    {
        if (_paintColorManager != null)
        {
            _paintColorManager.onPaintColorChanged -= SetColor;
        }
    }

    /// <summary>
    /// Description:
    /// Sets up the color matcher
    /// </summary>
    /// <param name="force"></param>
    public virtual void Setup(bool force = false)
    {
        if (!setup || force)
        {
            if (_paintColorManager == null)
            {
                _paintColorManager = GetComponentInParent<PaintColorManager>();
            }
            if (_paintColorManager != null)
            {
                SetColor();
                _paintColorManager.onPaintColorChanged += BeginChange;
            }
            setup = true;
        }
    }

    /// <summary>
    /// Description:
    /// Matches player color
    /// </summary>
    public abstract void SetColor();

    /// <summary>
    /// Description:
    /// Function that initiates color change of matcher, either using SetColor or FadeColor
    /// </summary>
    public virtual void BeginChange() { SetColor(); }

    /// <summary>
    /// Description:
    /// Fades paint color to a set value over time
    /// </summary>
    /// <returns></returns>
    public virtual IEnumerator FadeColor()
    {
        yield return null;
    }
}
