/** ------------------------------------------------------------------------------
- Filename:   PaintColorAuthoritative
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/18
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PaintColorAuthoritative.cs
 * --------------------- Description ------------------------
 * Static class that keeps track of paint colors
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/18       Created file.                                       Jonathan
 */
public static class PaintColorAuthoritative
{
    // An enum to list all possible paint colors
    public enum PaintColorEnum
    {
	    Null = -1,
        Red, Yellow, Green, Blue/**, Orange, Purple**/
    }

    // The key with which paint color definitions are stored in player prefs
    private const string colorDefsPrefName = "colorMode";

    // The file path to the definitions file for the paint colors
    private const string colorDefinitionsResourcesPath = "Paint Colors/Paint Color Definitions";

    // The PaintColorDefinitions scriptable object that defines what each color is
    private static PaintColorDefinitions paintColorDefinitionsAuth = null;

    // An accessor to the dictionary of color definitions
    private static Dictionary<PaintColorEnum, Color> _colors
    {
        get
        {
            if (paintColorDefinitionsAuth != null)
            {
                return paintColorDefinitionsAuth;
            }
            else
            {
                if (PlayerPrefs.HasKey(colorDefsPrefName))
                {
                    int target = PlayerPrefs.GetInt(colorDefsPrefName);
                    var allDefinitions = Resources.FindObjectsOfTypeAll<PaintColorDefinitions>();
                    foreach (PaintColorDefinitions def in allDefinitions)
                    {
                        if (def.identifier == target)
                        {
                            paintColorDefinitionsAuth = def;
                            return paintColorDefinitionsAuth;
                        }
                    }
                    PlayerPrefs.DeleteKey(colorDefsPrefName);
                    return _colors;
                }
                else
                {
                    paintColorDefinitionsAuth = Resources.Load(colorDefinitionsResourcesPath) as PaintColorDefinitions;
                    PlayerPrefs.SetInt(colorDefsPrefName, paintColorDefinitionsAuth.identifier);
                    return paintColorDefinitionsAuth;
                }
            }
        }
    }

    // A dictionary enumerating color values for all paint color enum values
    //private static Dictionary<PaintColorEnum, Color> _colors = new Dictionary<PaintColorEnum, Color>()
    //{
    //    //{PaintColorEnum.Red,    new Color(((float)0xb3/255.0f), ((float)0x00/255.0f), ((float)0x00/255.0f), 1.0f) },
    //    {PaintColorEnum.Null,   new Color(((float)0x00/255.0f), ((float)0x00/255.0f), ((float)0x00/255.0f), 0.0f) },
    //    {PaintColorEnum.Red,    new Color(((float)0xff/255.0f), ((float)0x2c/255.0f), ((float)0x4f/255.0f), 1.0f) },
    //    {PaintColorEnum.Yellow, new Color(((float)0xff/255.0f), ((float)0xdf/255.0f), ((float)0x42/255.0f), 1.0f) },
    //    {PaintColorEnum.Green,  new Color(((float)0x28/255.0f), ((float)0xd4/255.0f), ((float)0x31/255.0f), 1.0f) },
    //    {PaintColorEnum.Blue,   new Color(((float)0x20/255.0f), ((float)0x46/255.0f), ((float)0xef/255.0f), 1.0f) },
    //    //{PaintColorEnum.Orange, new Color(((float)0x00/255.0f), ((float)0x00/255.0f), ((float)0x00/255.0f), 1.0f) },
    //    //{PaintColorEnum.Purple, new Color(((float)0x00/255.0f), ((float)0x00/255.0f), ((float)0x00/255.0f), 1.0f) }
    //};

    // A float to help comparing colors
    public const float COMPARISONRANGE = 0.10f;

    /// <summary>
    /// Sets the color definitions reference and player prefs value
    /// </summary>
    /// <param name="defs"></param>
    public static void SetDefs(PaintColorDefinitions defs)
    {
        paintColorDefinitionsAuth = defs;
        PlayerPrefs.SetInt(colorDefsPrefName, paintColorDefinitionsAuth.identifier);
    }

    /// <summary>
    /// Gets a unity Color from an enum value
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Color GetColor(PaintColorEnum value)
    {
        return _colors[value];
    }

    /// <summary>
    /// Description:
    /// Gets an enum value froma  color
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    public static PaintColorEnum GetValue(Color color)
    {
        foreach (PaintColorEnum val in PaintColorEnum.GetValues(typeof(PaintColorEnum)))
        {
            if (_colors[val].IsColor(color, COMPARISONRANGE))
            {
                return val;
            }
        }
        return PaintColorEnum.Red;
    }

    /// <summary>
    /// Description:
    /// Find the closest color in _colors to the input color
    /// </summary>
    /// <param name="inColor"></param>
    /// <returns></returns>
    public static Color GetClosestColor(Color inColor)
    {
        float[] inHSV = new float[3];
        Color.RGBToHSV(inColor, out inHSV[0], out inHSV[1], out inHSV[2]);
        Dictionary<PaintColorEnum, float> hueDiffs = new Dictionary<PaintColorEnum, float>();
        foreach (var kvp in _colors)
        {
            float[] compHSV = new float[3];
            Color.RGBToHSV(kvp.Value, out compHSV[0], out compHSV[1], out compHSV[2]);
            hueDiffs.Add(kvp.Key, Mathf.Min(Mathf.Abs(compHSV[0] - inHSV[0]), Mathf.Abs(0-(1-compHSV[0]) - inHSV[0]), Mathf.Abs(0 - (1 - inHSV[0]) - compHSV[0])));
        }
        var min = float.MaxValue;
        var minCol = Color.white;
        foreach (var kvp in hueDiffs)
        {
            if (kvp.Value  < min)
            {
                min = kvp.Value;
                minCol = _colors[kvp.Key];
            }
        }
        return minCol;
    }

}

/// <summary>
/// A static class containing extension methods allowing the comparison of enum values and unity colors
/// (as well as colors to themselves)
/// </summary>
static class ColorExtend
{
    /// <summary>
    /// Description:
    /// Compares two colors with a hue tolerance range
    /// </summary>
    /// <param name="inColor"></param>
    /// <param name="testColor"></param>
    /// <param name="hueRange"></param>
    /// <returns></returns>
    public static bool IsColor(this Color inColor, Color testColor, float hueRange)
    {
        float[] inHSV = new float[3], tHSV = new float[3];
        Color.RGBToHSV(inColor, out inHSV[0], out inHSV[1], out inHSV[2]);
        Color.RGBToHSV(testColor, out tHSV[0], out tHSV[1], out tHSV[2]);
        if (Mathf.Abs(inHSV[0] - tHSV[0]) < hueRange)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Override of the above function but using the range specified in PaintColorAuthoritative
    /// </summary>
    /// <param name="inColor"></param>
    /// <param name="testColor"></param>
    /// <returns></returns>
    public static bool IsColor(this Color inColor, Color testColor)
    {
        return IsColor(inColor, testColor, PaintColorAuthoritative.COMPARISONRANGE);
    }

    /// <summary>
    /// Description:
    /// Compares a color and an enum value
    /// </summary>
    /// <param name="inColor"></param>
    /// <param name="testVal"></param>
    /// <returns></returns>
    public static bool IsColor(this Color inColor, PaintColorAuthoritative.PaintColorEnum testVal)
    {
        return IsColor(inColor, PaintColorAuthoritative.GetColor(testVal));
    }

    /// <summary>
    /// Description:
    /// Compares an enum value and a color
    /// </summary>
    /// <param name="inColor"></param>
    /// <param name="testVal"></param>
    /// <returns></returns>
    public static bool IsColor(this PaintColorAuthoritative.PaintColorEnum inColor, Color testVal)
    {
        return IsColor(PaintColorAuthoritative.GetColor(inColor), testVal);
    }
}
