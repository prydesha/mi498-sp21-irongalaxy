/** ------------------------------------------------------------------------------
- Filename:   SpriteColorizer
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/04/01
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** SpriteColorizer.cs
 * --------------------- Description ------------------------
 * Colorizes a sprite to match a paint color
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/04/01       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(SpriteRenderer))]
public class SpriteColorizer : BaseColorizer
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    private SpriteRenderer _sr = null;

    /* PROPERTIES */
    private SpriteRenderer _spriteRenderer
    {
        get
        {
            if (_sr == null)
            {
                _sr = GetComponent<SpriteRenderer>();
            }
            return _sr;
        }
    }

    /* METHODS */

    public override void ApplyColor()
    {
        base.ApplyColor();

        _spriteRenderer.color = color;
    }
}
