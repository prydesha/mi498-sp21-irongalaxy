/** ------------------------------------------------------------------------------
- Filename:   MaterialColorizer
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/17
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** MaterialColorizer.cs
 * --------------------- Description ------------------------
 * Colorizes a material
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/17       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(Renderer))]
public class MaterialColorizer : BaseColorizer
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    [Tooltip("The property name to set in the material on this object's renderer")]
    [SerializeField] private string _materialPropertyName = "_MainColor";
    // The renderer to affect
    private Renderer _renderer = null;
    // Intensity of the color
    [SerializeField] private float _intensity = 1;

    /* PROPERTIES */
    // Getter for the renderer on this object
    private Renderer rend
    {
        get
        {
            if (_renderer == null)
            {
                _renderer = GetComponent<Renderer>();
            }
            return _renderer;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description applies a color to a material property
    /// </summary>
    public override void ApplyColor()
    {
        base.ApplyColor();

        Color c = color * _intensity;
        c.a = 1;

        if (Application.isPlaying)
        {
            rend.material.SetColor(_materialPropertyName, c);
        }
        else
        {
            rend.sharedMaterial.SetColor(_materialPropertyName, c);
        }
    }
}
