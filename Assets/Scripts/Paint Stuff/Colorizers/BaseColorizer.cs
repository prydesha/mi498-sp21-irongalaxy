/** ------------------------------------------------------------------------------
- Filename:   BaseColorizer
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/17
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** BaseColorizer.cs
 * --------------------- Description ------------------------
 * Base class for sprites, materials, UI etc that needs to match colors defined in PaintColorAuthoritative
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/17       Created file.                                       Jonathan
 */
 //[ExecuteAlways()]
public abstract class BaseColorizer : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */

    [Tooltip("The color value to be displayed")]
    [SerializeField] private PaintColorAuthoritative.PaintColorEnum _colorVal = PaintColorAuthoritative.PaintColorEnum.Null;
    // The last color applied
    [HideInInspector] private Color lastAppliedColor = Color.white;

    /* PROPERTIES */

    // accessor for the _colorVal variable
    public Color color
    {
        get
        {
            return PaintColorAuthoritative.GetColor(_colorVal);
        }
        protected set
        {
            Debug.Log("Color changed");
            _colorVal = PaintColorAuthoritative.GetValue(PaintColorAuthoritative.GetClosestColor(value));
            ApplyColor();
        }
    }

    /* METHODS */

    /// <summary>
    /// Description: 
    /// Every update, test if the color changed, and apply the new color if it did
    /// </summary>
    private void Update()
    {
        if (lastAppliedColor != color)
        {
            ApplyColor();
        }
    }

    /// <summary>
    /// Description:
    /// Base method for applying this colorizer's color to the object
    /// </summary>
    public virtual void ApplyColor()
    {
        lastAppliedColor = color;
    }
}
