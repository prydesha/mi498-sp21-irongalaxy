/** ------------------------------------------------------------------------------
- Filename:   Pathfinding
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/21
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** Pathfinding.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/21       Created file.                                       smigi
 */
public class Pathfinding
{
    /* PUBLIC MEMBERS */



    /* PRIVATE MEMBERS */
    private const int MOVE_STRAIGHT_COST = 10;
    private const int MOVE_DIAG_COST = 14;

    public Grid<PathNode> grid { get; private set; }
    private List<PathNode> openList;
    private List<PathNode> closedList;
    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    public Pathfinding(int width, int height,Vector3 Position, Vector2 size)
    {
        grid = new Grid<PathNode>(width, height,Position, size ,(Grid<PathNode> g, int x, int y) => new PathNode(g, x, y));
       
    }

    public List<PathNode> FindPath(int startX, int startY,int endX, int endY)
    {
        PathNode startNode = grid.GetGridObject(startX, startY);
        PathNode endNode = grid.GetGridObject(endX, endY);
        openList = new List<PathNode> { startNode};
        closedList = new List<PathNode>();

        for(int x = 0; x < grid.width; x++)
        {
            for(int y =0; y< grid.height; y++)
            {
                PathNode pathNode = grid.GetGridObject(x, y);
                pathNode.gCost = int.MaxValue;
                pathNode.CalculateFCost();
                pathNode.cameFromNode = null;
            }
        }

        startNode.gCost = 0;
        startNode.hCost = CalculateDistanceCost(startNode, endNode);
        startNode.CalculateFCost();

        while(openList.Count > 0)
        {
            PathNode curNode = GetLowestFCostNode(openList);
            if(curNode == endNode)
            { 
                return CalculatePath(endNode);
            }
            openList.Remove(curNode);
            closedList.Add(curNode);

            foreach(PathNode neighborNode in getNeighbors(curNode))
            {
                if(closedList.Contains(neighborNode)) continue;
                if(!neighborNode.isWalkable)
                {
                    closedList.Add(neighborNode);
                    continue;
                }
                int tentativeGCost = curNode.gCost + CalculateDistanceCost(curNode, neighborNode) + neighborNode.movementCost;
                if(tentativeGCost < neighborNode.gCost)
                {
                    neighborNode.cameFromNode = curNode;
                    neighborNode.gCost = tentativeGCost;
                    neighborNode.hCost = CalculateDistanceCost(neighborNode, endNode);
                    neighborNode.CalculateFCost();

                    if(!openList.Contains(neighborNode))
                    {
                        openList.Add(neighborNode);
                    }
                }
            }
        }

        return null;
    }

    private List<PathNode> getNeighbors(PathNode curNode)
    {
        var neighborList = new List<PathNode>();

        if(curNode.x -1 >= 0 )
        {
            neighborList.Add(grid.GetGridObject(curNode.x - 1, curNode.y));
            if(curNode.y -1 >= 0)
                neighborList.Add(grid.GetGridObject(curNode.x - 1, curNode.y-1));
            if (curNode.y + 1 < grid.height)
                neighborList.Add(grid.GetGridObject(curNode.x - 1, curNode.y + 1));
        }
        if (curNode.x + 1 < grid.width)
        {
            neighborList.Add(grid.GetGridObject(curNode.x + 1, curNode.y));
            if (curNode.y - 1 >= 0)
                neighborList.Add(grid.GetGridObject(curNode.x + 1, curNode.y - 1));
            if (curNode.y + 1 < grid.height)
                neighborList.Add(grid.GetGridObject(curNode.x + 1, curNode.y + 1));
        }
        if (curNode.y - 1 >= 0)
            neighborList.Add(grid.GetGridObject(curNode.x, curNode.y - 1));
        if (curNode.y + 1 < grid.height)
            neighborList.Add(grid.GetGridObject(curNode.x, curNode.y + 1));
        return neighborList;
    }

    private List<PathNode> CalculatePath(PathNode endNode)
    {
        var path = new List<PathNode>();
        path.Add(endNode);
        var curNode = endNode;
        while (curNode.cameFromNode != null)
        {
            path.Add(curNode.cameFromNode);
            curNode = curNode.cameFromNode;
        }
        path.Reverse();
        return path;
    }

    private int CalculateDistanceCost(PathNode a , PathNode b)
    {
        int xDist = Mathf.Abs(a.x - b.x);
        int yDist = Mathf.Abs(a.y - b.y);
        int remaining = Mathf.Abs(xDist - yDist);
        return MOVE_DIAG_COST * Mathf.Min(xDist, yDist) + MOVE_STRAIGHT_COST * remaining;
    }
    

    private PathNode GetLowestFCostNode(List<PathNode> pathNodeList)
    {
        PathNode lowestFCostNode = pathNodeList[0];
        for (int i =1; i < pathNodeList.Count;i++)
        {
            if(pathNodeList[i].fCost < lowestFCostNode.fCost)
            {
                lowestFCostNode = pathNodeList[i];
            }
        }
        return lowestFCostNode;
    }
}
