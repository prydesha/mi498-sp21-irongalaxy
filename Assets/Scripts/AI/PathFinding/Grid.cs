/** ------------------------------------------------------------------------------
- Filename:   Grid
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/21
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** Grid.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/21       Created file.                                       smigi
 */
public class Grid<T>
{
    /* PUBLIC MEMBERS */
    public List<List<T>> cells = new List<List<T>>();
    public int width { get; private set; }
    public int height { get; private set; }
    public Vector2 size;
    public Vector3 StartPos;
    public delegate T cellCreation(Grid<T> g, int x, int y);
    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */



    public T GetGridObject(int x, int y)
    {
        return cells[y][x];
    }


    public void GetXY(Vector2 pos, out int x, out int y)
    {
        x = Mathf.Clamp(Mathf.FloorToInt((pos.x - StartPos.x )/ size.x),0,width -1);
        y = Mathf.Clamp(Mathf.FloorToInt((pos.y - StartPos.y) / size.y),0,height-1);

        if (x > width - 1) x = -1;
        if (y > width - 1) y = -1;
    }
    public void GetPosXY(int x, int y, out Vector2 pos)
    {
        pos = new Vector3(x * size.x, y * size.y) + StartPos;


    }

    public Grid(int width, int height, Vector3 StartPosition,Vector2 size, cellCreation callBack )
    {
        this.width = width;
        this.height = height;
        this.size = size;
        this.StartPos = StartPosition;
        for(int y = 0; y < height; y++)
        {
            cells.Add(new List<T>());
            for (int x = 0; x < width; x++)
            {
                cells[y].Add(callBack.Invoke(this,x,y)) ;
            }
        }

    }


}
