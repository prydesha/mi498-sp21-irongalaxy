/** ------------------------------------------------------------------------------
- Filename:   PathNode
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/21
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PathNode.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/21       Created file.                                       smigi
 */
public class PathNode
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    public int x { get; private set; }
    public int y { get; private set; }
    private Grid<PathNode> g;
    public bool isWalkable;
    private int _gCost;
    private int _hCost;
    private int _fCost;
    private int _movementCost = 0;
    public int gCost {
        get 
        {
            return _gCost;
        }
        set
        { 
            _gCost = value; 
        } 
    }
    public int hCost
    {
        get
        {
            return _hCost;
        }
        set
        {
            _hCost = value;
        }
    }
    public int fCost
    {
        get
        {
            return _fCost;
        }
        set
        {
            _fCost = value;
        }
    }

    public int movementCost
    {
        get
        {
            return _movementCost;
        }
        set
        {
            _movementCost = value;
        }
    }

    public PathNode cameFromNode;

    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    public PathNode(Grid<PathNode> g,int x,int y)
    {  
        this.x = x;
        this.y = y;
        this.g = g;
        this.isWalkable = true;
    }

    public override string ToString()
    {
        return x + "," + y;
    }


    public void CalculateFCost()
    {
        _fCost = _gCost + _hCost;
    }

    public Vector2 getRealWorldPos()
    {
        g.GetPosXY(x, y, out Vector2 pos);

        return pos;
    }
}
