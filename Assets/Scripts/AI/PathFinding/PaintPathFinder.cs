/** ------------------------------------------------------------------------------
- Filename:   PaintPathFinder
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/22
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PaintWieghtPair
{
    public PaintColorAuthoritative.PaintColorEnum color;
    public int weight;
}


/** PaintPathFinder.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/22       Created file.                                       smigi
 */
public class PaintPathFinder : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    [HideInInspector]
    public static PaintPathFinder _instance;

    
    /* PRIVATE MEMBERS */
    [Tooltip("The resolution of the pathfinding grid")]
    [SerializeField] private bool ignoreUnwalkableTerrain = false;
    [Tooltip("The resolution of the pathfinding grid")]
    [SerializeField] private Vector2Int GridResolution = new Vector2Int(16, 16);
    [Tooltip("PaintManager reference so we have info about the PaintCanvas")]
    [SerializeField] private SingleLayerPaintManagerV2 PM;
    [Tooltip("Weights for what paints to avoid, normal movement weight without paint is 10 to 14")]
    [SerializeField] private List<PaintWieghtPair> weights;
    public List<PolygonCollider2D> obstacles = new List<PolygonCollider2D>();
    [Tooltip("Camera gicing us the paint RT")]
    private Camera _PathCamera;

    [Tooltip("Most recent path calculated, used for debugging")]
    private List<PathNode> _path;
    [Tooltip("Pathfinding ref")]
    private Pathfinding PF;
    private RenderTexture RT;
    private PolygonCollider2D walkableArea;
    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */
    public List<PathNode> CalculatePath(Vector2 startPos, Vector2 endPos)
    {

        PF.grid.GetXY(startPos, out int x, out int y);
        PF.grid.GetXY(endPos, out int x2, out int y2);
        if (x == -1 || y == -1)
        {
            Debug.Log("PathFinder is off the grid system");
            return null;
        }
        if (x2 == -1 || y2 == -1)
        {
            Debug.Log("EndLocation is off the grid system");
            return null;
        }
        UpdateWeights();
        _path = PF.FindPath(x, y, x2, y2);
       
        return _path;
    }


    private void UpdateWeights()
    {
        Texture2D tex = new Texture2D(RT.width, RT.height, TextureFormat.RGB24, false);
        RenderTexture.active = RT;
        Rect rectReadPicture = new Rect(0, 0, RT.width, RT.height);
        tex.ReadPixels(rectReadPicture, 0, 0);
        var colors = tex.GetPixels();
        RenderTexture.active = null;

        for (int yi = 0; yi < tex.height; yi++)
        {
            for (int xi = 0; xi < tex.width; xi++)
            {

                if(walkableArea != null && !ignoreUnwalkableTerrain)
                {
                   
                    if(!walkableArea.OverlapPoint(PF.grid.GetGridObject(xi, yi).getRealWorldPos()))
                    {
                        PF.grid.GetGridObject(xi, yi).isWalkable = false;
                    }
                }

                foreach (PaintWieghtPair pair in weights)
                {
                    if (PaintColorAuthoritative.GetClosestColor(colors[((tex.width) * yi) + xi]) == PaintColorAuthoritative.GetColor(pair.color))
                    {
                        PF.grid.GetGridObject(xi, yi).movementCost = pair.weight;
                    }
                }

            }
        }
    }

    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {
        if(_instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        _instance = this;
        _PathCamera = GetComponent<Camera>();

        _PathCamera.orthographicSize = PM.transform.localScale.x / 2;
        RT = new RenderTexture(GridResolution.x, GridResolution.y, 1,RenderTextureFormat.ARGB32);
        RT.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
        RT.filterMode = FilterMode.Point;
        _PathCamera.transform.position = new Vector3(PM.transform.position.x, PM.transform.position.y, -1);
        _PathCamera.targetTexture = RT;
        walkableArea = PM.gameObject.GetComponent<PolygonCollider2D>();
        PF = new Pathfinding(GridResolution.x, GridResolution.y, _PathCamera.transform.position - new Vector3(_PathCamera.orthographicSize, _PathCamera.orthographicSize), new Vector2((_PathCamera.orthographicSize *2)/ GridResolution.x, ( _PathCamera.orthographicSize*2) / GridResolution.y));

    }

    // Update is called once per frame
    void Update()
    {
       // CalculatePath(new Vector2(-50,-50),new Vector2(50,50));
        if (_path != null)
        {
            for (int i = 0; i < _path.Count - 1; i++)
            {
                PF.grid.GetPosXY(_path[i].x, _path[i].y, out Vector2 pos1);
                PF.grid.GetPosXY(_path[i+1].x, _path[i+1].y, out Vector2 pos2);
                Debug.DrawLine(pos1, pos2);
              
            }
        }
    }

    public void AddPolyCollider()
    {
        var temp = new PolygonCollider2D();
        temp.isTrigger = true;
        obstacles.Add(temp);
    }

    #endregion
}


/*
[CustomEditor(typeof(PaintPathFinder))]
[ExecuteInEditMode]
public class ColliderList : Editor
{
    PaintPathFinder PF;
    SerializedProperty _colliderList;
    void OnEnable()
    {
        _colliderList = serializedObject.FindProperty("obstacles");
        PF = (PaintPathFinder)target;

    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        // EditorGUILayout.PropertyField(_colliderList);
        base.DrawDefaultInspector();



        if (GUILayout.Button("Add New Obstacle Collider"))
        {
            PF.AddPolyCollider();
        }
        serializedObject.ApplyModifiedProperties();
    }
}

*/
