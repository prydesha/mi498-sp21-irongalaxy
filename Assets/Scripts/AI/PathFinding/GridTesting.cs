/** ------------------------------------------------------------------------------
- Filename:   GridTesting
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/21
- Created by: smigi
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** GridTesting.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/21       Created file.                                       smigi
 */
public class GridTesting : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    private List<PathNode> path;

    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {
        var test = new Pathfinding(10,10,transform.position, new Vector2(1,1));
        test.grid.GetXY(new Vector3(9, 0, 0), out int x, out int y);
        test.grid.GetGridObject(5, 0).movementCost = 200;
        test.grid.GetGridObject(5, 1).movementCost = 200;
        test.grid.GetGridObject(5, 2).movementCost = 200;
        test.grid.GetGridObject(6, 3).movementCost = 200;

        path = test.FindPath(0, 0, x, y);

    }

    // Update is called once per frame
    void Update()
    {
        if (path != null)
        {
            for (int i = 0; i < path.Count - 1; i++)
            {
                Debug.DrawLine(new Vector3(path[i].x, path[i].y), new Vector3(path[i + 1].x, path[i + 1].y));
            }
        }
    }

    #endregion
}
