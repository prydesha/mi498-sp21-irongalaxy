/** ------------------------------------------------------------------------------
- Filename:   BabyCrab
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/30
- Created by: shawn
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

/** BabyCrab.cs
 * --------------------- Description ------------------------
 * tiny crab which is used for the purposes of educating the player
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/30       Created file.                                       shawn
 */
public class BabyCrab : MonoBehaviour
{
    /* PUBLIC MEMBERS */

    [SerializeField] private BabyBehavior _defaultBehavior = BabyBehavior.Stand;
    [SerializeField] private Health _health;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private Animator _animator;
    [SerializeField] private Player _player;
    [SerializeField] private IcePaintEffect _ice;
    [SerializeField] private LightningPaintEffect _lightning;
    [SerializeField] private float _iceSpeedMod = 2f;
    [Min(0.01f)]
    [SerializeField] private float StandardSpeed = 5f;
	[Tooltip("Range of times during the RunAround behavior to switch directions")]
    [SerializeField] private Vector2 _directionChangeRate = new Vector2(1f, 4f);
    
	
    /* PRIVATE MEMBERS */

    private BabyBehavior _currentBehavior;
    private static readonly int Walk = Animator.StringToHash("walk");
    
    private const float UpdateRate = 0.25f;

    private Coroutine _movement = null;

    private float _moveRandomChangeTimer = 0f;
    private Vector2 _moveDir;

    /* PROPERTIES */

    public float Speed
    {
	    get 
	    {
			var value = StandardSpeed;
			if (_ice.inPaint)
			{
				value *= _iceSpeedMod;
			}
			return value;
	    }
}

    /// <summary>
	/// Current state of this baby crab
	/// </summary>
    public BabyBehavior CurrentBehavior
    {
	    get => _currentBehavior;
	    private set
	    {
		    _currentBehavior = value;
		    switch (_currentBehavior)
		    {
			    case BabyBehavior.Stand:
				    _animator.SetBool(Walk, false);
				    break;
			    case BabyBehavior.RunAround:
				    _animator.SetBool(Walk, true);
				    ResetRunAround();
				    break;
			    case BabyBehavior.RunAway:
				    _animator.SetBool(Walk, true);
				    break;
		    }
	    }
    }

    /* METHODS */

    private IEnumerator Movement()
    {
	    while (gameObject.activeSelf)
	    {
		    switch (_currentBehavior)
		    {
			    case BabyBehavior.Stand:
				    _rigidbody.velocity = Vector2.zero;
				    break;
			    case BabyBehavior.RunAway:
				    _rigidbody.velocity = (transform.position - _player.transform.position).normalized * Speed;
				    break;
			    case BabyBehavior.RunAround:
				    _moveRandomChangeTimer -= UpdateRate;
				    if (_moveRandomChangeTimer <= 0f)
				    {
					    ResetRunAround();
				    }
				    _rigidbody.velocity = _moveDir.normalized * Speed;
				    break;
		    }
		    yield return new WaitForSeconds(UpdateRate);
	    }
    }

    private void ResetRunAround()
    {
	    _moveRandomChangeTimer = Random.Range(_directionChangeRate.x, _directionChangeRate.y);
	    _moveDir = new Vector2(
		    Random.Range(-1f, 1f),
		    Random.Range(-1f, 1f)
		    );
	    _moveDir.Normalize();
    }

    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {
	    CurrentBehavior = _defaultBehavior;
	    if (_movement != null)
	    {
		    StopCoroutine(_movement);
	    }
	    _movement = StartCoroutine(Movement());
    }

    #endregion
}


public enum BabyBehavior
{
	Stand = 0,
	RunAround = 1,
	RunAway = 2
}