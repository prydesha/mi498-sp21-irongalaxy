/** ------------------------------------------------------------------------------
- Filename:   BossPaintColorManager
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/23
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** BossPaintColorManager.cs
 * --------------------- Description ------------------------
 * Component that tracks and controls what color the boss appears as
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/23       Created file.                                       Jonathan
 */
public class BossPaintColorManager : PaintColorManager
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */


    /* PROPERTIES */


    /* METHODS */

    /// <summary>
    /// Description: Override of SetColor(enum)
    /// </summary>
    /// <param name="value"></param>
    public void SetColor(PaintColorAuthoritative.PaintColorEnum value)
    {
        paintColor = PaintColorAuthoritative.GetColor(value);
    }

    /// <summary>
    /// Description:
    /// Override of SetColor(string)
    /// </summary>
    /// <param name="value"></param>
    public void SetColor(string value)
    {
        SetColor((PaintColorAuthoritative.PaintColorEnum)System.Enum.Parse(typeof(PaintColorAuthoritative.PaintColorEnum), value, true));
    }
}
