/** ------------------------------------------------------------------------------
- Filename:   SpriteSpawner
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/29
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** SpriteSpawner.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/29       Created file.                                       Jonathan
 */
public class SpriteSpawner : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public int count = 0;

    /* PRIVATE MEMBERS */
    [SerializeField] private GameObject prefab = null;

    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */



    #region Unity Functions

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (InputManager.Paint.isDown)
        {
            Spawn();
        }
    }

    #endregion

    public void Spawn()
    {
        if (prefab != null)
        {
            Vector3 startPosition = new Vector3(Random.Range(-10.0f, 10.0f), Random.Range(-10.0f, 10.0f), 0);
            Instantiate(prefab, startPosition, Quaternion.identity, null);
            count++;
        }
    }
}
