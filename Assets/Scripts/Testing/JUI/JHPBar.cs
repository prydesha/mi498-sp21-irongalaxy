/** ------------------------------------------------------------------------------
- Filename:   JHPBar
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/18
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace JUI
{
    /** JHPBar.cs
     * --------------------- Description ------------------------
     * HP Bar handler
     *
     * ------------------------- Log ----------------------------
     * 
     * Date             Work Description                                    Name
     * -----------      ---------------------------                         --------
     * 2021/02/18       Created file.                                       Jonathan
     */
    public class JHPBar : MonoBehaviour
    {
        /* PUBLIC MEMBERS */
        public Health health = null;
        public Image image = null;
        public bool delayed = false;

        /* PRIVATE MEMBERS */
        private Coroutine delayedReductionCoroutine = null;
        private float displayHealthPercentage = 1;

        /* PROPERTIES */

        private float currentHealth
        {
            get
            {
                return health != null ? health.health : 0;
            }
        }
        private float currentHealthPercentage
        {
            get
            {
                return health != null ? health.healthPercentage : 0;
            }
        }

        /* METHODS */

        private void Start()
        {
            if (health != null)
            {
                health.OnDamage += UpdateDisplay;
                health.OnHealed += UpdateDisplay;
                health.OnDeath += UpdateDisplay;
            }
        }

        private void OnDisable()
        {
            if (health != null)
            {
                health.OnDamage -= UpdateDisplay;
                health.OnHealed -= UpdateDisplay;
                health.OnDeath -= UpdateDisplay;
            }
            if (delayedReductionCoroutine != null)
            {
                StopCoroutine(delayedReductionCoroutine);
            }
        }

        public void UpdateDisplay(float amount)
        {
            if (delayed && delayedReductionCoroutine == null)
            {
                delayedReductionCoroutine = StartCoroutine(DelayedUpdate(amount));
            }
            else
            {
                SetDisplayAmount(currentHealthPercentage);
            }
        }

        private void SetDisplayAmount(float value)
        {
            if (image != null)
            {
                image.fillAmount = value;
                displayHealthPercentage = Mathf.Clamp(value, 0, 1);
            }
        }

        public IEnumerator DelayedUpdate(float amount)
        {
            float startHealth = currentHealth + amount;
            float displayHealth = startHealth;
            SetDisplayAmount(displayHealth / health.maxHealth);
            yield return new WaitForSecondsRealtime(1);
            while (displayHealth > currentHealth)
            {
                displayHealth -= Time.deltaTime * health.maxHealth / 2;
                SetDisplayAmount(displayHealth / health.maxHealth);
                yield return null;
            }
            delayedReductionCoroutine = null;
        }
    }
}
