/** ------------------------------------------------------------------------------
- Filename:   JEffectDisplay
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/18
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** JEffectDisplay.cs
 * --------------------- Description ------------------------
 * Jonathan's script to display paint effects
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/18       Created file.                                       Jonathan
 */
public class JEffectDisplay : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    public OverlapColorDelegator colorFinder = null;
    public GameObject DoTDisplay = null;
    public GameObject HoTDisplay = null;
    public GameObject DefDisplay = null;
    public GameObject SlwDisplay = null;

    /* PRIVATE MEMBERS */

    public Dictionary<GameObject, Coroutine> activationCoroutines = new Dictionary<GameObject, Coroutine>();

    /* PROPERTIES */


    /* METHODS */

    private void Start()
    {
        if (colorFinder != null)
        {
            colorFinder._redEvent.AddListener(delegate { SetDisplayActive(HoTDisplay, 0.3f); });
            colorFinder._greenEvent.AddListener(delegate { SetDisplayActive(DoTDisplay, 0.3f); });
            colorFinder._yellowEvent.AddListener(delegate { SetDisplayActive(DefDisplay, 0.3f); });
            colorFinder._blueEvent.AddListener(delegate { SetDisplayActive(SlwDisplay, 0.3f); });
        }
    }

    public void SetDisplayActive(GameObject obj, float duration)
    {
        if (activationCoroutines.ContainsKey(obj))
        {
            StopCoroutine(activationCoroutines[obj]);
        }
        activationCoroutines[obj] = StartCoroutine(KeepActive(obj, duration));
    }

    public IEnumerator KeepActive(GameObject obj, float duration)
    {
        obj.SetActive(true);
        for (float t = 0; t < duration; t+=Time.deltaTime)
        {
            yield return null;
        }
        obj.SetActive(false);
    }
}
