/** ------------------------------------------------------------------------------
- Filename:   JSelectedPaintDisplay
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/18
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JUI
{
    /** JSelectedPaintDisplay.cs
     * --------------------- Description ------------------------
     * Jonathan's UI handler for paint selection
     *
     * ------------------------- Log ----------------------------
     * 
     * Date             Work Description                                    Name
     * -----------      ---------------------------                         --------
     * 2021/02/18       Created file.                                       Jonathan
     * 2021/03/18       Implemented UI animations                           Blake
     */

    public class JSelectedPaintDisplay : MonoBehaviour
    {
        /* PUBLIC MEMBERS */
        public PlayerPaintColorManager paintManager;
        public float baseScale = 160.0f;
        public float selectedScale = 190.0f;
        public LayoutElement redIcon;
        public LayoutElement blueIcon;
        public LayoutElement yellowIcon;

        /* PRIVATE MEMBERS */


        /* PROPERTIES */


        /* METHODS */


        private void Start()
        {
            if (paintManager != null)
            {
                paintManager.onPaintColorChanged += UpdateDisplay;
            }

            blueIcon.GetComponent<Animator>().CrossFade(blueIcon.name + "_grow", 0f);
            redIcon.GetComponent<Animator>().CrossFade(redIcon.name + "_shrink", 0f);
            yellowIcon.GetComponent<Animator>().CrossFade(yellowIcon.name + "_shrink", 0f);
        }

        private void OnDisable()
        {
            if (paintManager != null)
            {
                paintManager.onPaintColorChanged -= UpdateDisplay;
            }
        }

        private void Reset(LayoutElement element)
        {
            if (element != null)
            {
                element.gameObject.GetComponent<Animator>().CrossFade(element.name + "_shrink", 0f);
            }
        }

        private void Select(LayoutElement element)
        {
            if (element != null)
            {
                element.gameObject.GetComponent<Animator>().CrossFade(element.name + "_grow", 0f);
            }
        }

        public void UpdateDisplay()
        {
            Reset(redIcon);
            Reset(blueIcon);
            Reset(yellowIcon);

            if (((Color)paintManager).IsColor(PaintColorAuthoritative.PaintColorEnum.Red))
            {
                Select(redIcon);
            }
            else if (((Color)paintManager).IsColor(PaintColorAuthoritative.PaintColorEnum.Blue))
            {
                Select(blueIcon);
            }
            else if (((Color)paintManager).IsColor(PaintColorAuthoritative.PaintColorEnum.Yellow))
            {
                Select(yellowIcon);
            }
        }
    }
}
