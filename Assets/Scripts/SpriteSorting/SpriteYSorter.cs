/** ------------------------------------------------------------------------------
- Filename:   SpriteYSorter
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/24
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** SpriteYSorter.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/24       Created file.                                       Jonathan
 */
[RequireComponent(typeof(SpriteRenderer))]
public class SpriteYSorter : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    private float _lastY = 0;
    SpriteRenderer _sr = null;
    private int offset = 0;
    /* PROPERTIES */
    SpriteRenderer sr
    {
        get
        {
            if (_sr != null)
            {
                return _sr;
            }
            _sr = GetComponent<SpriteRenderer>();
            offset = _sr.sortingOrder;
            return _sr;
        }
    }

    /* METHODS */


    #region Unity Functions

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y != _lastY)
        {
            sr.sortingOrder = Mathf.RoundToInt(-transform.position.y) + offset;
            _lastY = transform.position.y;
        }
    }

    #endregion
}
