/** ------------------------------------------------------------------------------
- Filename:   RelativeSpriteYSorter
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/30
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** RelativeSpriteYSorter.cs
 * --------------------- Description ------------------------
 * Sets the sprite's order in layer depending on if it is higher or lower than another transform
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/30       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(SpriteRenderer))]
public class RelativeSpriteYSorter : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */

    [Tooltip("The transform to compare to")]
    [SerializeField] private Transform _comparisonTarget = null;
    [Tooltip("The order in layer applied when this object's transform is at a higher y value than the comparison target")]
    [SerializeField] private int lowerOrder = 0;
    [Tooltip("The order in layer applied when this object's transform is at a lower y value than the comparison target")]
    [SerializeField] private int higherOrder = 0;

    // The sprite renderer associated with this sprite sorter
    private SpriteRenderer _r = null;
    // The original order in layer for this sprite
    private int originalOrder = 0;

    /* PROPERTIES */

    // Ensures initialization of _ren
    private SpriteRenderer _spriteRenderer
    {
        get
        {
            if (_r == null)
            {
                _r = GetComponent<SpriteRenderer>();
            }
            return _r;
        }
    }

    // Accessor for the sprite's order in layer
    private int _currentOrder
    {
        get
        {
            return _spriteRenderer.sortingOrder;
        }
        set
        {
            _spriteRenderer.sortingOrder = value;
        }
    }

    // Whether this sprite is higher than the comparison target
    private bool _higher
    {
        get
        {
            if (_comparisonTarget != null)
            {
                return transform.position.y > _comparisonTarget.position.y;
            }
            else
            {
                return true;
            }
        }
    }

    /* METHODS */

    #region Unity Functions

    /// <summary>
    /// Description:
    /// When starting up, set up the sprite sorter
    /// </summary>
    void Start()
    {
        Setup();
    }

    /// <summary>
    /// Description:
    /// Every update, set the sorting order based on comparison to other transform
    /// </summary>
    void Update()
    {
        _currentOrder = originalOrder + (_higher ? higherOrder : lowerOrder);
    }

    #endregion

    /// <summary>
    /// Description:
    /// Sets up the relative sprite sorter
    /// </summary>
    private void Setup()
    {
        originalOrder = _currentOrder;
    }
}
