/** ------------------------------------------------------------------------------
- Filename:   Attack
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/24
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/** Attack.cs
 * --------------------- Description ------------------------
 * A class holding all data related to an attack
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/24       Created file.                                       Jonathan
 */
 [System.Serializable]
public class Attack
{

    // Helps with rotation
    private const float DegToRad = Mathf.PI / 180;

    /* PUBLIC MEMBERS */
    // The type of attack this is
    public PlayerInputAction.ActionType attackType;
    //The attack collider which deals with collisions and displaying the attack
    public PlayerAttackCollider collider;
    // The damage dealt by the attack
    public float damage;
    // Whether this attack ignores damage immunity
    public bool ignoreInvulnerabilities = false;
    // The scale multiplier of the attack
    public float scaleMultiplier;
    // Whether to randomize the rotation of the paint laid down
    public bool randomizeRotation;
    // The paint textures drawn and the relative position they are drawn at
    public Texture2D[] paintTextures;
    // The locations of the paint drawn
    public Vector2[] paintLocations;
    // Angles from the attack direction at which any projectiles created by the attack will be created
    public float[] projectileAngles;
    // Whether this attack will result in clearing completed actions
    public bool clearCompleted = false;

    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // Whether the attack's direction matters
    public bool directed
    {
        get
        {
            return paintLocations.Where(loc => (Vector2)loc == Vector2.zero).Count() > 0;
        }
    }

    /// <summary>
    /// Description:
    /// Finds the position of the attack collider
    /// </summary>
    public Vector2 attackLocation
    {
        get
        {
            return collider.GetCurrentPosition();
        }
    }

    /// <summary>
    /// Description:
    /// Finds the direction of the attack collider
    /// </summary>
    public Vector2 attackDirection
    {
        get
        {
            return collider.GetCurrentDirection();
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Constructor
    /// </summary>
    /// <param name="coll"></param>
    /// <param name="dam"></param>
    /// <param name="scale"></param>
    public Attack(PlayerAttackCollider coll = null, float dam = 5, float scale = 1)
    {
        attackType = PlayerInputAction.ActionType.lightAttack;
        collider = coll;
        damage = dam;
        ignoreInvulnerabilities = false;
        scaleMultiplier = scale;
        paintLocations = new Vector2[0];
        paintTextures = new Texture2D[0];
        projectileAngles = new float[0];
    }

    /// <summary>
    /// Description:
    /// Sets the scale of the attack collider
    /// </summary>
    public void PositionCollider(Vector2 direction)
    {
        collider.FaceDirection(direction);
        //collider.transform.localScale = collider.baseScale * scaleMultiplier;
    }

    /// <summary>
    /// Description
    /// Deals damage to entities touching the collider
    /// </summary>
    public bool DealDamage()
    {
        bool result = false;
        List<(GameObject obj, Vector2 position)> hits = (from (GameObject, Vector2) t in collider select t).ToList();
        foreach ((GameObject obj, Vector2 pos) pair in hits) // Get all collisions, but don't stay in the enumerator
        {
            var obj = pair.obj;
            Health h = obj.GetComponent<Health>();
            if (h != null)
            {
                h.Damage(damage, ignoreInvulnerabilities, pair.pos, collider.GetCurrentDirection());
                result = true;
                //break;
            }
        }
        return result;
    }

    /// <summary>
    /// Description:
    /// Lays down paint for this attack
    /// </summary>
    /// <param name="color"></param>
    /// <param name="direction"></param>
    public void LayPaint(Color color, Vector2 direction, GameObject effect = null)
    {
        if (paintTextures.Length == paintLocations.Length && PaintManagerBase.PM != null)
        {
            foreach (var pair in paintTextures.Zip(paintLocations, (tex, loc) => (tex, loc)))
            {
                float angle = Vector2.SignedAngle(Vector2.up, direction);
                Vector2 rPos = Rotate(pair.loc, angle);
                Vector2 wPos = (Vector2)collider.transform.parent.position + (Vector2)(collider.transform.parent.localToWorldMatrix * rPos);
                float drawAngle = randomizeRotation ? Random.Range(0.0f, 360.0f) : angle + 180.0f;
                //wPos = (Vector2)collider.transform.localPosition + rPos + (Vector2)collider.transform.parent.position;
                PaintManagerBase.PM.Draw(wPos, drawAngle, scaleMultiplier, pair.tex, color);
                if (effect != null)
                {
                    GameObject spawned = GameObject.Instantiate(effect, wPos, effect.transform.rotation, null);
                    ParticleSystem sys = spawned.GetComponent<ParticleSystem>();
                    if (sys != null)
                    {
                        sys.startColor = Color.Lerp(color, Color.black, 0.25f);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Description:
    /// Coroutine that lasts while the attacks collider is enabled
    /// </summary>
    /// <returns></returns>
    public IEnumerator WaitForCollidersToDissapear()
    {
        while (collider.gameObject.activeInHierarchy)
        {
            yield return null;
        }
    }

    /// <summary>
    /// Description:
    /// Rotates a Vector2D
    /// </summary>
    /// <param name="v"></param>
    /// <param name="degrees"></param>
    /// <returns></returns>
    public static Vector2 Rotate(Vector2 v, float degrees)
    {
        return RotateRadians(v, degrees * DegToRad);
    }

    /// <summary>
    /// Description:
    /// Rotates a vector2D in radians
    /// </summary>
    /// <param name="v"></param>
    /// <param name="radians"></param>
    /// <returns></returns>
    public static Vector2 RotateRadians(Vector2 v, float radians)
    {
        var ca = Mathf.Cos(radians);
        var sa = Mathf.Sin(radians);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }
}
