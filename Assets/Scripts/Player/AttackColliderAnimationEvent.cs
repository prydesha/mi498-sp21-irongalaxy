/** ------------------------------------------------------------------------------
- Filename:   AttackColliderAnimationEvent
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/22
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** AttackColliderAnimationEvent.cs
 * --------------------- Description ------------------------
 * Creates functions to be used as animation events which turn on an attack collider set by other scripts
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/22       Created file.                                       Jonathan
 */
public class AttackColliderAnimationEvent : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    [SerializeField] private PlayerAttackCollider _attackCollider = null;
    private Animator _animator = null;
    public bool attackComplete = false;

    /* PROPERTIES */
    public PlayerAttackCollider attackCollider
    {
        get
        {
            return _attackCollider;
        }
        set
        {
            _attackCollider = value;
            attackComplete = false;
        }
    }

    /* METHODS */

    private void Awake()
    {
        if (_animator == null)
        {
            _animator = GetComponent<Animator>();
        }
    }

    public void EnableCollider()
    {
        if (_attackCollider != null)// && !_attackCollider.gameObject.activeSelf)
        {
            _attackCollider.gameObject.SetActive(true);
            //Debug.Log("enabling collider: " + Time.time + _attackCollider.gameObject.activeSelf);
        }
    }

    public void DisableCollider()
    {
        if (_attackCollider != null)// && _attackCollider.gameObject.activeSelf)
        {
            _attackCollider.gameObject.SetActive(false);
            //Debug.Log("disabling collider: " + Time.time + _attackCollider.gameObject.activeSelf);
        }
    }

    public void SetAttackComplete()
    {
        attackComplete = true;
        if (_animator != null)
        {
            _animator.SetBool("lightAttack", false);
            _animator.SetBool("heavyAttack", false);
            _animator.SetBool("lightAttackAlt", false);
            _animator.SetBool("heavyAttackAlt", false);
        }
    }

    public IEnumerator WaitForAttackComplete()
    {
        float t = 0;
        while (!attackComplete)
        {
            yield return null;
            if (t >= 1.0f)
            {
                break;
            }
            t += Time.deltaTime;
        }
    }
}
