/** ------------------------------------------------------------------------------
- Filename:   PlayerPaintChangeHandler
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/11
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerPaintChangeHandler.cs
 * --------------------- Description ------------------------
 * Handles input that changes the player's paint color
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/11       Created file.                                       Jonathan
 * 2021/03/16		Added specific paint changing buttons				Shawn
 */
public class PlayerPaintChangeHandler : BaseInputHandler
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */


    /* PROPERTIES */


    /* METHODS */

    public override void Setup()
    {
    }

    public override void HandleInput()
    {
        if (InputManager.CyclePaintLeft.justPressed)
        {
            _inputDelegator.playerPaintColor.CycleColorLeft();
        }
        else if (InputManager.CyclePaintRight.justPressed)
        {
            _inputDelegator.playerPaintColor.CycleColorRight();
        }

        if (InputManager.Red.justPressed)
        {
	        ChangeToColor(PaintColorAuthoritative.PaintColorEnum.Red);
        }
        else if (InputManager.Blue.justPressed)
        {
	        ChangeToColor(PaintColorAuthoritative.PaintColorEnum.Blue);
        }
        else if (InputManager.Yellow.justPressed)
        {
	        ChangeToColor(PaintColorAuthoritative.PaintColorEnum.Yellow);
        }
    }

    private void ChangeToColor(PaintColorAuthoritative.PaintColorEnum color)
    {
	    while (_inputDelegator.playerPaintColor.PaintEnumValue != color)
	    {
		    _inputDelegator.playerPaintColor.CycleColorRight();
	    }
    }

    public override Player.PlayerStates GetStateChanges()
    {
        return base.GetStateChanges();
    }
}
