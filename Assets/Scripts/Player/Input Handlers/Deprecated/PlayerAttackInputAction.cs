/** ------------------------------------------------------------------------------
- Filename:   PlayerAttackInputAction
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/26
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerAttackInputAction.cs
 * --------------------- Description ------------------------
 * Modified input action which keeps track of an attack
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/26       Created file.                                       Jonathan
 */
public class PlayerAttackInputAction : PlayerInputAction
{
    /* PUBLIC MEMBERS */

    public Attack attack = null;

    /* PRIVATE MEMBERS */


    /* PROPERTIES */


    /* METHODS */

    public PlayerAttackInputAction(Attack newAttack, ActionType atype, CallbackMethod callback, CancelCallbackMethod cancelCallback, ActionValidCheck check, int newPriority = 0, float expirationTime = 0.5f)
        :base(atype, callback, cancelCallback, check, newPriority, expirationTime)
    {
        attack = newAttack;
    }
}
