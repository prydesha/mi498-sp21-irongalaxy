/** ------------------------------------------------------------------------------
- Filename:   PlayerBasicAttack
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/09
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerBasicAttack.cs
 * --------------------- Description ------------------------
 *
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/09       Created file.                                       Jonathan
 * 2021/02/18		Disabled attack collider when it					Shawn
 *					is not being used
 */
public class PlayerBasicAttack : BaseAttackInputHandler
{
    /* PUBLIC MEMBERS */
    [Header("Specific Attack Variables")]
    [Tooltip("The priority of queued attack actions")]
    public int _attackPriorityStart = 10;
    [Tooltip("Attack action priority while executing the attack (only one frame, but still)")]
    public int _attackPriorityExecute = 10;
    [Tooltip("Attack action priority while in recovery animation")]
    public int _attackPriorityRecovery = 2;
    [Tooltip("Damage of attack")]
    public int attackDamage = 5;
    [Tooltip("Attack base duration")]
    public float attackDuration = 0.5f;
    [Tooltip("Attack windup duration modifier (muliplier applied to Attack Duration)")]
    public float windupDurationModifier = 0.25f;
    [Tooltip("Attack execution duration modifier (muliplier applied to Attack Duration)")]
    public float executionDurationModifier = 0.15f;
    [Tooltip("Attack recovery duration modifer (muliplier applied to Attack Duration)")]
    public float recoveryDurationModifier = 0.6f;
    [Tooltip("Speed multiplier of knockback")]
    [SerializeField][Range(0.0f, 100.0f)] private float _knockbackMultiplier = 50.0f;
    [Tooltip("Duration of knockback when attacking enemies")]
    [SerializeField] private float _knockbackDuration = 0.1f;
    [Header("Attack Colliders")]
    [Tooltip("Top collider for attacking")]
    [SerializeField] private Collider2D _attackCollider = null;
    [Tooltip("Layer mask for attacking")]
    [SerializeField] private LayerMask attackTargetMask = new LayerMask();
    [Header("Paint Information")]
    public SpriteRenderer attackSprite = null;

    /* PRIVATE MEMBERS */

    // Whether or not to begin an attack animation
    private bool attacking = false;

    // Whether an attack is in progress
    private bool inAttack = false;

    // The current attack in progress
    private Coroutine _attackCoroutine = null;

    // The attack action currently queued by this component
    private PlayerInputAction queuedAttackAction = null;

    /* PROPERTIES */

    // The time in an attack that the windup will be finished
    private float windupFinishedTime
    {
        get
        {
            return attackDuration * windupDurationModifier;
        }
    }
    // The time in an attack that the execution will be finished
    private float executionFinishedTime
    {
        get
        {
            return windupFinishedTime + attackDuration * executionDurationModifier;
        }
    }
    // the time in an attack that the recovery will be finished
    private float recoveryFinishedTime
    {
        get
        {
            return executionFinishedTime + attackDuration * recoveryDurationModifier;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up this attack handler
    /// </summary>
    public override void Setup()
    {
	    if (_attackCollider)
	    {
		    _attackCollider.gameObject.SetActive(false);
	    }
    }

    /// <summary>
    /// Description:
    /// Handles attack input
    /// </summary>
    public override void HandleInput()
    {
        if (GetAttackPressed() && CanAttack())
        {
            QueueAction();
        }
    }

    /// <summary>
    /// Determines if this attack can occur
    /// </summary>
    /// <returns></returns>
    protected override bool CanAttack()
    {
        return base.CanAttack() && !inAttack;
    }

    /// <summary>
    /// Queues a dash action
    /// </summary>
    private void QueueAction()
    {
        DequeueAction();
        queuedAttackAction = new PlayerInputAction(PlayerInputAction.ActionType.unspecified, BeginAttack, ExitAttackSteps, CanAttack, _attackPriorityStart);
        _inputDelegator.inputBuffer.Add(queuedAttackAction);
    }

    /// <summary>
    /// Dequeue a dash action
    /// </summary>
    private void DequeueAction()
    {
        if (queuedAttackAction != null)
        {
            _inputDelegator.inputBuffer.Remove(queuedAttackAction);
        }
        queuedAttackAction = null;
    }

    /// <summary>
    /// Description:
    /// Coroutine that handles attacking
    /// </summary>
    /// <param name="inputAction"></param>
    /// <returns></returns>
    public IEnumerator AttackCoroutine(PlayerInputAction inputAction = null)
    {
        EnterAttackSteps();

        float t = 0;

        // Windup
        if (inputAction != null)
        {
            inputAction.priority = _attackPriorityStart;
        }
        while (t < windupFinishedTime)
        {
            // Let animation start up

            t += Time.deltaTime;
            yield return null;
        }

        // Execution
        if (inputAction != null)
        {
            inputAction.priority = _attackPriorityExecute;
        }

        Attack();

        t += Time.deltaTime;
        yield return null;

        // RecoveryFrames
        if (inputAction != null)
        {
            inputAction.priority = _attackPriorityRecovery;
        }
        while (t < recoveryDurationModifier)
        {
            // Let animation play out

            t += Time.deltaTime;
            yield return null;
        }

        ExitAttackSteps();
    }

    /// <summary>
    /// Begins the attack coroutine
    /// </summary>
    /// <param name="inputAction"></param>
    public void BeginAttack(PlayerInputAction inputAction = null)
    {
        if (_attackCoroutine != null)
        {
            StopCoroutine(_attackCoroutine);
            ExitAttackSteps();
        }
        _attackCoroutine = StartCoroutine(AttackCoroutine(inputAction));
    }

    /// <summary>
    /// Description:
    /// Steps to take when entering an attack
    /// </summary>
    public void EnterAttackSteps()
    {
        // Begin animation
        attacking = true;

        inAttack = true;
        inNonCancellableAnimation = true;
        _attackCollider.gameObject.SetActive(true);
        AudioManager.Play("BrushSwing");
    }

    /// <summary>
    /// Description: 
    /// Steps to take upon exiting or cancelling an attack
    /// </summary>
    public void ExitAttackSteps()
    {
        inAttack = false;
        inNonCancellableAnimation = false;
        _attackCoroutine = null;
        _attackCollider.gameObject.SetActive(false);
        DequeueAction();
        _inputDelegator.EndCurrentAction();
        _inputDelegator.CoroutineEndBehaviors();
    }

    /// <summary>
    /// Description:
    /// Handles an attack, damages health overlapping the attack collider, and calls a function to draw paint
    /// </summary>
    public override void Attack()
    {
        // Begin animation
        //attacking = true;

        // Get attack collider
        Collider2D attackCollider = GetAttackCollider();
        if (attackCollider != null)
        {
            // Get colliders overlapping the attack collider
            List<Collider2D> hits = new List<Collider2D>();
            ContactFilter2D filter = new ContactFilter2D();
            filter.layerMask = attackTargetMask;
            filter.useTriggers = true;
            if (Physics2D.OverlapCollider(attackCollider, filter, hits) >= 0)
            {
                bool hitFound = false;
                foreach (var hit in hits)
                {
                    Health health = hit.GetComponent<Health>();
                    if (health != null)
                    {
                        hitFound = true;
                        health.Damage(attackDamage);
                    }
                }

                if (hitFound)
                {
                    StartCoroutine(ApplyKnockback(-GetAttackDirection(), _knockbackDuration));
                }
            }

            DrawAttackPaint((Vector2)transform.position + GetAttackDirection() * _attackDistance, GetAttackDirection());
        }
        else
        {
            DrawAttackPaint((Vector2)transform.position + GetAttackDirection() * _attackDistance, GetAttackDirection());
        }
        StartCoroutine(ShowAttackSprite());

        base.Attack();
    }

    /// <summary>
    /// Description:
    /// Draws paint for the attack
    /// </summary>
    /// <param name="position"></param>
    /// <param name="direction"></param>
    /// <param name="attackColor"></param>
    public void DrawAttackPaint(Vector2 position, Vector2 direction)
    {
        float rotation = Vector2.SignedAngle(direction, Vector2.up);
        if (PaintManager.PM != null && _attackTexture != null)
        {
            PaintManager.PM.Draw(position, _attackTexture, (Color)_inputDelegator.playerPaintColor, (int)rotation);
        }
        if (PaintCamera.Instance != null)
        {
            PaintCamera.Instance.AddPaintSplatter(_inputDelegator.playerPaintColor, position, rotation);
        }
        if (PaintManagerBase.PM != null && _attackTexture != null)
        {
            PaintManagerBase.PM.Draw(position, (int)Random.Range(0, 360), Random.Range(4.9f, 4.25f), _attackTexture, (Color)_inputDelegator.playerPaintColor);
        }
    }

    /// <summary>
    /// Description:
    /// Applys a knockback effect to player upon hitting a thing
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="duration"></param>
    /// <returns></returns>
    public IEnumerator ApplyKnockback(Vector2 direction, float duration = 0.10f)
    {
        float t = 0;
        while (t < duration)
        {
            _inputDelegator.playerRigidbody.velocity += direction * _knockbackMultiplier * Time.deltaTime;
            t += Time.deltaTime;
            yield return null;
        }
    }

    /// <summary>
    /// Description:
    /// Shows the attack swipe sprite
    /// </summary>
    /// <param name="duration"></param>
    /// <returns></returns>
    public IEnumerator ShowAttackSprite(float duration = 0.1f)
    {
        if (attackSprite != null)
        {
            attackSprite.color = _inputDelegator.playerPaintColor;
            float t = 0;
            while (t < duration)
            {
                attackSprite.color = Color.Lerp(Color.Lerp(_inputDelegator.playerPaintColor, Color.white, 0.5f), new Color(1, 1, 1, 0), t / duration);
                t += Time.deltaTime;
                yield return null;
            }
            attackSprite.color = Color.clear;
        }
    }

    /// <summary>
    /// Description: 
    /// Gets the collider to use for the attack
    /// </summary>
    /// <returns></returns>
    public Collider2D GetAttackCollider()
    {
        Vector2 attackDir = GetAttackDirection();
        Vector2 currentDirToColl = (transform.position - _attackCollider.transform.position).normalized;
        _attackCollider.transform.RotateAround(transform.position, Vector3.forward, Vector2.SignedAngle(currentDirToColl, -attackDir));
        return _attackCollider;
    }

    /// <summary>
    /// Description:
    /// Gets the state changes the player should undergo
    /// if the player is starting an attack, this will cause the Player class to set an animator triggers
    /// </summary>
    /// <returns></returns>
    public override Player.PlayerStates GetStateChanges()
    {
        Player.PlayerStates attackingState = 0;
        if (attacking)
        {
            attackingState = Player.PlayerStates.lightAttacking;
            attacking = false;
        }
        return base.GetStateChanges() | attackingState;
    }
}
