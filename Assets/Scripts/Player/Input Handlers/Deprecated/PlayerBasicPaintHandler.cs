/** ------------------------------------------------------------------------------
- Filename:   PlayerBasicPaintHandler
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/28
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerBasicPaintHandler.cs
 * --------------------- Description ------------------------
 * Input handler that handles a basic paint attack
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/28       Created file.                                       Jonathan
 */
public class PlayerBasicPaintHandler : BaseAttackInputHandler
{
    /* PUBLIC MEMBERS */
    [Tooltip("The arc length of the attack in degrees")]
    public float arcDegreeLength = 60;
    [Tooltip("The distance between paint splatters within the arc")]
    public float arcDegreeStep = 15;

    /* PRIVATE MEMBERS */
    [SerializeField] private Texture2D attackTexture = null;

    private bool inSwipe = false;
    private Coroutine swipeCoroutine = null;
    private bool initialized = false;

    /* PROPERTIES */


    /* METHODS */

    public override void Attack()
    {
        if (!initialized)
        {
            Setup();
        }
        Vector2 relativeAttackPosition = inputDelegator.playerFacingDirection * _attackDistance;
        Vector2 worldAttackPosition = (Vector2)transform.position + relativeAttackPosition;
        if (PaintManager.PM != null)
        {
            PaintManager.PM.Draw(worldAttackPosition, _attackTexture, Color.red, (int)Random.Range(0, 360));
        }
        if (swipeCoroutine != null)
        {
            StopCoroutine(swipeCoroutine);
        }
        swipeCoroutine = StartCoroutine(PaintSwipe());
    }

    public override void HandleInput()
    {
        if (InputManager.Paint.justPressed)
        {
            Attack();
        }
    }

    private IEnumerator PaintSwipe()
    {
        inSwipe = true;
        Vector2 relativeAttackPosition = inputDelegator.playerFacingDirection * _attackDistance;

        if (PaintCamera.Instance != null)
        {
            float halfArc = arcDegreeLength / 2.0f;
            for (float angle = -halfArc; angle < halfArc; angle += 15)
            {
                Quaternion rotator = Quaternion.Euler(0, 0, angle);
                PaintCamera.Instance.AddPaintSplatter(Color.cyan, ((Vector2)transform.position) + (Vector2)(rotator * relativeAttackPosition), Random.Range(0.0f, 360.0f));
                yield return new WaitForSeconds(0.05f);
            }
        }
        inSwipe = false;
        swipeCoroutine = null;
    }

    public override void Setup()
    {
        if (attackTexture != null)
        {
            // Send attack texture to paint camera
        }
        initialized = true;
    }

    public override Player.PlayerStates GetStateChanges()
    {
        Player.PlayerStates state = base.GetStateChanges();
        if (inSwipe)
        {
            state = state | Player.PlayerStates.lightAttacking;
        }
        return state;
    }
}
