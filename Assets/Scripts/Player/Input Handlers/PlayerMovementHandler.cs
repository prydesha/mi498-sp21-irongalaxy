/** ------------------------------------------------------------------------------
- Filename:   PlayerMovementHandler
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/26
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerMovementHandler.cs
 * --------------------- Description ------------------------
 * Input Handler derived class which moves the player
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/26       Created file.                                       Jonathan
 */
public class PlayerMovementHandler : BaseInputHandler
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */

    [Header("Movement Variables")]
    [Tooltip("Movement Speed")]
    [SerializeField] private float _movementSpeed = 10.0f;
    [SerializeField] private float _acceleration = 2.5f;
    [Header("Ice Variables")]
    [SerializeField] private IcePaintEffect _iceEffectTracker = null;
    [SerializeField] private float _iceAccelerationModifier = 0.5f;
    [SerializeField] private float _iceMaxSpeedModifier = 2.0f;

    private int _speedReductionCoroutines = 0;
    private float _movementSpeedModifier = 1.0f;

    /* PROPERTIES */
    private float movementSpeed
    {
        get
        {
            return _movementSpeed * _movementSpeedModifier * _impairmentSpeedModifier;
        }
    }

    private float acceleration
    {
        get
        {
            return _acceleration * _impairmentAccelerationModifier;
        }
    }

    private float _impairmentAccelerationModifier
    {
        get
        {
            return _iceEffectTracker == null || !_iceEffectTracker.movementImpaired ? 1 : _iceAccelerationModifier;
        }
    }

    private float _impairmentSpeedModifier
    {
        get
        {
            return _iceEffectTracker == null || !_iceEffectTracker.movementImpaired ? 1 : _iceMaxSpeedModifier;
        }
    }

    /* METHODS */


    /// <summary>
    /// Description:
    /// Sets up this movement input handler
    /// Inputs: N/A
    /// Outputs: N/A
    /// </summary>
    public override void Setup()
    {
    }

    /// <summary>
    /// Description:
    /// Reads movement input and moves the player accordingly
    /// Inputs: N/A
    /// Outputs: N/A
    /// </summary>
    public override void HandleInput()
    {
        _inputDelegator.playerRigidbody.velocity = GetActualMovement(GetDesiredMovementDirection(), _inputDelegator.playerRigidbody.velocity);
        var lookInput = inputDelegator.lookInputDirection;
        _inputDelegator.playerFacingDirection = lookInput.magnitude != 0? lookInput.normalized: _inputDelegator.playerRigidbody.velocity.normalized;//(Camera.main.ScreenToWorldPoint(InputManager.MousePosition) - transform.position).normalized;
        playerStateTransitions = GetStateChanges();
    }

    private Vector2 GetDesiredMovementDirection()
    {
        return InputManager.Move;
    }

    public Vector2 GetActualMovement(Vector2 desiredDirection, Vector2 currentVelocity)
    {
        Vector2 desiredVelocity = desiredDirection.normalized * acceleration;
        Vector2 actualVelocity = currentVelocity + desiredVelocity;
        if (desiredDirection == Vector2.zero && (_iceEffectTracker == null || !_iceEffectTracker.movementImpaired))
        {
            actualVelocity = actualVelocity * 0.75f;
        }
        actualVelocity = actualVelocity.normalized * Mathf.Clamp(actualVelocity.magnitude, 0, movementSpeed);
        return actualVelocity;
    }

    public void ReduceSpeed()
    {
        float reductionPercentage = 0.5f;
        float time = 0.15f;
        StartCoroutine(ReduceSpeedCoroutine(reductionPercentage, time));
    }

    public IEnumerator ReduceSpeedCoroutine(float reduction = 0.5f, float duration = 0.1f)
    {
        _movementSpeedModifier = reduction;
        _speedReductionCoroutines++;
        for (float t = 0; t < duration; t+= Time.deltaTime)
        {
            yield return null;
        }
        _speedReductionCoroutines--;
        if (_speedReductionCoroutines <= 0)
        {
            _movementSpeedModifier = 1.0f;
        }
    }

    public override Player.PlayerStates GetStateChanges()
    {
        return base.GetStateChanges();
    }
}
