/** ------------------------------------------------------------------------------
- Filename:   BaseInputHandler
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/26
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** BaseInputHandler.cs
 * --------------------- Description ------------------------
 * Base class for components that interpret input and translate that input into player actions
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/26       Created file.                                       Jonathan
 */
public abstract class BaseInputHandler : MonoBehaviour
{
    /* PUBLIC MEMBERS */

    // Whether this input handler is currently performing a task which should not be interrupted
    [HideInInspector] public bool inNonCancellableAnimation = false;

    /* PRIVATE MEMBERS */

    // The delegator component that manages this script
    [HideInInspector] protected PlayerInputDelegator _inputDelegator = null;
    // The state transitions that this input handler should impose on the player.
    protected Player.PlayerStates playerStateTransitions;

    /* PROPERTIES */

    /// <summary>
    /// Getter for the current states of the player
    /// </summary>
    public Player.PlayerStates CurrentPlayerStates
    {
        get
        {
            return _inputDelegator.playerState;
        }
    }

    public PlayerInputDelegator inputDelegator
    {
        protected get
        {
            return _inputDelegator;
        }
        set
        {
            _inputDelegator = value;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up this input handler
    /// </summary>
    public abstract void Setup();

    /// <summary>
    /// Description:
    /// Base function which will read inputs and call functions to act on them
    /// Inputs: N/A
    /// Outputs: N/A
    /// </summary>
    public abstract void HandleInput();

    /// <summary>
    /// Description:
    /// Function to determine and return what state changes are necessary in the player core
    /// </summary>
    /// <returns></returns>
    public virtual Player.PlayerStates GetStateChanges()
    {
        return (Player.PlayerStates)0;
    }
}
