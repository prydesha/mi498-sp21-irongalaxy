/** ------------------------------------------------------------------------------
- Filename:   BaseAttackInputHandler
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/28
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

/** BaseAttackInputHandler.cs
 * --------------------- Description ------------------------
 * Base class for attack input handlers
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/28       Created file.                                       Jonathan
 */
public abstract class BaseAttackInputHandler : BaseInputHandler
{
    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */

    [Header("Base Attack Variables")]
    [Tooltip("The input action that causes this attack to be performed")]
    [SerializeField] AttackInputs.AttackButtons attackButton = AttackInputs.AttackButtons.leftMouse;
    [Tooltip("The texture of the attack to use")]
    [SerializeField] protected Texture2D _attackTexture = null;
    [Tooltip("The distance at which to draw the attack")]
    [SerializeField] protected float _attackDistance = 0;
    [Tooltip("The cooldown between attacks")]
    [SerializeField] protected float _cooldown = 0.2f;
    [Tooltip("Whether the player is limited to being able to attack in 8 directions")]
    [SerializeField] protected bool eightDirectionConstraint = false;

    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */
    protected virtual bool GetAttackPressed()
    {
        switch(attackButton)
        {
            case AttackInputs.AttackButtons.leftMouse:
                return InputManager.Paint.justPressed;
            case AttackInputs.AttackButtons.rightMouse:
                return InputManager.Jab.justPressed;
            default:
                return false;
        }
    }

    protected virtual bool CanAttack()
    {
        return Time.time >= _inputDelegator.attackCooldownTime;
    }

    public virtual void Attack()
    {
        _inputDelegator.attackCooldownTime = Time.time + _cooldown;
    }

    public virtual Vector2 GetAttackDirection()
    {
        //Vector2 worldMousePos = Camera.main.ScreenToWorldPoint(InputManager.MousePosition);
        Vector2 attackDir = inputDelegator.lookInputDirection;// (worldMousePos - inputDelegator.playerRigidbody.position).normalized;
        if (!eightDirectionConstraint)
        {
            return attackDir.normalized;
        }
        foreach (Vector2 vec in new Vector2[]{
            Vector2.up,
            Vector2.right,
            Vector2.down,
            Vector2.left,
            new Vector2(1,1),
            new Vector2(1,-1),
            new Vector2(-1,1),
            new Vector2(-1,-1)
        })
        {
            if (Mathf.Abs(Vector2.SignedAngle(attackDir, vec)) <= 22.5f)
            {
                attackDir = vec;
                break;
            }
        }
        return attackDir.normalized;
    }
}

/// <summary>
/// Static class which contains input information for attacks
/// </summary>
public static class AttackInputs
{
    /// <summary>
    /// Enum of attack input types
    /// </summary>
    public enum AttackButtons
    {
        leftMouse = 0,
        leftMouseHeld = 2,
        rightMouse = 1,
        rightMouseHeld = 3,
        none = -1
    }

    /// <summary>
    /// Description: Static function that reads attack input
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static bool GetInput(AttackButtons input, bool resetHeld = false)
    {
        bool result = false;
        switch (input)
        {
            case AttackButtons.leftMouse:
                result = InputManager.Paint.justPressed;
                break;
            case AttackButtons.rightMouse:
                result = InputManager.Jab.justPressed;
                break;
            case AttackButtons.leftMouseHeld:
                result = InputManager.Paint.heldLastFrame && InputManager.Paint.justReleased;
                break;
            case AttackButtons.rightMouseHeld:
                result = InputManager.Jab.heldLastFrame && InputManager.Jab.justReleased;
                break;
            default:
                break;
        }
        return result;
    }

    /// <summary>
    /// Description:
    /// Determines if any attack inputs are supplied
    /// </summary>
    /// <returns></returns>
    public static bool GetInput()
    {
        return (from AttackButtons enumVal in Enum.GetValues(typeof(AttackButtons)) where GetInput(enumVal, true) select true).Any();
    }
}
