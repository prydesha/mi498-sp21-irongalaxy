/** ------------------------------------------------------------------------------
- Filename:   PlayerDashHandler
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/26
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerDashHandler.cs
 * --------------------- Description ------------------------
 * Input Handler derived class which allows the player to dash
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/26       Created file.                                       Jonathan
 */
public class PlayerDashHandler : BaseInputHandler
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    [Tooltip("The duration of the dash in seconds")]
    [SerializeField] private float _dashDuration = 0.15f;
    [Tooltip("The distance covered by a dash in unity units")]
    [SerializeField] private float _dashMaxSpeed = 20.0f;
    [Tooltip("Curve of position compared to the percentage through the duration of the dash")]
    [SerializeField] private AnimationCurve _speedVsTimeCurve = new AnimationCurve();
    [Tooltip("Cooldown between dashes")]
    [SerializeField] private float _dashCooldown = 0.3f;

    [Tooltip("The last dash taken")]
    [SerializeField] private float _lastDashTime;
    [Tooltip("The priority of the dash action in the action queue")]
    [SerializeField] private int _dashPriority = 10;
    [Tooltip("The percentage of the dash during which the player is immune to damage")]
    [SerializeField] [Range(0.0f, 1.0f)] private float _dashDamageImmunityPercentage = 0;
    [Tooltip("The percentage of the dash during which the player is unaffected by paint")]
    [SerializeField] [Range(0.0f, 1.0f)] private float _dashPaintImmunityPercentage = 0;
    // Whether this input handler is currently performing a dash
    private bool _inDash = false;
    // The current dash coroutine
    private Coroutine _currentDashCoroutine = null;
    // The queued dash action
    private PlayerInputAction queuedDashAction = null;

    /* PROPERTIES */

    // Accessor 
    private Health playerHealth => _inputDelegator.playerHealth;
    private OverlapColorDelegator playerColorDelegator => _inputDelegator.playerOverlapColorDelegator;

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up this dash input handler
    /// </summary>
    public override void Setup()
    {
        // No work necessary here for now
        _currentDashCoroutine = null;
    }

    /// <summary>
    /// Description:
    /// Collect and act on input
    /// </summary>
    public override void HandleInput()
    {
        bool dashPressed = InputManager.Dodge.justPressed;
        if (dashPressed && CanDash())
        {
            QueueAction();
        }
    }

    /// <summary>
    /// Queues a dash action
    /// </summary>
    private void QueueAction()
    {
        DequeueAction();
        queuedDashAction = new PlayerInputAction(PlayerInputAction.ActionType.dash, BeginDash, ExitDashSteps, CanDash, _dashPriority);
        _inputDelegator.inputBuffer.Add(queuedDashAction);
    }

    /// <summary>
    /// Dequeue a dash action
    /// </summary>
    private void DequeueAction()
    {
        if (queuedDashAction != null)
        {
            _inputDelegator.inputBuffer.Remove(queuedDashAction);
        }
        queuedDashAction = null;
    }

    /// <summary>
    /// Description:
    /// Determine the correct current state of the player
    /// </summary>
    /// <returns></returns>
    public override Player.PlayerStates GetStateChanges()
    {
        Player.PlayerStates state = 0;
        if (_inDash)
        {
            state = Player.PlayerStates.dashing;
        }
        return base.GetStateChanges() | state;
    }

    /// <summary>
    /// Description:
    /// Returns whether a dash is valid at this time
    /// </summary>
    /// <returns></returns>
    protected virtual bool CanDash()
    {
        bool cooledDown = Time.time - _dashCooldown > _lastDashTime;
        bool outOfAnimation = true;// !_inputDelegator.inNonCancellableAnimation;
        bool outOfDash = !_inDash;
        bool moving = InputManager.Move.magnitude > 0;
        return cooledDown && outOfAnimation && outOfDash && moving;
    }

    /// <summary>
    /// Description:
    /// Begins a dash coroutine
    /// </summary>
    private void BeginDash(PlayerInputAction inputAction = null)
    {
        if (_currentDashCoroutine != null)
        {
            StopCoroutine(_currentDashCoroutine);
            ExitDashSteps();
        }
        _currentDashCoroutine = StartCoroutine(PerformDash());
    }

    /// <summary>
    /// Description:
    /// Handles behavior during a dash
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformDash( PlayerInputAction inputAction = null)
    {
        // Enter a dash
        EnterDashSteps();

        // Loop that controls behavior during first half of dash
        float t = 0;
        Vector2 dashDirection = InputManager.Move;
        if (dashDirection == Vector2.zero)
        {
            dashDirection = _inputDelegator.transform.forward;
        }
        while (t < _dashDuration && _inDash)
        {
            if (t/_dashDuration > _dashDamageImmunityPercentage)
            {
                playerHealth.DamageImmune = false;
            }
            if (t / _dashDuration > _dashPaintImmunityPercentage)
            {
                playerColorDelegator.ignorePaint = false;
            }

            // Loop maintenance:
            yield return null;
            t += Time.deltaTime;
            float curveTime = _speedVsTimeCurve.Evaluate(t / _dashDuration);

            // Apply velocity to the player according to the velocity curve
            _inputDelegator.playerRigidbody.velocity = Vector2.Lerp(_inputDelegator.playerRigidbody.velocity, dashDirection.normalized * _dashMaxSpeed, curveTime);
        }

        // Allow other input to take effect
        inNonCancellableAnimation = false;

        ExitDashSteps();
    }

    #region Local Functions
    // The steps required to enter a dash (in function form for re-usability and modularity
    void EnterDashSteps()
    {

        inNonCancellableAnimation = true;
        _inDash = true;

        //Vector3 initialDashDirection = _inputDelegator.GetCameraBasedMovementInput();

        //// Face the player in the direction of their input
        //RotatePlayer(initialDashDirection);

        // Cancel initial velocity
        _inputDelegator.playerRigidbody.velocity = Vector3.zero;

        // Set the dash cooldown
        _lastDashTime = Time.time;

        // Set damage ignore
        playerHealth.DamageImmune = true;

        // Set paint ignore
        playerColorDelegator.ignorePaint = true;

        AudioManager.Play("PlayerDash");
    }

    // The steps required to exit a dash (in function form for re-usability and modularity
    void ExitDashSteps()
    {
        // Allow other input to take effect
        inNonCancellableAnimation = false;

        // Dequeues the input action if possible
        DequeueAction();
        _inputDelegator.EndCurrentAction();

        // Notify the delegator that we have exited a coroutine
        _inputDelegator.CoroutineEndBehaviors();

        // Set damage ignore
        playerHealth.DamageImmune = false;

        // Set paint ignore
        playerColorDelegator.ignorePaint = false;

        _inDash = false;
        _currentDashCoroutine = null;
    }
    #endregion
}
