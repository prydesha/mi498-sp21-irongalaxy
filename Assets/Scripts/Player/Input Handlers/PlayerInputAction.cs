/** ------------------------------------------------------------------------------
- Filename:   PlayerInputAction
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/11
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerInputAction.cs
 * --------------------- Description ------------------------
 * Class representing an action that can be queued for the player to take
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/11       Created file.                                       Jonathan
 */
public class PlayerInputAction
{
    /* ENUMS */
    public enum ActionType
    {
        any = -1,
        unspecified = 0,
        dash = 1,
        lightAttack = 2,
        heavyAttack = 3,
        lightAltAttack = 4,
        heavyAltAttack = 5
    }

    /* DELEGATES */
    // Delegate for action callback (passes a reference to self to dynamically modify priority)
    public delegate void CallbackMethod(PlayerInputAction action = null);
    // Delegate for cancel callback
    public delegate void CancelCallbackMethod();
    // Delegate for validation callback
    public delegate bool ActionValidCheck();

    /* PUBLIC MEMBERS */
    // The name of this action (for debugging purposes
    public ActionType type = ActionType.unspecified;
    // The priority of this action
    public int priority = 0;

    /* PRIVATE MEMBERS */
    // the timestamp of this action
    private float _timestamp;
    // The expiration time of this action (will always be 0.5s for now)
    private float _expirationTime = 0.75f;
    // The callback to call when this action is performed
    private CallbackMethod _callback = delegate { };
    // The callback to call to cancel this action
    private CancelCallbackMethod _cancelCallback = delegate { };
    // The callback to call to check if this action can be performed
    private ActionValidCheck _check = delegate { return true; };

    /* PROPERTIES */
    // Accessor for the timestamp of this action
    public float timestamp
    {
        get
        {
            return _timestamp;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Constructor for an input action.
    /// Takes 3 callbacks and a priority value
    /// Callback 1 is a method which is called to perform this action
    /// Callback 2 is a method to be called in order to cancel this action
    /// Callback 3 is a method to be called to ensure this action can be taken
    /// </summary>
    /// <param name="callback"></param>
    /// <param name="cancelCallback"></param>
    /// <param name="check"></param>
    /// <param name="newPriority"></param>
    public PlayerInputAction(ActionType atype, CallbackMethod callback, CancelCallbackMethod cancelCallback, ActionValidCheck check, int newPriority = 0, float expirationTime = 0.5f)
    {
        type = atype;
        _timestamp = Time.timeSinceLevelLoad;
        _expirationTime = expirationTime;
        _callback = callback;
        _cancelCallback = cancelCallback;
        _check = check;
        priority = newPriority;
    }

    /// <summary>
    /// Description:
    /// Returns whether this action can be performed
    /// </summary>
    /// <returns></returns>
    public bool CanPerform()
    {
        return _check.Invoke() && Time.timeSinceLevelLoad < _timestamp + _expirationTime;
    }

    /// <summary>
    /// Description:
    /// Invokes the action callback if the action can be taken
    /// </summary>
    /// <returns></returns>
    public void PerformAction()
    {
        if (CanPerform())
        {
            _callback.Invoke(this);
            //Debug.Log("Performing: " + ToString());
        }
    }

    /// <summary>
    /// Description:
    /// Calls the cancel action callback
    /// </summary>
    public void Cancel()
    {
        _cancelCallback.Invoke();
    }

    /// <summary>
    /// Description:
    /// outputs a string with relevant action data
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return type.ToString() + "_" + timestamp.ToString();
    }
}
