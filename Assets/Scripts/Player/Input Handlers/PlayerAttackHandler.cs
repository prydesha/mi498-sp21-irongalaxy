/** ------------------------------------------------------------------------------
- Filename:   PlayerAttackHandler
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/25
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/** PlayerAttackHandler.cs
 * --------------------- Description ------------------------
 * Class which handles attack inputs and allows combos
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/25       Created file.                                       Jonathan
 */
public class PlayerAttackHandler : BaseAttackInputHandler
{
    #region Variables and Properties
    /* PUBLIC/EXPOSED MEMBERS */
    #region Inspector exposed variables:
    [Header("Specific Attack Variables")]
    [Tooltip("The priority of queued attack actions")]
    public int _attackPriorityStart = 10;
    [Tooltip("Attack action priority while executing the attack (only one frame, but still)")]
    public int _attackPriorityExecute = 10;
    [Tooltip("Attack action priority while in recovery animation")]
    public int _attackPriorityRecovery = 2;
    [Tooltip("Speed multiplier of knockback")]
    [SerializeField] [Range(0.0f, 100.0f)] private float _knockbackMultiplier = 50.0f;
    [Tooltip("Duration of knockback when attacking enemies")]
    [SerializeField] private float _knockbackDuration = 0.1f;
    [Header("Input")]
    [Tooltip("The amount of time an input must be held for before it registers as 'held'")]
    [SerializeField] private float heldInputConfirmTime = 0.75f;
    [Header("Attacks")]
    [Tooltip("The component which tells the animator which attack colliders to enable/disable")]
    [SerializeField] private AttackColliderAnimationEvent animationInterface = null;
    [Tooltip("Combos that the player can perform")]
    [SerializeField] private ComboTree _combos = null;
    [Tooltip("Whether to read the combo tree past a combo depth of 1")]
    [SerializeField] private bool useCombos = true;
    [Tooltip("The effect to spawn when placing paint")]
    [SerializeField] private GameObject paintEffect = null; 
    [Tooltip("Attacks that the player can perform")]
    [SerializeField] private Attack[] _attacks = new Attack[0];
    [Tooltip("Reference to a LightningPaintEffect component if you want the player's attacks to shoot lightning in a certain paint color")]
    [SerializeField] private LightningPaintEffect _lightningEffect = null;
    [Tooltip("Whether only directed attacks create projectiles")]
    [SerializeField] private bool nonDirectedProjectiles = true;
    #endregion

    /* PRIVATE MEMBERS */

    #region Non exposed variables
    // Whether or not to begin an attack animation
    private bool attacking = false;

    // Whether an attack is in progress
    private bool inAttack = false;

    // The current attack in progress
    private Coroutine _attackCoroutine = null;

    // The attack input action currently queued by this component
    private List<PlayerAttackInputAction> _queuedAttackActions = new List<PlayerAttackInputAction>();
    #endregion

    /* PROPERTIES */

    public PlayerAttackInputAction queuedAttackAction
    {
        get
        {
            if (_queuedAttackActions.Count > 0)
            {
                return _queuedAttackActions[0];
            }
            return null;
        }
        private set
        {
            _queuedAttackActions.Add(value);
        }
    }
    #endregion

    ///* METHODS */

    /// <summary>
    /// Description:
    /// Sets up this attack handler
    /// </summary>
    public override void Setup()
    {
        if (animationInterface == null)
        {
            animationInterface = GetComponentInChildren<AttackColliderAnimationEvent>();
        }
    }

    /// <summary>
    /// Description:
    /// Gets an iterator to the input delegator's completed action types
    /// </summary>
    /// <returns></returns>
    public IEnumerable<PlayerInputAction.ActionType> GetInputChain()
    {
        foreach (PlayerInputAction action in _inputDelegator.completedActions)
        {
            yield return action.type;
        }
    }

    /// <summary>
    /// Description:
    /// Handles attack input
    /// </summary>
    public override void HandleInput()
    {
        InputManager.InputButton.heldConfirmTime = heldInputConfirmTime;
        if (GetAttackPressed() && !inAttack)
        {
            Attack result = FindAttack();
            if (result != null)
            {
                QueueAction(result);
            }
        }
    }

    /// <summary>
    /// Description: Determines if an attack button was pressed
    /// </summary>
    /// <returns></returns>
    protected override bool GetAttackPressed()
    {
        return AttackInputs.GetInput();
    }

    /// <summary>
    /// Description:
    /// Creates an enumerator through the completed and queued actions
    /// </summary>
    /// <returns></returns>
    protected IEnumerable<PlayerInputAction.ActionType> GetPriorActions()
    {
        var completed = _inputDelegator.completedActions.Where(action => action != null).Select(action => action.type);
        var upcoming = _inputDelegator.inputBuffer.Where(action => action != null).Select(action => action.type);
        foreach (var act in completed)
        {
            yield return act;
        }
        foreach (var act in upcoming)
        {
            yield return act;
        } 
    }

    /// <summary>
    ///  Finds the attack to use
    /// </summary>
    /// <returns></returns>
    private Attack FindAttack()
    {
        if (_combos != null)
        {
            (PlayerInputAction.ActionType type, bool leaf) result = _combos.FindCombo(GetPriorActions(), useCombos);
            foreach (Attack a in _attacks)
            {
                if (a != null && result.type != PlayerInputAction.ActionType.unspecified && a.attackType == result.type)
                {
                    a.clearCompleted = result.leaf;
                    return a;
                }
            }
        }
        return null;
    }

    /// <summary>
    /// Determines if this attack can occur
    /// </summary>
    /// <returns></returns>
    protected override bool CanAttack()
    {
        return base.CanAttack() && !inAttack;
    }

    /// <summary>
    /// Queues a dash action
    /// </summary>
    private void QueueAction(Attack attack)
    {
        //DequeueAction();
        var newAction = new PlayerAttackInputAction(attack, attack.attackType, BeginAttack, ExitAttackSteps, CanAttack, _attackPriorityStart);
        queuedAttackAction = newAction;
        _inputDelegator.inputBuffer.Add(newAction);
    }

    /// <summary>
    /// Dequeue a dash action
    /// </summary>
    private void DequeueAction()
    {
        if (queuedAttackAction != null)
        {
            PlayerAttackInputAction qaa = (PlayerAttackInputAction)queuedAttackAction;
            _inputDelegator.inputBuffer.Remove(queuedAttackAction);
        }
        _queuedAttackActions.Remove(queuedAttackAction);
        _queuedAttackActions = _queuedAttackActions.Where(act => (Time.timeSinceLevelLoad < act.timestamp + 0.5f)).ToList();
    }

    /// <summary>
    /// Description:
    /// Coroutine that handles attacking
    /// </summary>
    /// <param name="inputAction"></param>
    /// <returns></returns>
    public IEnumerator AttackCoroutine(PlayerInputAction inputAction = null)
    {
        float t = 0;
        PlayerAttackInputAction attackAction = (PlayerAttackInputAction)inputAction;
        Vector2 attackDirection = GetAttackDirection();

        // Set attack collider
        if (animationInterface != null)
        {
            animationInterface.attackCollider = attackAction.attack.collider;
        }

        EnterAttackSteps();

        // Wait for animation to turn on collider
        while (!attackAction.attack.collider.valid && t < 0.20f)
        {
            attackAction.attack.PositionCollider(attackDirection);
            t += Time.deltaTime;
            yield return null;
            attacking = false;
        }

        // Handle attack effects
        if (attackAction.attack.DealDamage())
        {
            StartCoroutine(ApplyKnockback(-attackDirection, _knockbackDuration));
        }
        attackAction.attack.LayPaint(inputDelegator.playerPaintColor, attackDirection, paintEffect);

        if (animationInterface == null)
        {
            yield return StartCoroutine(attackAction.attack.WaitForCollidersToDissapear());
        }
        else
        {
            yield return animationInterface.StartCoroutine(animationInterface.WaitForAttackComplete());
        }

        if (!attackAction.attack.directed && nonDirectedProjectiles)
        {
            HandleExtraAttackEffects(attackAction.attack);
        }
        else
        {
            HandleExtraAttackEffects(attackAction.attack);
        }

        if (attackAction.attack.clearCompleted)
        {
            inputDelegator.completedActions.Clear();
        }

        ExitAttackSteps();

        #region Old Implementation
        //float t = 0;

        //// Windup
        //if (inputAction != null)
        //{
        //    inputAction.priority = _attackPriorityStart;
        //}
        //while (t < windupFinishedTime)
        //{
        //    // Let animation start up

        //    t += Time.deltaTime;
        //    yield return null;
        //}

        //// Execution
        //if (inputAction != null)
        //{
        //    inputAction.priority = _attackPriorityExecute;
        //}

        //Attack();

        //t += Time.deltaTime;
        //yield return null;

        //// RecoveryFrames
        //if (inputAction != null)
        //{
        //    inputAction.priority = _attackPriorityRecovery;
        //}
        //while (t < recoveryDurationModifier)
        //{
        //    // Let animation play out

        //    t += Time.deltaTime;
        //    yield return null;
        //}

        //ExitAttackSteps();
        #endregion
    }

    /// <summary>
    /// Begins the attack coroutine
    /// </summary>
    /// <param name="inputAction"></param>
    public void BeginAttack(PlayerInputAction inputAction = null)
    {
        if (_attackCoroutine != null)
        {
            StopCoroutine(_attackCoroutine);
            ExitAttackSteps();
        }
        _attackCoroutine = StartCoroutine(AttackCoroutine(inputAction));
    }

    /// <summary>
    /// Description:
    /// Steps to take when entering an attack
    /// </summary>
    public void EnterAttackSteps()
    {
        // Begin animation
        attacking = true;

        inAttack = true;
        inNonCancellableAnimation = true;
        AudioManager.Play("BrushSwing");
    }

    /// <summary>
    /// Description: 
    /// Steps to take upon exiting or cancelling an attack
    /// </summary>
    public void ExitAttackSteps()
    {
        //attacking = false;
        inAttack = false;
        inNonCancellableAnimation = false;
        _attackCoroutine = null;
        DequeueAction();
        _inputDelegator.EndCurrentAction();
        _inputDelegator.CoroutineEndBehaviors();
    }

    /// <summary>
    /// Description:
    /// Applys a knockback effect to player upon hitting a thing
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="duration"></param>
    /// <returns></returns>
    public IEnumerator ApplyKnockback(Vector2 direction, float duration = 0.10f)
    {
        float t = 0;
        while (t < duration)
        {
            _inputDelegator.playerRigidbody.velocity += direction * _knockbackMultiplier * Time.deltaTime;
            t += Time.deltaTime;
            yield return null;
        }
    }

    /// <summary>
    /// Description
    /// Handles the case that an attack is performed, for which the direction of the attack matters
    /// This function is deprecated, but may be used in the future if multiple projectiles per attack proves op or cumbersome
    /// </summary>
    public void HandleDirectedAttack(Attack attack)
    {
        if (_lightningEffect != null)
        {
            _lightningEffect.CreateLightningProjectile(attack.attackLocation, attack.attackDirection);
        }
    }

    /// <summary>
    /// Handles effects for non directed attacks that still create projectiles
    /// </summary>
    /// <param name="attack"></param>
    public void HandleExtraAttackEffects(Attack attack)
    {
        if (_lightningEffect != null)
        {
            Vector2 baseDir = attack.attackDirection;
            foreach (float angle in attack.projectileAngles)
            {
                Vector2 rotatedDir = Quaternion.Euler(0, 0, angle) * baseDir;
                _lightningEffect.CreateLightningProjectile(attack.attackLocation, rotatedDir);
            }
        }
    }

    /// <summary>
    /// Description:
    /// Gets the state changes the player should undergo
    /// if the player is starting an attack, this will cause the Player class to set an animator triggers
    /// </summary>
    /// <returns></returns>
    public override Player.PlayerStates GetStateChanges()
    {
        Player.PlayerStates attackingState = 0;
        if (attacking && queuedAttackAction != null)
        {
            switch (queuedAttackAction.attack.attackType)
            {
                case PlayerInputAction.ActionType.lightAttack:
                    attackingState = Player.PlayerStates.lightAttacking;
                    break;
                case PlayerInputAction.ActionType.heavyAttack:
                    attackingState = Player.PlayerStates.heavyAttacking;
                    break;
                case PlayerInputAction.ActionType.lightAltAttack:
                    attackingState = Player.PlayerStates.lightAttackingAlt;
                    break;
                case PlayerInputAction.ActionType.heavyAltAttack:
                    attackingState = Player.PlayerStates.heavyAttackingAlt;
                    break;
                default:
                    break;
            }
        }
        return base.GetStateChanges() | attackingState;
    }
}
