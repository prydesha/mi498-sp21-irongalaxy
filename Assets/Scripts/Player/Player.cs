/** ------------------------------------------------------------------------------
- Filename:   Player
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/26
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** Player.cs
 * --------------------- Description ------------------------
 * Class which maintains state information, references to components, 
 * and other data necessary for a player to function.
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/26       Created file.                                       Jonathan
 * 2021/02/19		Integrated animation controller						Shawn
 * 2021/03/24       Added moving bool to animation controller           Blake
 */

public class Player : MonoBehaviour
{
    /* PUBLIC MEMBERS */
    [Flags]
    public enum PlayerStates
    {
        idle = 0,
        moving = 1,
        dashing = 2,
        lightAttacking = 4,
        heavyAttacking = 8,
        lightAttackingAlt = 16,
        heavyAttackingAlt = 32
    }

    /* PRIVATE MEMBERS */

    // The current state of the player
    private PlayerStates _playerState = 0;

    [Header("Component References")]

    [Tooltip("The player's input delegator, which manages components which handle player actions")]
    [SerializeField] private PlayerInputDelegator _playerInputDelegator = null;

    [Tooltip("The animator used to animate the player")]
    [SerializeField] private Animator _playerAnimator = null;

    [Tooltip("The sprite renderer showing the player")]
    [SerializeField] private SpriteRenderer _spriteRenderer = null;

    [Tooltip("The collider used by the player")]
    [SerializeField] private Collider2D _playerCollider = null;

    [Tooltip("The rigidbody used by the player")]
    [SerializeField] private Rigidbody2D _playerRigidbody = null;

    [Tooltip("The health used by this player")]
    [SerializeField] private Health _playerHealth = null;

    [Tooltip("The paint color manager that keeps track of currently used paint color")]
    [SerializeField] private PlayerPaintColorManager _playerColor = null;

    [Tooltip("The paint color manager that keeps track of overlapping paint colors")]
    [SerializeField] private OverlapColorDelegator _playerOverlapColorDelegator = null;

    [Tooltip("The effect to enable when the player is holding down an attack button")]
    [SerializeField] private GameObject _chargeFX = null;

    // The direction the player is facing
    private Vector2 _facingDirection = Vector2.down;

    // Whether player input is processed
    private bool _disablePlayerInput = false;

    /* PROPERTIES */

    // Accessor for the player's state
    public PlayerStates playerState
    {
        get
        {
            return _playerState;
        }
        set
        {
            _playerState = value;
        }
    }

    // Accessor for the player's rigidbody
    public Rigidbody2D playerRigidbody
    {
        get
        {
            return _playerRigidbody;
        }
    }

    // Accessor for the player's rigidbody
    public OverlapColorDelegator playerOverlapColorDelegator
    {
        get
        {
            return _playerOverlapColorDelegator;
        }
    }

    // Accessor for the player's paint color
    public PlayerPaintColorManager playerPaintColor
    {
        get
        {
            return _playerColor;
        }
        set
        {
            if (_playerColor != null)
            {
                _playerColor.paintColor = value;
            }
        }
    }

    // The direction the player is facing
    public Vector2 facingDirection
    {
        get
        {
            return _facingDirection;
        }
        set
        {
            _facingDirection = value;
            HandleAnimations();
        }
    }

    public Health Health => _playerHealth;

    public bool disablePlayerInput
    {
        get
        {
            return _disablePlayerInput || GameManager.GM.inCutscene;
        }
        set
        {
            _disablePlayerInput = value;
        }
    }

    /* METHODS */

    #region Unity Functions

    /// <summary>
    /// Description:
    /// Start function. Calls setup functions.
    /// Inputs: N/A
    /// Outputs: N/A
    /// </summary>
    void Start()
    {

        Initialize();
        
    }

    /// <summary>
    /// Description:
    /// Update function. Manages player frame by frame.
    /// Inputs: N/A
    /// Outputs: N/A
    /// </summary>
    void Update()
    {
        if (_playerInputDelegator != null && !disablePlayerInput)
        {
            _playerInputDelegator.HandleInputs();
        }
        else
        {
            _playerRigidbody.velocity = Vector2.zero;
            _playerState = PlayerStates.idle;
            facingDirection = Vector2.down;
        }
    }

    #endregion

    /// <summary>
    /// Description:
    /// Sets up the player and attempts to initialize null component references
    /// Inputs: N/A
    /// Outputs: N/A
    /// </summary>
    private void Initialize()
    {
        #region Attempt to ensure component references are set up
        if (_playerInputDelegator == null)
        {
            _playerInputDelegator = GetComponent<PlayerInputDelegator>();
        }
        if (_playerAnimator == null)
        {
            _playerAnimator = GetComponent<Animator>();
        }
        if (_spriteRenderer == null)
        {
            if (_playerAnimator != null)
            {
                _spriteRenderer = _playerAnimator.GetComponentInChildren<SpriteRenderer>();
            }
            else
            {
                _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
            }
        }
        if (_playerCollider == null)
        {
            _playerCollider = GetComponent<Collider2D>();
        }
        if (_playerOverlapColorDelegator == null)
        {
            _playerOverlapColorDelegator = GetComponent<OverlapColorDelegator>();
        }
        if (_playerRigidbody == null)
        {
            _playerRigidbody = GetComponent<Rigidbody2D>();
        }
        if (_playerHealth == null)
        {
            _playerHealth = GetComponent<Health>();
        }
        if (_playerColor == null)
        {
            _playerColor = GetComponent<PlayerPaintColorManager>();
        }
        #endregion

        #region Perform additional setup work
        if (_playerInputDelegator != null)
        {
            _playerInputDelegator.Initialize(this);
        }
        #endregion
    }

    /// <summary>
    /// Description:
    /// Handles animation of the player based on player state
    /// Inputs: N/A
    /// Outputs: N/A
    /// </summary>
    private void HandleAnimations()
    {
	    #region Animation Constants
	    const string faceX = "faceX";
	    const string faceY = "faceY";
        const string moveX = "moveX";
        const string moveY = "moveY";
        const string speed = "speed";
        const string backwards = "backwards";
        #endregion

        if (_playerAnimator != null)
        {
	        var x = facingDirection.x;
	        var y = facingDirection.y;
			_playerAnimator.SetFloat(faceX, x);   
			_playerAnimator.SetFloat(faceY, y); 
            
            if (_playerRigidbody != null)
            {
                Vector2 normalizedSpeed = _playerRigidbody.velocity;
                _playerAnimator.SetFloat(moveX, normalizedSpeed.x);
                _playerAnimator.SetFloat(moveY, normalizedSpeed.y);
                _playerAnimator.SetFloat(speed, _playerRigidbody.velocity.magnitude);
                _playerAnimator.SetBool(backwards, Vector2.Angle(normalizedSpeed, new Vector2(x, y)) > 90);
            }

            _playerAnimator.SetFloat("chargeTime", Mathf.Max(InputManager.Paint.heldTime, InputManager.Jab.heldTime));
            if (_chargeFX != null)
            {
                _chargeFX.SetActive(InputManager.Paint.held || InputManager.Jab.held);
            }

            _playerAnimator.SetBool("dashing", playerState.IsState(PlayerStates.dashing));

            _playerAnimator.SetBool("lightAttack", _playerState.IsState(PlayerStates.lightAttacking));
            _playerAnimator.SetBool("lightAttackAlt", _playerState.IsState(PlayerStates.lightAttackingAlt));
            _playerAnimator.SetBool("heavyAttack", _playerState.IsState(PlayerStates.heavyAttacking));
            _playerAnimator.SetBool("heavyAttackAlt", _playerState.IsState(PlayerStates.heavyAttackingAlt)); 
        }
    }
}

/// <summary>
/// Class which provides quality of life functions for player state (Credit to Shawn)
/// </summary>
public static class PlayerStateExtensions
{
    /// <summary>
    /// Returns true if the calling state has 
    /// the desired state's flag
    /// </summary>
    /// <param name="ps"></param>
    /// <param name="desiredState"></param>
    /// <returns></returns>
    public static bool IsState(this Player.PlayerStates ps, Player.PlayerStates desiredState)
    {
        return (ps & desiredState) == desiredState;
    }

    /// <summary>
    /// Sets the status of a specific flag within a calling state
    /// to either disabled or enabled
    /// </summary>
    /// <param name="ps">the state to set the flag in</param>
    /// <param name="flag">the flag to set</param>
    /// <param name="status">status to set the flag to</param>
    /// <returns>the calling state with the changed flag</returns>
    public static Player.PlayerStates SetFlagStatus(this Player.PlayerStates ps, Player.PlayerStates flag, bool status)
    {
        if (status)
        {
            ps |= flag;
        }
        else
        {
            ps &= ~flag;
        }
        return ps;
    }
}

