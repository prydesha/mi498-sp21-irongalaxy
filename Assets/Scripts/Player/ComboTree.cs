/** ------------------------------------------------------------------------------
- Filename:   ComboTree
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/25
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/** ComboTree.cs
 * --------------------- Description ------------------------
 * Class that defines a scriptable object with a tree structure of inputs corresponding to combos
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/25       Created file.                                       Jonathan
 */
 [CreateAssetMenu(fileName = "New Combo Tree", menuName = "Scriptable Objects/Combo Tree")]
public class ComboTree : ScriptableObject
{
    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */

    // The top node of the tree
    public CTNode tree = new CTNode(PlayerInputAction.ActionType.any);

    // Forward declaration of lists used to find combos (initialization at runtime creates memory garbage)
    private List<CTNode> currentNodes = new List<CTNode>();
    private List<CTNode> nextNodes = new List<CTNode>();

    /* PROPERTIES */



    /* METHODS */

    /// <summary>
    /// Description:
    /// Finds the node in the combo tree corresponding to a string of actions and inputs
    /// </summary>
    /// <param name="actions"></param>
    /// <returns></returns>
    public (PlayerInputAction.ActionType, bool) FindCombo(IEnumerable<PlayerInputAction.ActionType> actions, bool useCombos = true)
    {
        // Initialize result
        (PlayerInputAction.ActionType, bool) result = (PlayerInputAction.ActionType.unspecified, false);

        // Clear buffers used
        currentNodes.Clear();
        currentNodes.Add(tree);

        // Handles the case that combos are disabled (do not read nodes with a depth greater than 1)
        if (!useCombos)
        {
            return (tree.FindChild(), true);
        }

        // Find all nodes that result from iterating through the input actions
        foreach (PlayerInputAction.ActionType aType in actions)
        {
            nextNodes.Clear();
            foreach (CTNode node in currentNodes)
            {
                foreach (CTNode next in node.FindChildren(aType))
                {
                    if (next != null)
                    {
                        nextNodes.Add(next);
                    }
                }
            }
            currentNodes.Clear();
            currentNodes.AddRange(nextNodes);
            currentNodes.Add(tree);
        }

        // Clear buffer again
        nextNodes.Clear();

        // Get a list of action nodes that match current inputs
        foreach (CTNode node in currentNodes)
        {
            CTNode next = node.FindChild();
            if (next != null && next != PlayerInputAction.ActionType.any)
            {
                nextNodes.Add(next);
            }
        }

        // Sort by tree depth
        nextNodes.Sort((a, b) => -CTNode.GetDepth(a, tree).CompareTo(CTNode.GetDepth(b, tree)));

        // If there is an action to return, return it
        if (nextNodes.Count > 0)
        {
            bool clear = nextNodes[0].children.Length <= 0 && !AnyOfDepth(CTNode.GetDepth(nextNodes[0], tree));
            result = (nextNodes[0], clear);
        }
        else
        {
            string message = "No action specified for input chain:\n";
            foreach (var a in actions)
            {
                message = message + a.ToString() + "\n";
            }
            message = message + "\nInputs:\n";
            foreach (AttackInputs.AttackButtons button in AttackInputs.AttackButtons.GetValues(typeof(AttackInputs.AttackButtons)))
            {
                message = message + button.ToString() + ": " + AttackInputs.GetInput(button, false) + ",\n";
            }
            Debug.LogError(message);
            result = (PlayerInputAction.ActionType.unspecified, true);
        }
        return result;
    }

    private bool AnyOfDepth(int depth)
    {
        var currentNode = tree;
        bool result = false;
        for (int i = 0; i < depth; i++)
        {
            CTNode[] nodes = currentNode.FindChildren(PlayerInputAction.ActionType.any).ToArray();
            if (nodes.Count() == 0)
            {
                return false;
            }
            else if (i == depth - 1)
            {
                result = true;
                break;
            }
            else
            {
                currentNode = nodes[0];
            }
        }
        return result;
    }
}

#pragma warning disable
/// <summary>
/// A class representing a node in the combat tree
/// </summary>
[System.Serializable]
public class CTNode: IEnumerable
{
    // Child nodes
    public CTNode[] children = null;
    // The type of action this node represents
    public PlayerInputAction.ActionType typePayload = PlayerInputAction.ActionType.unspecified;
    // The input that triggers this type of action
    public AttackInputs.AttackButtons inputType = AttackInputs.AttackButtons.leftMouse;

    /// <summary>
    /// Description:
    /// Constructor
    /// </summary>
    /// <param name="payload"></param>
    /// <param name="input"></param>
    public CTNode(PlayerInputAction.ActionType payload, AttackInputs.AttackButtons input = AttackInputs.AttackButtons.leftMouse)
    {
        typePayload = payload;
        inputType = input;
    }

    /// <summary>
    /// Allows CTNodes to be iterable
    /// </summary>
    /// <returns></returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
        foreach (CTNode child in GetChildren())
        {
            yield return child;
        }
    }

    /// <summary>
    /// Description:
    /// Gets all the descendents of this node
    /// </summary>
    /// <returns></returns>
    public IEnumerable GetChildren()
    {
        foreach (CTNode child in children)
        {
            yield return child.GetChildren();
        }
        yield return this;
    }

    /// <summary>
    /// Finds all leaf nodes descended from this node
    /// </summary>
    /// <returns></returns>
    public IEnumerable GetLeaves()
    {
        foreach (CTNode child in children)
        {
            if (child.children.Length == 0)
            {
                yield return child;
            }
            else
            {
                yield return child.GetLeaves();
            }
        }
    }

    /// <summary>
    /// Finds a child that matches the action type
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public IEnumerable<CTNode> FindChildren(PlayerInputAction.ActionType target)
    {
        foreach (CTNode child in children)
        {
            if (child == target || child == PlayerInputAction.ActionType.any)
            {
                yield return child;
            }
        }
        //return null;
    }

    /// <summary>
    /// Finds a child that matches an input type
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public CTNode FindChild(AttackInputs.AttackButtons target)
    {
        foreach (CTNode child in children)
        {
            if (child.inputType == target)
            {
                return child;
            }
        }
        return null;
    }

    /// <summary>
    /// Finds a child whose input is supplied
    /// </summary>
    /// <returns></returns>
    public CTNode FindChild()
    {
        foreach (CTNode child in children)
        {
            if (AttackInputs.GetInput(child.inputType))
            {
                return child;
            }
        }
        return null;
    }

    /// <summary>
    /// An inefficient but effective method of determining the depth of a node with respect to another
    /// </summary>
    /// <param name="target"></param>
    /// <param name="ancestor"></param>
    /// <returns></returns>
    public static int GetDepth(CTNode target, CTNode ancestor)
    {
        if (ancestor == target)
        {
            return 0;
        }
        else if (IsAncestor(target, ancestor))
        {
            int[] depths = new int[ancestor.children.Length];
            for (int i = 0; i < ancestor.children.Length; i++)
            {
                CTNode child = ancestor.children[i];
                depths[i] = GetDepth(target, child) + 1;
            }
            return Mathf.Min(depths);
        }
        return int.MaxValue/2;
    }

    /// <summary>
    /// This is a terribly inefficient way to handle this, but since I can't add parent nodes to this 
    /// data structure, this is the best I've got
    /// </summary>
    /// <param name="target"></param>
    /// <param name="testAncestor"></param>
    /// <returns></returns>
    public static bool IsAncestor(CTNode target, CTNode testAncestor)
    {
        if (target == testAncestor)
        {
            return true;
        }
        return (from child in testAncestor.children select IsAncestor(target, child)).Any();
    }

    /// <summary>
    /// Description:
    /// Allows nodes to be treated as action types
    /// </summary>
    /// <param name="node"></param>
    public static implicit operator PlayerInputAction.ActionType(CTNode node)
    {
        return node != null? node.typePayload: PlayerInputAction.ActionType.unspecified;
    }
}
#pragma warning enable
