/** ------------------------------------------------------------------------------
- Filename:   PlayerPaintColorManager
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/09
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerPaintColorManager.cs
 * --------------------- Description ------------------------
 * Class which manages the player's currently selected paint color
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/09       Created file.                                       Jonathan
 */
public class PlayerPaintColorManager : PaintColorManager
{
    /* DELEGATE MEMBERS */

    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */
    [SerializeField]
    private List<PaintColorAuthoritative.PaintColorEnum> _availableColors = new List<PaintColorAuthoritative.PaintColorEnum>()
    {
        PaintColorAuthoritative.PaintColorEnum.Red,
        PaintColorAuthoritative.PaintColorEnum.Yellow,
        PaintColorAuthoritative.PaintColorEnum.Blue
    };

    [Tooltip("DO NOT CHANGE THIS! IT'S A WIP")]
    [SerializeField] private int _currentSelectedColor = 0;

    /* PROPERTIES */

    // Accessor for the player's currently selected paint color
    public override Color paintColor
    {
        get
        {
            return PaintColorAuthoritative.GetColor(_paintColor);
        }
        set
        {
            var val = PaintColorAuthoritative.GetValue(value);
            int index = 0;
            foreach (PaintColorAuthoritative.PaintColorEnum i in _availableColors)
            {
                if (i.IsColor(value))
                {
                    index = _availableColors.IndexOf(i);
                    break;
                }
            }
            if (index >= 0 && index < _availableColors.Count)
            {
                _currentSelectedColor = index;
                _paintColor = _availableColors[index];
            }
            else
            {
                _currentSelectedColor = 0;
                _paintColor = _availableColors[0];
            }
            onPaintColorChanged.Invoke();
        }
    }

    public PaintColorAuthoritative.PaintColorEnum PaintEnumValue => _availableColors[_currentSelectedColor];

    /* METHODS */

    /// <summary>
    /// Description:
    /// Cycles the player's paint color to the right
    /// </summary>
    public void CycleColorRight()
    {
        _currentSelectedColor = (_currentSelectedColor + _availableColors.Count - 1) % _availableColors.Count;
        paintColor = PaintColorAuthoritative.GetColor(_availableColors[_currentSelectedColor]);
    }
    /// <summary>
    /// Description:
    /// Cycles the player's paint color to the left
    /// </summary>
    public void CycleColorLeft()
    {
        _currentSelectedColor = (_currentSelectedColor + 1) % _availableColors.Count;
        paintColor = PaintColorAuthoritative.GetColor(_availableColors[_currentSelectedColor]);
    }
}
