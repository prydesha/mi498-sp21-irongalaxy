/** ------------------------------------------------------------------------------
- Filename:   PlayerAttackCollider
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/25
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerAttackCollider.cs
 * --------------------- Description ------------------------
 * Class that tracks collisions with an attackCollider
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/25       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(Collider2D))]
public class PlayerAttackCollider : MonoBehaviour, IEnumerable
{
    /* PUBLIC MEMBERS */
    // The base scale of the attack
    [HideInInspector] public Vector3 baseScale;

    /* PRIVATE MEMBERS */
    // The collider used for this attack shape
    [SerializeField] private Collider2D _attackCollider = null;
    // The renderer displayed for this attack shape
    [Tooltip("The renderer used to display this attack shape if applicable")]
    [SerializeField] private Renderer _attackRenderer = null;
    [Tooltip("The layers that can be tracked as collisions with this attack collider")]
    [SerializeField] private LayerMask _validCollisionLayers = new LayerMask();

    // The collision objects tracked by this attack collider
    private HashSet<GameObject> _collisions = new HashSet<GameObject>();

    private float timeEnabled = Mathf.Infinity;

    /* PROPERTIES */

    public bool valid
    {
        get
        {
            return (timeEnabled - Time.time < 0.1f);
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up the class
    /// </summary>
    private void Awake()
    {
        baseScale = transform.localScale;
        if (_attackCollider == null)
        {
            _attackCollider = GetComponent<Collider2D>();
        }
        if (_attackRenderer == null)
        {
            _attackRenderer = GetComponent<Renderer>();
        }
    }

    private void OnDisable()
    {
        timeEnabled = Mathf.Infinity;
    }

    private void OnEnable()
    {
        timeEnabled = Time.time;
    }

    /// <summary>
    /// Description:
    /// Rotates the collider around it's parent transform to face a world space direction
    /// </summary>
    /// <param name="dir"></param>
    public void FaceDirection(Vector2 dir)
    {
        Vector2 currentDirToColl = GetCurrentDirection();
        _attackCollider.transform.RotateAround(transform.parent.position, Vector3.forward, Vector2.SignedAngle(currentDirToColl, -dir));
    }

    /// <summary>
    /// Description:
    /// returns the normalized direction to the attack collider
    /// </summary>
    /// <returns></returns>
    public Vector2 GetCurrentDirection()
    {
        Vector2 direction = (transform.parent.position - _attackCollider.transform.position).normalized;
        if (direction == Vector2.zero)
        {
            direction = transform.up;
        }
        return direction;
    }

    /// <summary>
    /// Description:
    /// returns the collider's current position (used to find projectile spawn point)
    /// </summary>
    /// <returns></returns>
    public Vector2 GetCurrentPosition()
    {
        return transform.position;
    } 

    /// <summary>
    /// Description:
    /// Shows or hides the renderer of this attack
    /// </summary>
    /// <param name="show"></param>
    public void ShowAttack(bool show)
    {
        if (_attackRenderer != null)
        {
            _attackRenderer.enabled = show;
        }
    }

    /// <summary>
    /// Description:
    /// Makes this class enumerable
    /// </summary>
    /// <returns></returns>
    public IEnumerator GetEnumerator()
    {
        List<RaycastHit2D> hits = new List<RaycastHit2D>();
        ContactFilter2D filter = new ContactFilter2D();
        filter.useTriggers = true;

        _attackCollider.Cast(transform.up, filter, hits, 0, true);

        foreach (RaycastHit2D hit in hits)
        {
            yield return (hit.collider.gameObject, hit.point);
        }
    }
}
