/** ------------------------------------------------------------------------------
- Filename:   AnimationAttackTrigger
- Project:    Brush Knight 
- Developers: Team BoSS
- Created on: 2021/02/24
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** AnimationAttackTrigger.cs
 * --------------------- Description ------------------------
 * Class which can be used to trigger player attacks from animation events
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/24       Created file.                                       Jonathan
 */
public class AnimationAttackTrigger : MonoBehaviour
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */


    /* PROPERTIES */

    // The "Properties" section is for getters/setters of 
    // your private variables (feel free to delete this comment)

    /* METHODS */

    public void EnableAttackCollider(int index)
    {
        Debug.Log("Dummy enable attack collider");
    }

    public void DisableAttackCollider(int index)
    {
        Debug.Log("Dummy disable attack collider");
    }
}
