/** ------------------------------------------------------------------------------
- Filename:   BasePlayerColorManagerMatcher
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/09
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
/** BasePlayerColorManagerMatcher.cs
 * --------------------- Description ------------------------
 * Base class for components which match the player's color
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/09       Created file.                                       Jonathan
 */
 [ExecuteAlways()]
public abstract class BasePlayerColorManagerMatcher : BaseColorManagerMatcher
{
    /* PUBLIC MEMBERS */

    /* PRIVATE MEMBERS */
    // The player to base this color matcher on
    protected Player _player = null;

    /* PROPERTIES */

    protected override PaintColorManager paintColorManager
    {
        get => _player != null? _player.playerPaintColor : null;
        set => base.paintColorManager = value;
    }

    protected override Color _targetColor
    {
        get
        {
            if (_player != null)
            {
                return paintColorManager;
            }
            else
            {
                return Color.red;
            }
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Sets up this color matcher
    /// </summary>
    protected override void OnEnable()
    {
        Setup(true);
    }

    protected override void OnDisable()
    {
        if (_player != null)
        {
            _player.playerPaintColor.onPaintColorChanged -= SetColor;
        }
    }

    public override void Setup(bool force = false)
    {
        if (!setup || force)
        {
            if (_player == null)
            {
                _player = GetComponentInParent<Player>();
            }
            if (_player != null)
            {
                SetColor();
                _player.playerPaintColor.onPaintColorChanged += BeginChange;
            }
            setup = true;
        }
    }
}
