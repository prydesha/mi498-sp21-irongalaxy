/** ------------------------------------------------------------------------------
- Filename:   PlayerInputDelegator
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/26
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerInputDelegator.cs
 * --------------------- Description ------------------------
 * Class which manages input handlers which collect input and act on it
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/26       Created file.                                       Jonathan
 */
public class PlayerInputDelegator : MonoBehaviour
{
    /* PUBLIC MEMBERS */

    // delegate used to tell when a coroutine ends in an input handler
    public delegate void OnCoroutineEndDelegate();
    // instance of that delegate
    public static OnCoroutineEndDelegate onCoroutineEnd = delegate { };

    // The buffer of inputs
    [HideInInspector] public List<PlayerInputAction> inputBuffer = new List<PlayerInputAction>();

    /* PRIVATE MEMBERS */

    [Tooltip("List of input handlers which handle player actions")]
    [SerializeField] private List<BaseInputHandler> _inputHandlers = new List<BaseInputHandler>();

    // A reference to the player controller which manages this player input delegator
    private Player playerController = null;

    // The last time an attack handler managed by this delegator attacked
    private float _lastAttackTime = Mathf.NegativeInfinity;

    // Last time an action was taken
    private float _lastActionTime = Mathf.NegativeInfinity;

    // The action the player is currently performing
    private PlayerInputAction currentAction = null;

    // Actions completed by the player (FIFO with expiration)
    [HideInInspector] public List<PlayerInputAction> completedActions = new List<PlayerInputAction>();

    /* PROPERTIES */

    // Accessor for the player's current state
    public Player.PlayerStates playerState
    {
        get
        {
            if (playerController != null)
            {
                return playerController.playerState;
            }
            return 0;
        }
        private set
        {
            if (playerController != null)
            {
                playerController.playerState = value;
            }
        }
    }

    // Accessor for the player's rigidbody
    public Rigidbody2D playerRigidbody
    {
        get
        {
            if (playerController != null)
            {
                return playerController.playerRigidbody;
            }
            return null;
        }
    }

    // Accessor for the player's health
    public Health playerHealth
    {
        get
        {
            if (playerController != null)
            {
                return playerController.Health;
            }
            return null;
        }
    }

    // Accessor for the player's health
    public OverlapColorDelegator playerOverlapColorDelegator
    {
        get
        {
            if (playerController != null)
            {
                return playerController.playerOverlapColorDelegator;
            }
            return null;
        }
    }

    // Accessor for the player's paint color
    public PlayerPaintColorManager playerPaintColor
    {
        get
        {
            return playerController.playerPaintColor;
        }
    }

    /// <summary>
    /// Accessor for the player's facing direction
    /// </summary>
    public Vector2 playerFacingDirection
    {
        get
        {
            if (playerController != null)
            {
                return playerController.facingDirection;
            }
            return Vector2.down;
        }
        set
        {
            if (playerController != null)
            {
                playerController.facingDirection = value;
            }
        }
    }

    private Vector2 _lastLookInputDirection = Vector2.zero;
    public Vector2 lookInputDirection
    {
        get
        {
            Vector2 result = _lastLookInputDirection;
            switch(InputManager.currentControlScheme)
            {
                case "KeyboardAndMouse":
                    result = (Camera.main.ScreenToWorldPoint(InputManager.MousePosition) - transform.position).normalized;
                    break;
                case "Gamepad":
                    result = InputManager.Look;
                    break;
                default:
                    break;
            }
            if (result == Vector2.zero)
            {
                result = InputManager.Move;
                if (result == Vector2.zero)
                {
                    result = _lastLookInputDirection;
                }
            }
            _lastLookInputDirection = result;
            return result;
        }
    }

    // Whether this player is in a non-cancellable animation
    [HideInInspector]
    public bool inNonCancellableAnimation
    {
        get
        {
            foreach (var handler in _inputHandlers)
            {
                if (handler.inNonCancellableAnimation)
                {
                    return true;
                }
            }
            return false;
        }
    }

    // Accessor to the last time an input handler managed by this delegator performed an attack
    public float attackCooldownTime
    {
        get
        {
            return _lastAttackTime;
        }
        set
        {
            _lastAttackTime = value;
        }
    }

    /* METHODS */

    private void Update()
    {
        if (Time.time - _lastActionTime > 1f)
        {
            completedActions.Clear();
        }
    }

    /// <summary>
    /// Description: Sets up the player input delegator with a reference to a player
    /// Inputs: setPlayer - Player
    /// Outputs: N/A
    /// </summary>
    /// <param name="setPlayer"></param>
    public void Initialize(Player setPlayer = null)
    {
        if (setPlayer == null)
        {
            playerController = GetComponent<Player>();
        }
        else
        {
            playerController = setPlayer;
        }

        foreach (var inputHandler in _inputHandlers)
        {
            inputHandler.inputDelegator = this;
            inputHandler.Setup();
        }
    }

    /// <summary>
    /// Description:
    /// Iterates through input handlers and orders them to perform actions
    /// Inputs: N/A
    /// Outputs: N/A
    /// </summary>
    public void HandleInputs()
    {
        foreach (var inputHandler in _inputHandlers)
        {
            inputHandler.HandleInput();
        }
        playerState = GetState();
        TakeBufferedActions();
    }

    /// <summary>
    /// Description:
    /// Computes the combined state of all input handlers
    /// </summary>
    /// <returns></returns>
    public Player.PlayerStates GetState()
    {
        Player.PlayerStates state = 0;
        foreach (var inputHandler in _inputHandlers)
        {
            state = state | inputHandler.GetStateChanges();
        }
        return state;
    }

    /// <summary>
    /// Description:
    /// Weird shit.
    /// Iterates through actions to take until one can be taken, then takes that action
    /// </summary>
    public void TakeBufferedActions()
    {
        while (inputBuffer.Count > 0)
        {
            int nextActionIndex = 0;
            int nextActionPriority = -1;
            for (int i = 0; i < inputBuffer.Count; i++)
            {
                if (nextActionPriority < inputBuffer[i].priority)
                {
                    nextActionPriority = inputBuffer[i].priority;
                    nextActionIndex = i;
                    inputBuffer.RemoveRange(0, i);
                    i = 0;
                }
            }
            PlayerInputAction nextAction = inputBuffer[0];
            if (currentAction != null && nextAction != null && nextAction.priority > currentAction.priority && nextAction.CanPerform())
            {
                // Cancel action in favor of higher priority
                currentAction.Cancel();
                currentAction = nextAction;
                inputBuffer.RemoveAt(0);
                currentAction.PerformAction();
                _lastActionTime = Time.time;
                completedActions.Add(currentAction);
                break;
            }
            else if (currentAction == null && nextAction != null && nextAction.CanPerform())
            {
                // Take next action in buffer
                currentAction = nextAction;
                inputBuffer.RemoveAt(0);
                currentAction.PerformAction();
                _lastActionTime = Time.time;
                completedActions.Add(currentAction);
                break;
            }
            else if (currentAction == null)
            {
                // There is no next action or it cannot be performed
                inputBuffer.RemoveAt(0);
            }
            else
            {
                break;
            }
        }
    }

    /// <summary>
    /// Description:
    /// Ends the current action
    /// </summary>
    public void EndCurrentAction()
    {
        currentAction = null;
    }

    #region Inter-Handler Communication
    /// <summary>
    /// Description:
    /// Function to be called when a handler coroutine ends
    /// </summary>
    public void CoroutineEndBehaviors()
    {
        onCoroutineEnd?.Invoke();
        onCoroutineEnd = delegate { };
    }
    #endregion
}
