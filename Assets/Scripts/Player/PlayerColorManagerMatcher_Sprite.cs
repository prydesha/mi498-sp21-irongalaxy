/** ------------------------------------------------------------------------------
- Filename:   PlayerColorMatcher_Sprite
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/02/09
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** PlayerColorMatcher_Sprite.cs
 * --------------------- Description ------------------------
 * Fades paint color on sprites to match the player's currently selected color
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/02/09       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(SpriteRenderer))]
public class PlayerColorMatcher_Sprite : BasePlayerColorManagerMatcher
{
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    // The sprite renderer to affect
    private SpriteRenderer _spriteRenderer = null;
    [Tooltip("The name of the shader property to set")]
    [SerializeField] private string materialPropertyName = "ReplacementColor";
    [SerializeField] private float _fadeTime = 0;

    private Coroutine fadeCoroutine = null;

    /* PROPERTIES */

    // Accesses the sprite renderer to control secondary color of
    public SpriteRenderer spriteRenderer
    {
        get
        {
            if (_spriteRenderer == null)
            {
                _spriteRenderer = GetComponent<SpriteRenderer>();
            }
            return _spriteRenderer;
        }
    }

    /* METHODS */

    /// <summary>
    /// Description:
    /// Initiates color change
    /// </summary>
    public override void BeginChange()
    {
        if (fadeCoroutine != null)
        {
            StopCoroutine(fadeCoroutine);
        }
        fadeCoroutine = StartCoroutine(FadeColor());
    }

    /// <summary>
    /// Description:
    /// Sets the color of this color matcher's sprite renderer
    /// </summary>
    public override void SetColor()
    {
        if (Application.isEditor        )
        {
            spriteRenderer.sharedMaterial.SetColor(materialPropertyName, _targetColor);
        }
        else
        {
            spriteRenderer.material.SetColor(materialPropertyName, _targetColor);
        }
    }
    /// <summary>
    /// Override that allows for setting a color that is not the _target color
    /// </summary>
    /// <param name="colorToSet"></param>
    public void SetColor(Color colorToSet)
    {
        if (Application.isEditor)
        {
            spriteRenderer.sharedMaterial.SetColor(materialPropertyName, colorToSet);
        }
        else
        {
            spriteRenderer.material.SetColor(materialPropertyName, colorToSet);
        }
    }

    /// <summary>
    /// Description:
    /// Coroutine that fades the currently displayed color to the target color
    /// </summary>
    /// <returns></returns>
    public override IEnumerator FadeColor()
    {
        float t = 0;;
        Color startColor = spriteRenderer.material.GetColor(materialPropertyName);
        while (t < _fadeTime)
        {
            Color currentColor = Color.Lerp(startColor, _targetColor, t / _fadeTime);
            SetColor(currentColor);

            t += Time.deltaTime;
            yield return null;
        }
        SetColor();
    }
}
