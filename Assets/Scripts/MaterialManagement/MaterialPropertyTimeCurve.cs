/** ------------------------------------------------------------------------------
- Filename:   MaterialPropertyTimeCurve
- Project:    Avant Guardian 
- Developers: Team BoSS
- Created on: 2021/03/24
- Created by: Jonathan
------------------------------------------------------------------------------- */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** MaterialPropertyTimeCurve.cs
 * --------------------- Description ------------------------
 * Uses an animation curve to control a material property over time
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/03/24       Created file.                                       Jonathan
 */
 [RequireComponent(typeof(Renderer))]
public class MaterialPropertyTimeCurve : MonoBehaviour
{ 
    /* PUBLIC MEMBERS */


    /* PRIVATE MEMBERS */
    [SerializeField] private bool startAutomatically = false;
    [SerializeField][Min(0.01f)] private float duration = 1.0f;
    [SerializeField] private AnimationCurve curve = new AnimationCurve();
    [SerializeField] private float curveMultiplier = 1.0f;
    [SerializeField] private string propertyName = null;
    private Coroutine curveCoroutine = null;
    private Renderer _renderer = null;

    /* PROPERTIES */

    private Renderer targetRenderer
    {
        get
        {
            if (_renderer != null)
            {
                return _renderer;
            }
            _renderer = GetComponent<Renderer>();
            return _renderer;
        }
    }

    /* METHODS */



    #region Unity Functions
    // Start is called before the first frame update
    void Start()
    {
        if (startAutomatically)
        {
            BeginCurveCoroutine();
        }
    }
    #endregion

    public void BeginCurveCoroutine()
    {
        if (curveCoroutine != null)
        {
            StopCoroutine(curveCoroutine);
        }
        curveCoroutine = StartCoroutine(CurveCoroutine());
    }

    public IEnumerator CurveCoroutine()
    {
        for (float t = 0; t < duration; t += Time.deltaTime)
        {
            float curveTime = t / duration;
            float value = curve.Evaluate(curveTime) * curveMultiplier;
            targetRenderer.material.SetFloat(propertyName, value);
            yield return null;
        }
    }
}
