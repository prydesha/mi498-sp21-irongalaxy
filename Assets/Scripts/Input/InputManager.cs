/** ------------------------------------------------------------------------------
- Filename:   InputManager
- Project:    MI498-SP21-IronGalaxy 
- Developers: IronGalaxy
- Created on: 2021/01/25
- Created by: shawn
------------------------------------------------------------------------------- */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

/** InputManager.cs
 * --------------------- Description ------------------------
 * Singleton class which handles the reading and passing of player input
 * to all other scripts in our game. This class does nothing to modify that
 * input or use it in any way, it is simply a means for accessing input values
 *
 * ------------------------- Log ----------------------------
 * 
 * Date             Work Description                                    Name
 * -----------      ---------------------------                         --------
 * 2021/01/25       Created file.                                       shawn
 * 2021/03/16		Added buttons for specific paint switching			Shawn
 */
[RequireComponent(typeof(PlayerInput))]
public class InputManager : MonoBehaviour
{
	#region Constants
	public const string ACTION_MAP_INGAME = "InGame";
	public const string ACTION_MAP_UI = "UI";
	#endregion
	
    /* PUBLIC MEMBERS */
    // delegate which can be used to outline a function which is called after 
    // this script receives a SendMessage from its player input
    public delegate void InputExtension(InputValue value);
    /// <summary>
    /// Occurs immediately after a movement input has been received and interpreted
    /// by this manager
    /// </summary>
    public static event InputExtension OnMoveExtend;
    /// <summary>
    /// Occurs immediately after a looking input from a gamepad has been
    /// received and interpreted by this manager
    /// </summary>
    public static event InputExtension OnGamePadLookExtend;
    /// <summary>
    /// Occurs immediately after the mouse position has been
    /// changed and interpreted by this manager
    /// </summary>
    public static event InputExtension OnMouseMoveExtend;




    /* PRIVATE MEMBERS */
    #region Singleton instance
    private static InputManager _instance = null;
    private static void MissingInstanceBehavior()
    {
        Debug.LogWarning("Some script is trying to acquire " +
                         "data from a null Player singleton instance");
    }
    #endregion

    // our player input object
    private PlayerInput _playerInput = null;

    private bool _anyKeyWasPressedThisFrame = false;
    /// <summary>
    /// Occurs whenever any key from any input map
    /// is pressed
    /// </summary>
    public static UnityEvent OnAnyKeyPressed = new UnityEvent();

    #region Input Variable and Button Declerations

    private Vector2 _move = Vector2.zero;
    private Vector2 _gamepadlook = Vector2.zero;
    private Vector2 _mousePosition = Vector2.zero;

    private InputButton _paintButton = new InputButton();
    private InputButton _jabButton = new InputButton();
    private InputButton _startButton = new InputButton();
    private InputButton _dodgeButton = new InputButton();
    private InputButton _cyclePaintLeft = new InputButton();
    private InputButton _cyclePaintRight = new InputButton();
    private InputButton _pause = new InputButton();
    private InputButton _red = new InputButton();
    private InputButton _yellow = new InputButton();
    private InputButton _blue = new InputButton();

    private Vector2 _navigate = Vector2.zero;

    private InputButton _select = new InputButton();
    private InputButton _unpause = new InputButton();
    // Reference to all buttons used by this manager
    private List<InputButton> _allButtons;
    #endregion

    /* PROPERTIES */

    public static string currentControlScheme
    {
        get
        {
            return _instance._playerInput.currentControlScheme;
        }
    }



    #region Input Getters
    /// <summary>
    /// Getter for the player's current move input value
    /// (A vector2 whose coordinates will be between -1 and 1)
    /// </summary>
    public static Vector2 Move
    {
        get
        {
            if (_instance)
            {
                return _instance._move;
            }
            MissingInstanceBehavior();
            return default;
        }
    }
    /// <summary>
    /// Getter for the player's current gamepad looking input value
    /// (A vector2 whose coordinates will be between -1 and 1)
    /// </summary>
    public static Vector2 Look
    {
        get
        {
            if (_instance)
            {
                return _instance._gamepadlook;
            }
            MissingInstanceBehavior();
            return default;
        }
    }
    /// <summary>
    /// Getter for the current screen position of the mouse.
    /// (0,0) is the bottom left corner
    /// </summary>
    public static Vector2 MousePosition
    {
        get
        {
            if (_instance)
            {
                return _instance._mousePosition;
            }
            MissingInstanceBehavior();
            return default;
        }
    }
    /// <summary>
    /// Getter for the player's current paint input value
    /// </summary>
    public static InputButton Paint
    {
        get
        {
            if (_instance)
            {
                return _instance._paintButton;
            }
            MissingInstanceBehavior();
            return null;
        }
    }
    /// <summary>
    /// Getter for the player's current jab input value
    /// </summary>
    public static InputButton Jab
    {
        get
        {
            if (_instance)
            {
                return _instance._jabButton;
            }
            MissingInstanceBehavior();
            return null;
        }
    }
    /// <summary>
    /// Getter for the player's current cycle paint left
    /// </summary>
    public static InputButton CyclePaintLeft
    {
        get
        {
            if (_instance)
            {
                return _instance._cyclePaintLeft;
            }
            MissingInstanceBehavior();
            return null;
        }
    }
    /// <summary>
    /// Getter for the player's current cycle paint right
    /// </summary>
    public static InputButton CyclePaintRight
    {
        get
        {
            if (_instance)
            {
                return _instance._cyclePaintRight;
            }
            MissingInstanceBehavior();
            return null;
        }
    }
    /// <summary>
    /// Getter for the player's current start input value
    /// </summary>
    public static InputButton Start
    {
        get
        {
            if (_instance)
            {
                return _instance._startButton;
            }
            MissingInstanceBehavior();
            return null;
        }
    }
    /// <summary>
    /// Getter for the player's current dodge input value
    /// </summary>
    public static InputButton Dodge
    {
        get
        {
            if (_instance)
            {
                return _instance._dodgeButton;
            }
            MissingInstanceBehavior();
            return null;
        }
    }

    /// <summary>
    /// Getter for the player's Navigation
    /// </summary>
    public static Vector2 Navigation
    {
        get
        {
            if (_instance)
            {
                return _instance._navigate;
            }
            MissingInstanceBehavior();
            return Vector2.zero;
        }
    }
    /// <summary>
    /// Getter for the player's current Select
    /// </summary>
    public static InputButton Select
    {
        get
        {
            if (_instance)
            {
                return _instance._select;
            }
            MissingInstanceBehavior();
            return null;
        }
    }
    public static InputButton Pause
    {
        get
        {
            if (_instance)
            {
                return _instance._pause;
            }
            MissingInstanceBehavior();
            return null;
        }
    }
    public static InputButton UnPause
    {
        get
        {
            if (_instance)
            {
                return _instance._unpause;
            }
            MissingInstanceBehavior();
            return null;
        }
    }
    public static InputButton Red
    {
	    get
	    {
		    if (_instance)
		    {
			    return _instance._red;
		    }
		    MissingInstanceBehavior();
		    return null;
	    }
    }
    public static InputButton Blue
    {
	    get
	    {
		    if (_instance)
		    {
			    return _instance._blue;
		    }
		    MissingInstanceBehavior();
		    return null;
	    }
    }
    public static InputButton Yellow
    {
	    get
	    {
		    if (_instance)
		    {
			    return _instance._yellow;
		    }
		    MissingInstanceBehavior();
		    return null;
	    }
    }
    #endregion

    /* METHODS */

    #region SendMessage Receivers
    /// <summary>
    /// Called via SendMessage() from this player's PlayerInput component.
    /// Updates the current movement variables of our player
    /// </summary>
    /// <param name="value"></param>
    private void OnMove(InputValue value)
    {
        _move = value.Get<Vector2>();
        OnMoveExtend?.Invoke(value);
    }
    // sim...
    private void OnGamePadLook(InputValue value)
    {
        _gamepadlook = value.Get<Vector2>();
        OnGamePadLookExtend?.Invoke(value);
    }
    private void OnMousePosition(InputValue value)
    {
        _mousePosition = value.Get<Vector2>();
        OnMouseMoveExtend?.Invoke(value);
    }
    private void OnPaint(InputValue value)
    {
        CheckButtonBasedOnValue(_paintButton, value);
    }
    private void OnJab(InputValue value)
    {
        CheckButtonBasedOnValue(_jabButton, value);
    }
    private void OnCyclePaintLeft(InputValue value)
    {
        CheckButtonBasedOnValue(_cyclePaintLeft, value);
    }
    private void OnCyclePaintRight(InputValue value)
    {
        CheckButtonBasedOnValue(_cyclePaintRight, value);
    }
    private void OnDodge(InputValue value)
    {
        CheckButtonBasedOnValue(_dodgeButton, value);
    }
    private void OnStart(InputValue value)
    {
        CheckButtonBasedOnValue(_startButton, value);
    }
    private void OnNavigate(InputValue value)
    {
        _navigate = value.Get<Vector2>();
        OnMoveExtend?.Invoke(value);
    }
    private void OnSelect(InputValue value)
    {
        CheckButtonBasedOnValue(_select, value);
    }
    private void OnPause(InputValue value)
    {
        CheckButtonBasedOnValue(_pause, value);
    }
    private void OnUnPause(InputValue value)
    {
        CheckButtonBasedOnValue(_unpause, value);
    }
    private void OnBlue(InputValue value)
    {
	    CheckButtonBasedOnValue(_blue, value);
    }
    private void OnYellow(InputValue value)
    {
	    CheckButtonBasedOnValue(_yellow, value);
    }
    private void OnRed(InputValue value)
    {
	    CheckButtonBasedOnValue(_red, value);
    }
    #endregion

    #region General methods used by more than one button
    /// <summary>
    /// Given an input button and input value, determines if the button
    /// should be pressed or released
    /// </summary>
    /// <param name="button">The button that has received input</param>
    /// <param name="value">The input the button has received</param>
    private void CheckButtonBasedOnValue(InputButton button, InputValue value)
    {
	    CheckButtonBasedOnValue(button, value.isPressed);
    }
    /// <summary>
    /// Given an input button and input value, determines if the button
    /// should be pressed or released
    /// </summary>
    /// <param name="button">The button that has received input</param>
    /// <param name="value">true if the button should be pressed</param>
    private void CheckButtonBasedOnValue(InputButton button, bool value)
    {
	    if (value)
	    {
		    AnyKeyPressedBehavior();
		    button.Press();
	    }
	    else
	    {
		    button.Release();
	    }
    }
    
    /// <summary>
    /// Checks to perform actions associated with the
    /// event that any key has been pressed
    /// </summary>
    private void AnyKeyPressedBehavior()
    {
        if (!_anyKeyWasPressedThisFrame)
        {
            _anyKeyWasPressedThisFrame = true;
            OnAnyKeyPressed?.Invoke();
        }
    }
    /// <summary>
    /// Changes the current action map being used to detect input.
    /// </summary>
    /// <param name="newMap">This should be one of the 
    /// string constants specified by the PlayerInputDetector 
    /// (i.e. InputManager.ACTION_MAP_UI)</param>
    public static void SwitchInputType(string newMap)
    {
        if (_instance)
        {
            _instance._playerInput.SwitchCurrentActionMap(newMap);
            foreach (InputButton b in _instance._allButtons)
            {
                b.Release();
            }

        }
        else
        {
            Debug.LogError("No InputManager");
        }
    }

    #endregion

    #region Unity Functions

    private void Awake()
    {
        #region Enforce Singleton
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;
        transform.parent = null;
        DontDestroyOnLoad(gameObject);
        #endregion

        _playerInput = GetComponent<PlayerInput>();
        _playerInput.notificationBehavior = PlayerNotifications.SendMessages;
        
        // initialize list of buttons
        _allButtons = new List<InputButton>()
        {
            _paintButton,
            _jabButton,
            _dodgeButton,
            _cyclePaintLeft,
            _cyclePaintRight,
            _startButton,
            _unpause,
            _pause,
            _select,
            _red,
            _blue,
            _yellow
        };
    }

    // Update is called once per frame
    void Update()
    {
        // check for 'any key' using the new unity input system
        if (Keyboard.current != null)
        {
            if (Keyboard.current.anyKey.wasPressedThisFrame) 
            {
                AnyKeyPressedBehavior();
            }
        }
    }
    
    private void LateUpdate()
    {
        // Reset button presses and releases
        foreach(InputButton b in _allButtons)
        {
            b.EndFrame();
        }

        _anyKeyWasPressedThisFrame = false;
    }

    #endregion

    
    /// <summary>
    /// Class used to store button variables. The Methods of this class
    /// should only be used by the InputManager, but its properties are
    /// available to all
    /// </summary>
    public class InputButton
    {
        public static float heldConfirmTime = 0.75f;

        // is this button currently being pressed?
        private bool _isDown = false;
        // has the button been initially pressed during the current frame?
        private bool _justPressed = false;
        // has the button been released during the current frame?
        private bool _justReleased = false;
        // was the button released this frame and not held in the last frame?
        private bool _justTapped = false;
        // was the button held last frame?
        public bool heldLastFrame = false;
        // the amount of time that this button has been pressed
        private float _downTime = Mathf.Infinity;

        #region Properties
        /// <summary>
        /// Was this button pressed during the current frame?
        /// </summary>
        public bool justPressed => _justPressed;
        /// <summary>
        /// Was this button released during the current frame?
        /// </summary>
        public bool justReleased => _justReleased;
        /// <summary>
        /// Was this button released during the current frame?
        /// </summary>
        public bool justTapped => _justTapped;
        /// <summary>
        /// Is this button currently being pressed?
        /// </summary>
        public bool isDown => _isDown;
        /// <summary>
        /// Is this button currently not pressed?
        /// </summary>
        public bool isUp => !_isDown;
        /// <summary>
        /// Is this button held (pressed but not just pressed this frame)
        /// </summary>
        public bool held
        {
            get
            {
                return Time.time - _downTime > heldConfirmTime;
            }
        }
        /// <summary>
        /// Did the button just register as being held?
        /// </summary>
        public bool justHeld
        {
            get
            {
                return held && !heldLastFrame;
            }
        }
        public float heldTime
        {
            get
            {
                return _downTime < Mathf.Infinity? Time.time - _downTime: 0;
            }
        }
        #endregion

        #region Delegates and Events
        public delegate void ButtonEvent();
        // invoked whenever the Press function of this button is called
        public event ButtonEvent OnButtonPress = delegate { };
        // invoked whenever the Release function of this button is called
        public event ButtonEvent OnButtonRelease = delegate { };
        // invoked whenever the button is held for at minimum HELDCONFIRMTIME seconds
        public event ButtonEvent OnButtonHeld = delegate { };
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public InputButton()
        {
            _isDown = false;
            _justReleased = false;
            _justPressed = false;
        }

        /// <summary>
        /// InputManager Only Function.
        /// Function to call when this button has been pressed
        /// </summary>
        public void Press()
        {
            if (!_isDown)
            {
                _isDown = true;
                _justPressed = true;
                SetHeld();
                OnButtonPress?.Invoke();
            }
        }

        /// <summary>
        /// InputManager Only Function.
        /// Function to call when this button has been released
        /// </summary>
        public void Release()
        {
            if (_isDown)
            {
                _isDown = false;
                _justReleased = true;
                _justTapped = !held;
                ResetHeld();
                OnButtonRelease?.Invoke();
            }
        }

        /// <summary>
        /// InputManager Only Function.
        /// Function to call at the end of a frame, resets the values
        /// of JustPressed and JustReleased
        /// </summary>
        public void EndFrame()
        {
            _justPressed = false;
            _justReleased = false;
            _justTapped = false;
            if (held)
            {
                if (!heldLastFrame)
                {
                    OnButtonHeld.Invoke();
                }
            }
            heldLastFrame = held;
        }

        private void SetHeld()
        {
            _downTime = Time.time;
        }

        public void ResetHeld()
        {
            _downTime = Mathf.Infinity;
        }
    }
}
